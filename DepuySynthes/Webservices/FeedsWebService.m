//
//  FeedsWebService.m
//  DepuySynthes
//
//  Created by Akshay Gohel on 28/12/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import "FeedsWebService.h"
#import <UIKit/UIKit.h>
#import "Constants.h"

#define TIME_OUT_INTERVAL 30.0f

@interface FeedsWebService () <NSURLConnectionDataDelegate> {
    NSMutableData *responseData;
    NSURLConnection *connection;
    NSTimer *timer;
}

@property(nonatomic, copy) CompletionBlock completionBlock;

@end

@implementation FeedsWebService

-(void) getFeedsWithKey:(NSString*)key columns:(NSArray*)columnNameArray byFeedID:(NSString*)feedID byCategoryID:(NSString*)categoryID limit:(int)limit andCompletionBlock:(CompletionBlock) completionBlock {
    self.completionBlock = completionBlock;
    // create request
    
    NSString *urlString = nil;
    if (columnNameArray.count == 0) {
        urlString = [NSString stringWithFormat:@"http://marathmola.com/feed-login/?key=%@&type=feed", key];
    } else {
        NSString *columns = @"";
        for (id colName in columnNameArray) {
            if ([colName isKindOfClass:[NSString class]]) {
                columns = [NSString stringWithFormat:@"%@%@,",columns, colName];
            }
        }
        columns = [columns substringToIndex:columns.length-1];
        urlString = [NSString stringWithFormat:@"http://marathmola.com/feed-login/?key=%@&type=feed&col=%@", key, columns];
    }
    if (feedID && ![feedID isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&id=%@", urlString, feedID];
    }
    if (categoryID && ![categoryID isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&cid=%@", urlString, categoryID];
    }
    if (limit > 0) {
        urlString = [NSString stringWithFormat:@"%@&limit=%d", urlString, limit];
    }
    
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_INTERVAL];
    
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    //    CFRunLoopRun();
    if (connection) {
        timer = [NSTimer scheduledTimerWithTimeInterval:TIME_OUT_INTERVAL target:self selector:@selector(requestTimedOut) userInfo:nil repeats:NO];
        // response data of the request
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    DLog(@"%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    if (responseData == nil) {
        responseData = [[NSMutableData alloc] initWithData:data];
    } else {
        [responseData appendData:data];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    DLog(@"response: %@", response);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    //    CFRunLoopStop(CFRunLoopGetCurrent());
    [timer invalidate];
    timer = nil;
    DLog(@"connectionDidFinishLoading");
    [self parseLoginResponse];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [timer invalidate];
    timer = nil;
    responseData = nil;
    DLog(@"Error: %@", error.localizedDescription);
    if (self.completionBlock) {
        self.completionBlock(@"Error");
    }
    //    CFRunLoopStop(CFRunLoopGetCurrent());
}

-(void) requestTimedOut {
    DLog(@"Request Timed Out");
    [connection cancel];
    [self showAppologiesAlertWithTitle:nil andMessage:nil andDelegate:nil];
    [self connection:connection didFailWithError:nil];
}

#pragma mark - Login JSON Parsing

-(void) parseLoginResponse {
    @try {
        NSError *error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData
                                                             options:kNilOptions
                                                               error:&error];
        if (self.completionBlock) {
            self.completionBlock(json);
        }
    }
    @catch (NSException *exception) {
        DLog(@"Exception: %@", exception);
        [connection cancel];
        [self showAppologiesAlertWithTitle:nil andMessage:nil andDelegate:nil];
        [self connection:connection didFailWithError:nil];
    }
}

-(void) showAppologiesAlertWithTitle:(NSString*) title andMessage:(NSString*) message andDelegate:(id)delegate {
    if (title == nil) {
        title = @"Apologies!";
    }
    if (message == nil) {
        message = @"There is some error. Please try again later.";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:@"Dismiss" otherButtonTitles: nil];
    [alert show];
}

@end
