//
//  ProductWebService.h
//  DepuySynthes
//
//  Created by Akshay Gohel on 28/12/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductWebService : NSObject

typedef void(^CompletionBlock)(id responseData);

-(void) getProductPartWithKey:(NSString*)key byProductID:(NSString*)productID andCompletionBlock:(CompletionBlock) completionBlock;

-(void) getProductCategoryWithKey:(NSString*)key byProductID:(NSString*)productID byPartID:(NSString*)partID andCompletionBlock:(CompletionBlock) completionBlock;

-(void) getSubProductWithKey:(NSString*)key forCategory:(NSString*)categoryID andCompletionBlock:(CompletionBlock) completionBlock;

-(void) getProductInfoWithKey:(NSString*)key byProductID:(NSString*)productID byCategoryID:(NSString*)categoryID andCompletionBlock:(CompletionBlock) completionBlock;

-(void) surveyDetailWithKey:(NSString*)key withType:(NSString*)type withQuizeId:(NSString*)Id withPoints:(NSString*)points withMobile:(NSString*)mobile andCompletionBlock:(CompletionBlock) completionBlock;

-(void) loginWithKey:(NSString*)key withMobile:(NSString*)mobile withUUID:(NSString*)uuid andCompletionBlock:(CompletionBlock) completionBlock;

-(void) registerWithKey:(NSString*)key withMobile:(NSString*)mobile withUUID:(NSString*)uuid withHospital:(NSString*)hospital withEmail:(NSString*)email
     withName:(NSString*)name withCity:(NSString*)city andCompletionBlock:(CompletionBlock) completionBlock;

-(void) registeForArticleWithKey:(NSString*)key withMobile:(NSString*)mobile withUUID:(NSString*)uuid withEmail:(NSString*)email
               withName:(NSString*)name withUserName:(NSString*)userName andCompletionBlock:(CompletionBlock) completionBlock;

@end
