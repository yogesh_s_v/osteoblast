//
//  ProductWebService.m
//  DepuySynthes
//
//  Created by Akshay Gohel on 28/12/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import "ProductWebService.h"
#import <UIKit/UIKit.h>
#import "Constants.h"

#define TIME_OUT_INTERVAL 30.0f

@interface ProductWebService () <NSURLConnectionDataDelegate> {
    NSMutableData *responseData;
    NSURLConnection *connection;
    NSTimer *timer;
}

@property(nonatomic, copy) CompletionBlock completionBlock;

@end

@implementation ProductWebService

-(void) getProductPartWithKey:(NSString*)key byProductID:(NSString*)productID andCompletionBlock:(CompletionBlock) completionBlock {
    self.completionBlock = completionBlock;
    // create request
    
    NSString *urlString = [NSString stringWithFormat:@"http://marathmola.com/feed-login/?key=%@&type=parts", key];
    if (productID && ![productID isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&id=%@", urlString, productID];
    }
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_INTERVAL];
    
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    //    CFRunLoopRun();
    if (connection) {
        timer = [NSTimer scheduledTimerWithTimeInterval:TIME_OUT_INTERVAL target:self selector:@selector(requestTimedOut) userInfo:nil repeats:NO];
        // response data of the request
    }
}

-(void) getProductCategoryWithKey:(NSString*)key byProductID:(NSString*)productID byPartID:(NSString*)partID andCompletionBlock:(CompletionBlock) completionBlock {
    self.completionBlock = completionBlock;
    // create request
    
    NSString *urlString = [NSString stringWithFormat:@"http://marathmola.com/feed-login/?key=%@&type=procat", key];
    if (productID && ![productID isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&id=%@", urlString, productID];
    }
    if (partID && ![partID isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&partid=%@", urlString, partID];
    }
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_INTERVAL];
    
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    //    CFRunLoopRun();
    if (connection) {
        timer = [NSTimer scheduledTimerWithTimeInterval:TIME_OUT_INTERVAL target:self selector:@selector(requestTimedOut) userInfo:nil repeats:NO];
        // response data of the request
    }
}

-(void) getSubProductWithKey:(NSString*)key forCategory:(NSString*)categoryID andCompletionBlock:(CompletionBlock) completionBlock {
    self.completionBlock = completionBlock;
    // create request
    
    NSString *urlString = [NSString stringWithFormat:@"http://marathmola.com/feed-login/?key=%@&type=subpro", key];
    if (categoryID && ![categoryID isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&catid=%@", urlString, categoryID];
    }
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_INTERVAL];
    
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    //    CFRunLoopRun();
    if (connection) {
        timer = [NSTimer scheduledTimerWithTimeInterval:TIME_OUT_INTERVAL target:self selector:@selector(requestTimedOut) userInfo:nil repeats:NO];
        // response data of the request
    }
}

-(void) getProductInfoWithKey:(NSString*)key byProductID:(NSString*)productID byCategoryID:(NSString*)categoryID andCompletionBlock:(CompletionBlock) completionBlock {
    self.completionBlock = completionBlock;
    // create request
    
    NSString *urlString = [NSString stringWithFormat:@"http://marathmola.com/feed-login/?key=%@&type=prodata", key];
    if (productID && ![productID isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&proid=%@", urlString, productID];
    }
    if (categoryID && ![categoryID isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&catid=%@", urlString, categoryID];
    }
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_INTERVAL];
    
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    //    CFRunLoopRun();
    if (connection) {
        timer = [NSTimer scheduledTimerWithTimeInterval:TIME_OUT_INTERVAL target:self selector:@selector(requestTimedOut) userInfo:nil repeats:NO];
        // response data of the request
    }
}

-(void) surveyDetailWithKey:(NSString*)key withType:(NSString*)type withQuizeId:(NSString*)Id withPoints:(NSString*)points withMobile:(NSString*)mobile andCompletionBlock:(CompletionBlock) completionBlock;{
    self.completionBlock = completionBlock;
    // create request
    
    NSString *urlString = [NSString stringWithFormat:@"http://marathmola.com/feed-login/?key=%@&type=prodata", key];
    if (type && ![type isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&type=%@", urlString, type];
    }
    if (Id && ![Id isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&quizeid=%@", urlString, Id];
    }
    if (points && ![points isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&points=%@", urlString, points];
    }
    if (mobile && ![mobile isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&mobile=%@", urlString, mobile];
    }
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_INTERVAL];
    
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    //    CFRunLoopRun();
    if (connection) {
        timer = [NSTimer scheduledTimerWithTimeInterval:TIME_OUT_INTERVAL target:self selector:@selector(requestTimedOut) userInfo:nil repeats:NO];
        // response data of the request
    }
}

-(void) loginWithKey:(NSString*)key withMobile:(NSString*)mobile withUUID:(NSString*)uuid andCompletionBlock:(CompletionBlock) completionBlock {
    self.completionBlock = completionBlock;
    // create request
    
    NSString *urlString = [NSString stringWithFormat:@"http://marathmola.com/feed-login/?key=%@&type=log&mobile=%@&imei=%@", key, mobile, uuid];
    
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_INTERVAL];
    
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    //    CFRunLoopRun();
    if (connection) {
        timer = [NSTimer scheduledTimerWithTimeInterval:TIME_OUT_INTERVAL target:self selector:@selector(requestTimedOut) userInfo:nil repeats:NO];
        // response data of the request
    }
}

-(void) registerWithKey:(NSString*)key withMobile:(NSString*)mobile withUUID:(NSString*)uuid withHospital:(NSString*)hospital withEmail:(NSString*)email
               withName:(NSString*)name withCity:(NSString*)city andCompletionBlock:(CompletionBlock) completionBlock{
    self.completionBlock = completionBlock;
    // create request
    
    NSString *urlString = [NSString stringWithFormat:@"http://marathmola.com/feed-login/?key=%@&type=regi&mobile=%@&imei=%@", key, mobile, uuid];
    
    if (hospital && ![hospital isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&hospital=%@", urlString, hospital];
    }
    if (email && ![email isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&email=%@", urlString, email];
    }
    if (name && ![name isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&name=%@", urlString, name];
    }
    if (city && ![city isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&city=%@", urlString, city];
    }
    
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_INTERVAL];
    
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    //    CFRunLoopRun();
    if (connection) {
        timer = [NSTimer scheduledTimerWithTimeInterval:TIME_OUT_INTERVAL target:self selector:@selector(requestTimedOut) userInfo:nil repeats:NO];
        // response data of the request
    }
}

-(void) registeForArticleWithKey:(NSString*)key withMobile:(NSString*)mobile withUUID:(NSString*)uuid withEmail:(NSString*)email
                        withName:(NSString*)name withUserName:(NSString*)userName andCompletionBlock:(CompletionBlock) completionBlock {
    self.completionBlock = completionBlock;
    // create request
    
    NSString *urlString = [NSString stringWithFormat:@"http://marathmola.com/feed-login/?key=%@&type=regi&mobile=%@&imei=%@", key, mobile, uuid];
    
    if (email && ![email isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&email=%@", urlString, email];
    }
    if (name && ![name isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&name=%@", urlString, name];
    }
    if (userName && ![userName isEqualToString:@""]) {
        urlString = [NSString stringWithFormat:@"%@&username=%@", urlString, userName];
    }
    
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_INTERVAL];
    
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    //    CFRunLoopRun();
    if (connection) {
        timer = [NSTimer scheduledTimerWithTimeInterval:TIME_OUT_INTERVAL target:self selector:@selector(requestTimedOut) userInfo:nil repeats:NO];
        // response data of the request
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    DLog(@"%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    if (responseData == nil) {
        responseData = [[NSMutableData alloc] initWithData:data];
    } else {
        [responseData appendData:data];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    DLog(@"response: %@", response);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    //    CFRunLoopStop(CFRunLoopGetCurrent());
    [timer invalidate];
    timer = nil;
    DLog(@"connectionDidFinishLoading");
    [self parseLoginResponse];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [timer invalidate];
    timer = nil;
    responseData = nil;
    DLog(@"Error: %@", error.localizedDescription);
    if (self.completionBlock) {
        self.completionBlock(@"Error");
    }
    //    CFRunLoopStop(CFRunLoopGetCurrent());
}

-(void) requestTimedOut {
    DLog(@"Request Timed Out");
    [connection cancel];
    [self showAppologiesAlertWithTitle:nil andMessage:nil andDelegate:nil];
    [self connection:connection didFailWithError:nil];
}

#pragma mark - Login JSON Parsing

-(void) parseLoginResponse {
    @try {
        NSError *error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData
                                                             options:kNilOptions
                                                               error:&error];
        if (self.completionBlock) {
            self.completionBlock(json);
        }
    }
    @catch (NSException *exception) {
        DLog(@"Exception: %@", exception);
        [connection cancel];
        [self showAppologiesAlertWithTitle:nil andMessage:nil andDelegate:nil];
        [self connection:connection didFailWithError:nil];
    }
}

-(void) showAppologiesAlertWithTitle:(NSString*) title andMessage:(NSString*) message andDelegate:(id)delegate {
    if (title == nil) {
        title = @"Apologies!";
    }
    if (message == nil) {
        message = @"There is some error. Please try again later.";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:@"Dismiss" otherButtonTitles: nil];
    [alert show];
}

@end
