//
//  FeedsWebService.h
//  DepuySynthes
//
//  Created by Akshay Gohel on 28/12/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FeedsWebService : NSObject

typedef void(^CompletionBlock)(id responseData);

-(void) getFeedsWithKey:(NSString*)key columns:(NSArray*)columnNameArray byFeedID:(NSString*)feedID byCategoryID:(NSString*)categoryID limit:(int)limit andCompletionBlock:(CompletionBlock) completionBlock;

@end
