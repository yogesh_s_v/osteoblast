//
//  AppDelegate.m
//  DepuySynthes
//
//  Created by cdp on 12/18/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "HomeViewController.h"
#import "CategoryViewController.h"
#import "WEPopoverController.h"
#import "MenuViewController.h"
#import "TermsAndConditionsViewController.h"
#import "SecondLaunchImageViewController.h"
#import "ContactUsViewController.h"
#import "RegisterViewController.h"
#import "LaunchViewController.h"
#import "Constants.h"

@interface AppDelegate () <UINavigationControllerDelegate> {
    NSString *notificationID;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
//    LaunchViewController *launchViewController = [[LaunchViewController alloc]initWithNibName:@"LaunchViewController" bundle:nil];
//    self.window.rootViewController = launchViewController;
    
//    self.window.rootViewController.view.transform = CGAffineTransformMakeScale(0.01, 0.01);
//    [UIView animateWithDuration:0.6 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
//        // animate it to the identity transform (100% scale)
//        self.window.rootViewController.view.transform = CGAffineTransformIdentity;
//    } completion:^(BOOL finished){
//        [self performSelector:@selector(setLaunchScreenAsRootView) withObject:nil afterDelay:2.0f];
//    }];
    
//    [self addBounceAnnimationToView:self.window.rootViewController.view];
    [self setLoginScreen:NO];
    
    
    self.selectedMenu = DPMenuNone;
    
    [self.window makeKeyAndVisible];
//    [self performSelector:@selector(setLoginScreenAsRootView) withObject:nil afterDelay:14.0f];
//    [self performSelector:@selector(setLaunchScreenAsRootView) withObject:nil afterDelay:14.0f];
    [COMMON_SETTINGS loadInfo];
    
    NSArray *fontFamilies = [UIFont familyNames];
    
    for (int i = 0; i < [fontFamilies count]; i++)
    {
        NSString *fontFamily = [fontFamilies objectAtIndex:i];
        NSArray *fontNames = [UIFont fontNamesForFamilyName:[fontFamilies objectAtIndex:i]];
        NSLog (@"%@: %@", fontFamily, fontNames);
    }
    
    return YES;
}

-(void)setLaunchScreenAsRootView {
    [self setLaunchScreen:YES];
}

-(void) setLaunchScreen:(BOOL)animated {
    SecondLaunchImageViewController *secondLaunchImageViewController = [[SecondLaunchImageViewController alloc]initWithNibName:@"SecondLaunchImageViewController" bundle:nil];
    
    if (animated) {
        secondLaunchImageViewController.modalPresentationStyle = UIModalPresentationCustom;
        secondLaunchImageViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self.window.rootViewController presentViewController:secondLaunchImageViewController animated:YES completion:^{
            self.window.rootViewController = secondLaunchImageViewController;
        }];
    } else {
        self.window.rootViewController = secondLaunchImageViewController;
    }
    
    [self performSelector:@selector(setLoginScreenAsRootView) withObject:nil afterDelay:3.0f];
}

-(void)setLoginScreenAsRootView {
    [self setLoginScreen:YES];
}

-(void) setLoginScreen:(BOOL)animated {
    LoginViewController *loginViewController = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:[NSBundle mainBundle]];
    
    UINavigationController *navigationViewController = [[UINavigationController alloc]initWithRootViewController:loginViewController];
    navigationViewController.navigationBar.barTintColor = [UIColor colorWithRed:46.0/255.0 green:46.0/255.0 blue:46.0/255.0 alpha:1.0];
    navigationViewController.delegate = self;
    
    CGRect frame = self.window.frame;
    frame.origin.y = frame.size.height-40.0f;
    frame.size.height = 40.0f;
    self.bottomFooter = [[UIView alloc] initWithFrame:frame];
    self.bottomFooter.backgroundColor = [UIColor colorWithRed:0.0f green:54.0f/255.0f blue:98.0f/255.0f alpha:1.0f];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 0.0f, 155.0f, 40.0f)];
    titleLabel.text = @"Johnson & Johnson";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont systemFontOfSize:13.0f];
    [self.bottomFooter addSubview:titleLabel];
    
    UIButton *privacyPolicyButton = [[UIButton alloc] initWithFrame:CGRectMake(self.window.frame.size.width-160.0f, 0.0f, 140.0f, 40.0f)];
    [privacyPolicyButton addTarget:self action:@selector(privacyPolicyTapped) forControlEvents:UIControlEventTouchUpInside];
    [privacyPolicyButton setTitle:@"Terms & Conditions" forState:UIControlStateNormal];
    privacyPolicyButton.titleLabel.font = [UIFont systemFontOfSize:11.0f];
    privacyPolicyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self.bottomFooter addSubview:privacyPolicyButton];
    
    [navigationViewController.view addSubview:self.bottomFooter];
    
    if (animated) {
        navigationViewController.modalPresentationStyle = UIModalPresentationCustom;
        navigationViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self.window.rootViewController presentViewController:navigationViewController animated:YES completion:^{
            self.window.rootViewController = navigationViewController;
        }];
    } else {
        self.window.rootViewController = navigationViewController;
    }
}

-(void) setHomeScreen:(BOOL)animated {
    //    LoginViewController *loginViewController = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:[NSBundle mainBundle]];
    HomeViewController *homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    
    UINavigationController *navigationViewController = [[UINavigationController alloc]initWithRootViewController:homeViewController];
    navigationViewController.navigationBar.barTintColor = [UIColor colorWithRed:46.0/255.0 green:46.0/255.0 blue:46.0/255.0 alpha:1.0];
    //    navigationViewController.navigationBar.tintColor = [UIColor whiteColor];
    navigationViewController.delegate = self;
    
    CGRect frame = self.window.frame;
    frame.origin.y = frame.size.height-40.0f;
    frame.size.height = 40.0f;
    self.bottomFooter = [[UIView alloc] initWithFrame:frame];
    self.bottomFooter.backgroundColor = [UIColor colorWithRed:0.0f green:54.0f/255.0f blue:98.0f/255.0f alpha:1.0f];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 0.0f, 155.0f, 40.0f)];
    titleLabel.text = @"Johnson & Johnson";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont systemFontOfSize:13.0f];
    [self.bottomFooter addSubview:titleLabel];
    
    UIButton *privacyPolicyButton = [[UIButton alloc] initWithFrame:CGRectMake(self.window.frame.size.width-160.0f, 0.0f, 140.0f, 40.0f)];
    [privacyPolicyButton addTarget:self action:@selector(privacyPolicyTapped) forControlEvents:UIControlEventTouchUpInside];
    [privacyPolicyButton setTitle:@"Terms & Conditions" forState:UIControlStateNormal];
    privacyPolicyButton.titleLabel.font = [UIFont systemFontOfSize:11.0f];
    privacyPolicyButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self.bottomFooter addSubview:privacyPolicyButton];
    
    [navigationViewController.view addSubview:self.bottomFooter];
    
    if (animated) {
        navigationViewController.modalPresentationStyle = UIModalPresentationCustom;
        navigationViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self.window.rootViewController presentViewController:navigationViewController animated:YES completion:^{
            self.window.rootViewController = navigationViewController;
        }];
    } else {
        self.window.rootViewController = navigationViewController;
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = oneEvent.userInfo;
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"uid"]];
        if ([uid isEqualToString:self.uidToDelete])
        {
            //Cancelling local notification
            [app cancelLocalNotification:oneEvent];
            break;
        }
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - UINavigationController Delegate

-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (![viewController isKindOfClass:[HomeViewController class]] && ![viewController isKindOfClass:[LoginViewController class]]) {
        UIButton *newBackButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 20.0f, 30.0f)];
        [newBackButton setImage:[UIImage imageNamed:@"BackImage"] forState:UIControlStateNormal];
//        newBackButton.imageEdgeInsets = UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
        [newBackButton addTarget:self action:@selector(popToPreviousViewController) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithCustomView:newBackButton];
        
        UIButton *leftLogoButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 44.0f, 44.0f)];
        //    [leftLogoButton addTarget:self action:@selector(showLeftPane) forControlEvents:UIControlEventTouchUpInside];
        leftLogoButton.imageEdgeInsets = UIEdgeInsetsMake(5.0f, 5.0f, 5.0f, 5.0f);
        leftLogoButton.userInteractionEnabled = NO;
        [leftLogoButton setImage:[UIImage imageNamed:@"OsteoBlastLogo"] forState:UIControlStateNormal];
        UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftLogoButton];
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSpacer.width -= 16.0f;
        viewController.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects: leftBarButton, negativeSpacer, leftBarButtonItem, nil];
    }else {
        UIButton *leftLogoButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 44.0f, 44.0f)];
        //    [leftLogoButton addTarget:self action:@selector(showLeftPane) forControlEvents:UIControlEventTouchUpInside];
        leftLogoButton.imageEdgeInsets = UIEdgeInsetsMake(5.0f, 5.0f, 5.0f, 5.0f);
        leftLogoButton.userInteractionEnabled = NO;
        [leftLogoButton setImage:[UIImage imageNamed:@"OsteoBlastLogo"] forState:UIControlStateNormal];
        UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftLogoButton];
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSpacer.width -= 16.0f;
        viewController.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects: negativeSpacer, leftBarButtonItem, nil];
    }
    
    if (![viewController isKindOfClass:[LoginViewController class]] && ![viewController isKindOfClass:[RegisterViewController class]]) {
        UIButton *rightHomeButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 35.0f, 35.0f)];
        [rightHomeButton addTarget:self action:@selector(popToRootViewController) forControlEvents:UIControlEventTouchUpInside];
        //    rightHomeButton.imageEdgeInsets = UIEdgeInsetsMake(12.0f, 12.0f, 12.0f, 12.0f);
        [rightHomeButton setImage:[UIImage imageNamed:@"MenuHomeImage"] forState:UIControlStateNormal];
        UIBarButtonItem *homeBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightHomeButton];
        
        UIButton *rightProductButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 30.0f, 30.0f)];
        [rightProductButton addTarget:self action:@selector(menuButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        rightProductButton.imageEdgeInsets = UIEdgeInsetsMake(4.0f, 4.0f, 4.0f, 4.0f);
        [rightProductButton setImage:[UIImage imageNamed:@"MenuProductImage"] forState:UIControlStateNormal];
        UIBarButtonItem *productBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightProductButton];
        
        viewController.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects: productBarButtonItem, homeBarButtonItem, nil];
    }
    
//    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 280.0f, 44.0f)];
//    
//    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-5.0f, 2.0f, 155.0f, 42.0f)];
//    titleLabel.text = @"DePuy Synthes";
//    titleLabel.textColor = [UIColor whiteColor];
//    titleLabel.font = [UIFont systemFontOfSize:15.0f];
//    [titleView addSubview:titleLabel];
//    viewController.navigationItem.titleView = titleView;
}

#pragma mark - 

-(void) popToRootViewController {
    UIViewController *vC = self.window.rootViewController;
    if ([vC isKindOfClass:[UINavigationController class]]) {
        [((UINavigationController*)vC) popToRootViewControllerAnimated:YES];
        self.selectedMenu = DPMenuNone;
        UIViewController *viewController = ((UINavigationController*)vC).viewControllers.firstObject;
        if (viewController && [viewController isKindOfClass:[HomeViewController class]]) {
            [((HomeViewController*)viewController) initializeDataAndView];
        }
    }
}

-(void) popToPreviousViewController {
    UIViewController *vC = self.window.rootViewController;
    if ([vC isKindOfClass:[UINavigationController class]]) {
        [((UINavigationController*)vC) popViewControllerAnimated:YES];
    }
    
}

-(void) menuButtonTapped {
    UIViewController *vC = self.window.rootViewController;
    if ([vC isKindOfClass:[UINavigationController class]]) {
        if (self.popoverController.popoverVisible) {
            [self.popoverController dismissPopoverAnimated:YES];
            if (self.selectedMenu != DPMenuContactUs) {
                if (self.selectedMenu == DPMenuProducts) {
                    CategoryViewController *categoryViewController = nil;
                    for (UIViewController *viewController in ((UINavigationController*)vC).viewControllers) {
                        if ([viewController isKindOfClass:[CategoryViewController class]]) {
                            categoryViewController = (CategoryViewController*)viewController;
                            break;
                        }
                    }
                    if (categoryViewController == nil) {
                        categoryViewController = [[CategoryViewController alloc] initWithNibName:@"CategoryViewController" bundle:nil];
                        [((UINavigationController*)vC) pushViewController:categoryViewController animated:YES];
                    } else {
                        [((UINavigationController*)vC) popToViewController:categoryViewController animated:YES];
                    }
                }else {
                    UIViewController *presentViewController = ((UINavigationController*)vC).viewControllers.lastObject;
                    if (![presentViewController isKindOfClass:[HomeViewController class]]) {
                        [((UINavigationController*)vC) popToRootViewControllerAnimated:YES];
                    }else {
                        [((HomeViewController*)presentViewController) initializeDataAndView];
                    }
                }
            } else {
                for (UIViewController *viewController in ((UINavigationController*)vC).viewControllers) {
                    if ([viewController isKindOfClass:[ContactUsViewController class]]) {
                        return;
                    }
                }
                ContactUsViewController *contactUsViewController = [[ContactUsViewController alloc]initWithNibName:@"ContactUsViewController" bundle:nil];
                [((UINavigationController*)vC) pushViewController:contactUsViewController animated:YES];
            }
        } else {
            self.popoverController.backgroundView.layer.borderWidth = 1.0f;
            self.popoverController.backgroundView.layer.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.4f].CGColor;
            MenuViewController *contentViewController = [[MenuViewController alloc]initWithNibName:@"MenuViewController" bundle:nil];
            self.popoverController = [[WEPopoverController alloc] initWithContentViewController:contentViewController];
            [self.popoverController presentPopoverFromRect:CGRectMake(self.window.frame.size.width-50.0f ,30.0f ,30.0f ,30.0f) inView:self.window.rootViewController.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        }
    }
}

-(void) privacyPolicyTapped {
    UIViewController *vC = self.window.rootViewController;
    if ([vC isKindOfClass:[UINavigationController class]]) {
        for (UIViewController *viewController in ((UINavigationController*)vC).viewControllers) {
            if ([viewController isKindOfClass:[TermsAndConditionsViewController class]]) {
                return;
            }
        }
        TermsAndConditionsViewController *termsAndConditionsViewController = [[TermsAndConditionsViewController alloc]initWithNibName:@"TermsAndConditionsViewController" bundle:nil];
//        [((UINavigationController*)vC) presentViewController:termsAndConditionsViewController animated:YES completion:nil];
        [((UINavigationController*)vC) pushViewController:termsAndConditionsViewController animated:YES];
    }
}

//- (void)addBounceAnnimationToView:(UIView *)view
//{
//    view.alpha = 1.0f;
//    CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
//
//    bounceAnimation.values = @[@(0.0), @(0.1), @(0.2), @(0.3), @(0.4), @(0.5), @(0.6), @(0.7), @(0.8), @(0.9), @(1)];
//
//    bounceAnimation.duration = 0.6;
//    NSMutableArray *timingFunctions = [[NSMutableArray alloc] initWithCapacity:bounceAnimation.values.count];
//    for (NSUInteger i = 0; i < bounceAnimation.values.count; i++) {
//        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
//    }
//    [bounceAnimation setTimingFunctions:timingFunctions.copy];
//    bounceAnimation.removedOnCompletion = NO;
//
//    [view.layer addAnimation:bounceAnimation forKey:@"bounce"];
//}


@end
