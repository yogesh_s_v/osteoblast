//
//  OrganInfoViewController.h
//  DepuySynthes
//
//  Created by cdp on 12/25/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrganInfoViewController : UIViewController

@property (nonatomic, strong) NSMutableArray *organInfoArray;

@end
