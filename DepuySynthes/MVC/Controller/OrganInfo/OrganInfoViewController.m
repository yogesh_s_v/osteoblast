//
//  OrganInfoViewController.m
//  DepuySynthes
//
//  Created by cdp on 12/25/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import "OrganInfoViewController.h"
#import "AsyncImageView.h"
#import "CustomScrollerViewController.h"
#import "Constants.h"

@interface OrganInfoViewController () <CustomScrollerViewControllerDelegate,UIWebViewDelegate>
@property (nonatomic, strong) CustomScrollerViewController *customScrollerViewController;
@property (weak, nonatomic) IBOutlet UIScrollView *organInfoScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *organInfoImageView;
@property (weak, nonatomic) IBOutlet UILabel *organInfoHeadingLabel;
@property (weak, nonatomic) IBOutlet UIView *organInfoView;
@property (weak, nonatomic) IBOutlet UILabel *documentLabel;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end


@implementation OrganInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.documentLabel.hidden = YES;
    self.customScrollerViewController = [[CustomScrollerViewController alloc] initWithNibName:@"CustomScrollerViewController" bundle:nil];
    self.customScrollerViewController.delegate = self;
    
    CGRect frame = CGRectMake(0.0f, 66.0f, APP_DELEGATE.window.frame.size.width, 40.0f);
    self.customScrollerViewController.view.frame = frame;
    
    self.customScrollerViewController.selectedColor = [UIColor colorWithRed:28.0f/255.0f green:117.0f/255.0f blue:188.0f/255.0f alpha:1.0f];
    self.customScrollerViewController.defaultColor = [UIColor colorWithRed:28.0f/255.0f green:117.0f/255.0f blue:188.0f/255.0f alpha:1.0f];
    self.customScrollerViewController.markerColor = [UIColor colorWithRed:28.0f/255.0f green:117.0f/255.0f blue:188.0f/255.0f alpha:1.0f];
    self.customScrollerViewController.showSeparator = NO;
    
    [self.view addSubview:self.customScrollerViewController.view];
    
    NSMutableArray *listOfItems = [[NSMutableArray alloc]init];
    for (int i = 0; i < self.organInfoArray.count; i++) {
        id object = [self.organInfoArray objectAtIndex:i];
        if (object && [object isKindOfClass:[NSDictionary class]]) {
            id item_title = [object objectForKey:@"item_title"];
            if (item_title && [item_title isKindOfClass:[NSString class]]) {
                [listOfItems addObject:item_title];
            }
        }
    }
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.customScrollerViewController.listofItems = listOfItems;
    [self.customScrollerViewController reloadData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CustomScrollerViewControllerDelegate

-(void) customScrollerViewControllerDidSelectItem:(id)item atIndex:(int)index {
    DLog(@"%@", item);
    DLog(@"%d", index);
    for (UIView *view in self.organInfoView.subviews) {
        [view removeFromSuperview];
    }
    id primaryKnee = [self.organInfoArray objectAtIndex:index];

    if (primaryKnee && [primaryKnee isKindOfClass:[NSDictionary class]]) {
        for (UIView *view in self.organInfoScrollView.subviews) {
            if ([view isKindOfClass:[UIWebView class]] && view != self.webView) {
                [view removeFromSuperview];
            }
        }
        if ([primaryKnee objectForKey:@"item_title"] &&![[primaryKnee objectForKey:@"item_title"] isEqualToString:@"Documentation"]) {
            self.documentLabel.hidden = YES;
            id item_image = [primaryKnee objectForKey:@"item_images"];
            if (item_image && [item_image isKindOfClass:[NSString class]]) {
                [((AsyncImageView*)self.organInfoImageView) setImageURL:[NSURL URLWithString:item_image]];
            }
            id item_title = [primaryKnee objectForKey:@"item_title"];
            if (item_title && [item_title isKindOfClass:[NSString class]]) {
                self.organInfoHeadingLabel.text = item_title;
            }
            id item_data = [primaryKnee objectForKey:@"item_data"];
            if (item_data && [item_data isKindOfClass:[NSString class]]) {
//                if ([((NSString*)item_data) rangeOfString:@"<div"].location != NSNotFound || [((NSString*)item_data) rangeOfString:@"<p"].location != NSNotFound) {
//                    [self.webView loadHTMLString:item_data baseURL:nil];
                    self.webView.hidden = NO;
                    self.organInfoView.hidden = YES;
//                } else {
//                    self.webView.hidden = YES;
//                    self.organInfoView.hidden = NO;
                
//                    NSMutableAttributedString *itemDataHtml = [[NSMutableAttributedString alloc] initWithData:[item_data dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
//                    
//                    item_data = itemDataHtml.string;
//                    
//                    NSDictionary *organInfoAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:13.0f], NSForegroundColorAttributeName : [UIColor blackColor]};
//                    CGSize size = [item_data boundingRectWithSize:CGSizeMake(self.view.frame.size.width-20.0f, 10000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:organInfoAttr context:nil].size;
//                    
//                    UILabel *organInfoLable = [[UILabel alloc] init];
//                    organInfoLable.numberOfLines = 0;
//                    organInfoLable.autoresizingMask = self.organInfoView.autoresizingMask;
//                    organInfoLable.font = [UIFont systemFontOfSize:13.0f];
//                    organInfoLable.textColor = [UIColor blackColor];
//                    
//                    CGRect frame = self.organInfoView.frame;
//                    frame.size.height = size.height+10.0f;
//                    self.organInfoView.frame = frame;
//                    [self.organInfoView addSubview:organInfoLable];
//                    
//                    frame.size.height = size.height;
//                    organInfoLable.frame = CGRectMake(0.0f, 0.0f, frame.size.width, frame.size.height);
//                    organInfoLable.text = item_data;
//                }
                
                NSString *itemTitle = @"";
                id item_title = [primaryKnee objectForKey:@"item_title"];
                if (item_title && [item_title isKindOfClass:[NSString class]]) {
                    itemTitle = [NSString stringWithFormat:@"<h2>%@<h2><hr>", item_title];
                }
                
                NSString *imageTag = @"";
                id item_image = [primaryKnee objectForKey:@"item_images"];
                if (item_image && [item_image isKindOfClass:[NSString class]]) {
                    imageTag = [NSString stringWithFormat:@"<img src='%@'/ style='max-width: 100%%'>", item_image];
                }
                
                NSString *htmlString = [NSString stringWithFormat:@"<font face='Helvetica Bold' size='3'>%@%@<font face='Helvetica Light' size='3'>%@", itemTitle, imageTag, item_data];
                [self.webView loadHTMLString:htmlString baseURL:nil];
            }
//            self.organInfoScrollView.contentSize = CGSizeMake(320.0f, self.organInfoView.frame.origin.y+self.organInfoView.frame.size.height+70.0f);
        }else {
            id item_title = [primaryKnee objectForKey:@"item_title"];
            if (item_title && [item_title isKindOfClass:[NSString class]]) {
                self.organInfoImageView.image = nil;
                self.organInfoHeadingLabel.text = nil;
                self.documentLabel.hidden = NO;
//                NSDictionary *organInfoAttr = @{ NSFontAttributeName : [UIFont boldSystemFontOfSize:16.0f], NSForegroundColorAttributeName : [UIColor colorWithRed:0.0f green:54.0f/255.0f blue:98.0f/255.0f alpha:1.0f]};
//                CGSize size = [item_title boundingRectWithSize:CGSizeMake(self.view.frame.size.width-20.0f, 10000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:organInfoAttr context:nil].size;
//                
//                UILabel *organInfoLable = [[UILabel alloc] init];
//                organInfoLable.numberOfLines = 0;
//                organInfoLable.autoresizingMask = UIViewAutoresizingNone;
//                organInfoLable.font = [UIFont systemFontOfSize:16.0f];
//                organInfoLable.textColor = [UIColor blackColor];
//                organInfoLable.text = item_title;
//                
//                CGRect frame = self.organInfoScrollView.frame;
//                frame.size.height = size.height;
//                frame.origin.x = 10.0f;
//                organInfoLable.frame = frame;
//                [self.organInfoScrollView addSubview:organInfoLable];
                
                id item_data = [primaryKnee objectForKey:@"item_data"];
//                CGRect frame1 = self.organInfoScrollView.frame;
//                frame1.origin.y = self.documentLabel.frame.size.height;
//                frame1.size.height -= self.documentLabel.frame.size.height;
//                UIWebView *contentWebView = [[UIWebView alloc]initWithFrame:frame1];
                self.webView.hidden = NO;
                self.organInfoView.hidden = YES;
                if (item_data && [item_data isKindOfClass:[NSString class]]) {
                    NSString *itemTitle = @"";
                    id item_title = [primaryKnee objectForKey:@"item_title"];
                    if (item_title && [item_title isKindOfClass:[NSString class]]) {
                        itemTitle = [NSString stringWithFormat:@"<h2>%@<h2><hr>", item_title];
                    }
                    
                    NSString *imageTag = @"";
                    id item_image = [primaryKnee objectForKey:@"item_images"];
                    if (item_image && [item_image isKindOfClass:[NSString class]]) {
                        imageTag = [NSString stringWithFormat:@"<img src='%@'/ style='max-width: 100%%'>", item_image];
                    }
                    
                    NSString *htmlString = [NSString stringWithFormat:@"%@%@%@", itemTitle, imageTag, item_data];
                    [self.webView loadHTMLString:htmlString baseURL:nil];
                }
//                contentWebView.delegate = self;
//                contentWebView.autoresizingMask = UIViewAutoresizingNone;
//                [self.organInfoScrollView addSubview:contentWebView];
//                self.organInfoScrollView.contentSize = CGSizeMake(APP_DELEGATE.window.frame.size.width, contentWebView.frame.size.height+self.documentLabel.frame.size.height);
            }
        }
    }
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    DLog(@"request.URL.absoluteString:%@", request.URL.absoluteString);
    if (![request.URL.absoluteString isEqualToString:@"about:blank"]) {
        [[UIApplication sharedApplication] openURL:request.URL];
        return NO;
    }
    return YES;
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    DLog(@"Failed: %@", error);
}

@end
