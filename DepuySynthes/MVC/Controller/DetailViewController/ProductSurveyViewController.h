//
//  ProductSurveyViewController.h
//  DepuySynthes
//
//  Created by Akshay Gohel on 09/01/15.
//  Copyright (c) 2015 The_world_inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductSurveyViewController : UIViewController

@property(nonatomic, strong) NSDictionary *surveyDetails;

@end
