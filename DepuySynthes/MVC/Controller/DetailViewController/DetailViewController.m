//
//  DetailViewController.m
//  DepuySynthes
//
//  Created by cdp on 12/21/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController
@synthesize detailViewTitleString,detailHeadingLabel,conferenceDateLabel,venueLabel,timingLabel;
@synthesize venueString,timingString,dateString;
@synthesize detailViewDescriptionWebView,scrollView;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"DePuy Synthes";
    [self initializeDataAndView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initializeDataAndView
{
    detailHeadingLabel.text = detailViewTitleString;
    conferenceDateLabel.text = [NSString stringWithFormat:@"Conerence Date : %@",dateString];
    timingLabel.text = [NSString stringWithFormat:@"Timings : %@",timingString];
    venueLabel.text = [NSString stringWithFormat:@"Venue : %@",venueString];
    scrollView.contentSize = CGSizeMake(320.0f, 500.0f);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
