//
//  ProductSurveyDetailViewController.h
//  DepuySynthes
//
//  Created by Akshay Gohel on 09/01/15.
//  Copyright (c) 2015 The_world_inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductSurveyDetailViewController : UIViewController

@property (nonatomic, strong) NSDictionary *surveyDict;

@property (nonatomic, strong) NSString *ratingString;

@property (nonatomic) BOOL isPercentageReport;

@end
