//
//  ProductSurveyViewController.m
//  DepuySynthes
//
//  Created by Akshay Gohel on 09/01/15.
//  Copyright (c) 2015 The_world_inc. All rights reserved.
//

#import "ProductSurveyViewController.h"
#import "Constants.h"
#import "ProductSurveyDetailViewController.h"
#import "RatingView.h"
#import "ProductWebService.h"

@interface ProductSurveyViewController ()<RatingViewDelegate, UITextViewDelegate> {
    BOOL keyboardAppeared;
    CGRect keyboardframe;
}
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *sliderContainerView;
@property (weak, nonatomic) IBOutlet UISlider *percentageSlider;
@property (weak, nonatomic) IBOutlet UIView *buttonContainerView;
@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@property (weak, nonatomic) IBOutlet UIButton *noButton;
@property (weak, nonatomic) IBOutlet UIButton *submittButton;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;

@property (weak, nonatomic) IBOutlet UIView *textResponseContainerView;
@property (weak, nonatomic) IBOutlet UIView *textContainerView;
@property (weak, nonatomic) IBOutlet UITextView *responseTextView;

@property (strong, nonatomic) IBOutlet UIToolbar *keyboardToolbar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@property (strong, nonatomic) RatingView *ratingView;

@end

@implementation UIView (FindViewThatIsFirstResponder)

- (UIView *)findViewThatIsFirstResponder
{
    if (self.isFirstResponder) {
        return self;
    }
    
    for (UIView *subView in self.subviews) {
        UIView *firstResponder = [subView findViewThatIsFirstResponder];
        if (firstResponder != nil) {
            return firstResponder;
        }
    }
    return nil;
}

@end

@implementation ProductSurveyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.sliderContainerView.hidden = YES;
    self.buttonContainerView.hidden = YES;
    self.ratingView.hidden = YES;
    self.submittButton.hidden = YES;
    self.textResponseContainerView.hidden = YES;
    self.submittButton.layer.cornerRadius = 2.0f;
    self.containerView.layer.cornerRadius = 2.0f;
    self.containerView.superview.backgroundColor = [UIColor colorWithRed:0.0f green:54.0f/255.0f blue:98.0f/255.0f alpha:1.0f];
    self.yesButton.selected = YES;
    [self.keyboardToolbar removeFromSuperview];
    
    self.textContainerView.layer.cornerRadius = 4.0f;
    self.textContainerView.layer.borderColor = [UIColor blackColor].CGColor;
    self.textContainerView.layer.borderWidth = 1.0f;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 3.2) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidAppear:)
                                                     name:UIKeyboardDidShowNotification
                                                   object:nil];
    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidAppear:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidDisappear:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    keyboardAppeared = YES;
    
    [self loadSurveyDetails];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadSurveyDetails {
    float y = 20.0f;
    
    id catTitle = [self.surveyDetails objectForKey:@"feed_title"];
    if (catTitle && [catTitle isKindOfClass:[NSString class]]) {
        NSDictionary *titleAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(13.0f)]};
        CGSize size = [catTitle boundingRectWithSize:CGSizeMake(self.sliderContainerView.frame.size.width-32.0f, 2000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:titleAttr context:nil].size;
        
        UILabel *categoryNewsHeadingLabel = [[UILabel alloc] init];
        categoryNewsHeadingLabel.numberOfLines = 0;
        categoryNewsHeadingLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(13.0f)];
        categoryNewsHeadingLabel.textColor = [UIColor blackColor];
        [self.containerView addSubview:categoryNewsHeadingLabel];
        
        categoryNewsHeadingLabel.frame = CGRectMake(16.0f, y, self.sliderContainerView.frame.size.width-32.0f, size.height+5.0f);
        categoryNewsHeadingLabel.text = catTitle;
        
        y += size.height+25.0f;
        
    }
    
    id surveyType = [self.surveyDetails objectForKey:@"feed_survey_type"];
    if (surveyType && [surveyType isKindOfClass:[NSString class]]) {
        if ([surveyType integerValue] == 1) {
            self.sliderContainerView.frame = CGRectMake(10.0f, y, self.containerView.frame.size.width-20.0f, 50.0f);
            self.sliderContainerView.hidden = NO;
            self.submittButton.hidden = NO;
            self.buttonContainerView.frame = CGRectMake(10.0f, y+150.0f, self.containerView.frame.size.width-20.0f, 70.0f);
            self.buttonContainerView.hidden = YES;
            self.textResponseContainerView.frame = CGRectMake(10.0f, y+150.0f, self.containerView.frame.size.width-20.0f, 70.0f);
            self.textResponseContainerView.hidden = YES;
            
            y += self.sliderContainerView.frame.size.height+25.0f;
            
            CGRect frame = self.submittButton.frame;
            frame.origin.y = y;
            self.submittButton.frame = frame;
            
            y += self.submittButton.frame.size.height+25.0f;
        }else if ([surveyType integerValue] == 2) {
            self.buttonContainerView.frame = CGRectMake(10.0f, y, self.containerView.frame.size.width-20.0f, 70.0f);
            self.buttonContainerView.hidden = NO;
            self.submittButton.hidden = NO;
            self.sliderContainerView.frame = CGRectMake(10.0f, y+150.0f, self.containerView.frame.size.width-20.0f, 50.0f);
            self.sliderContainerView.hidden = YES;
            self.textResponseContainerView.frame = CGRectMake(10.0f, y+150.0f, self.containerView.frame.size.width-20.0f, 70.0f);
            self.textResponseContainerView.hidden = YES;
            
            y += self.buttonContainerView.frame.size.height+25.0f;
            
            CGRect frame = self.submittButton.frame;
            frame.origin.y = y;
            self.submittButton.frame = frame;
            
            y += self.submittButton.frame.size.height+25.0f;
        }else if ([surveyType integerValue] == 4) {
            self.submittButton.hidden = NO;
            self.buttonContainerView.frame = CGRectMake(10.0f, y+150.0f, self.containerView.frame.size.width-20.0f, 70.0f);
            self.buttonContainerView.hidden = YES;
            self.sliderContainerView.frame = CGRectMake(10.0f, y+150.0f, self.containerView.frame.size.width-20.0f, 50.0f);
            self.sliderContainerView.hidden = YES;
            
            self.textResponseContainerView.frame = CGRectMake(10.0f, y, self.containerView.frame.size.width-20.0f, 40.0f);
            self.textResponseContainerView.hidden = NO;
            
//            self.ratingView.hidden = NO;
//            self.ratingView = [[RatingView alloc]initWithFrame:CGRectMake(10.0f, y, self.containerView.frame.size.width-20.0f, 40.0f)
//                                             selectedImageName:@"selected.png"
//                                               unSelectedImage:@"unSelected.png"
//                                                      minValue:0
//                                                      maxValue:5
//                                                 intervalValue:0.25
//                                                    stepByStep:YES];
//            self.ratingView.delegate = self;
//            [self.containerView addSubview:self.ratingView];
//            
            y += self.textResponseContainerView.frame.size.height+45.0f;
            
            CGRect frame = self.submittButton.frame;
            frame.origin.y = y;
            self.submittButton.frame = frame;
            
            y += self.submittButton.frame.size.height+25.0f;
            
        }
    }
    
    CGRect frame = self.containerView.frame;
    frame.size.height = y;
    frame.origin.y = ((self.view.frame.size.height-64.0f)/2)-(y/2);
    self.containerView.frame = frame;
}

#pragma mark - text view events

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    textView.inputAccessoryView = self.keyboardToolbar;
    return YES;
}

#pragma Button Tapped Events

-(IBAction)yesButtonTapped:(id)sender {
    if (self.yesButton.selected) {
        self.yesButton.selected = NO;
    }else {
        self.yesButton.selected = YES;
    }
    if (self.noButton.selected) {
        self.noButton.selected = NO;
    }
}

-(IBAction)noButtonTapped:(id)sender {
    if (self.noButton.selected) {
        self.noButton.selected = NO;
    }else {
        self.noButton.selected = YES;
    }
    if (self.yesButton.selected) {
        self.yesButton.selected = NO;
    }
}

-(IBAction)doneButtonTapped:(id)sender {
    [self.responseTextView resignFirstResponder];
}

-(IBAction)sliderValueChanged:(UISlider*)sender {
    self.ratingLabel.text = [NSString stringWithFormat:@"Rating: %d/100",(int)sender.value];
}

-(IBAction)submitButtonTapped:(id)sender {
    ProductWebService *productWebService = [[ProductWebService alloc]init];
    NSMutableString *quizeIdString;
    NSMutableString *ratingS = [[NSMutableString alloc]initWithString:@""];
    id feedId = [self.surveyDetails objectForKey:@"feed_id"];
    if (feedId && [feedId isKindOfClass:[NSString class]]) {
        quizeIdString = [NSMutableString stringWithFormat:@"%@",feedId];
    }
    ProductSurveyDetailViewController *productSurveyDetailViewController = [[ProductSurveyDetailViewController alloc]initWithNibName:@"ProductSurveyDetailViewController" bundle:nil];
    productSurveyDetailViewController.surveyDict = self.surveyDetails;
    if (!self.sliderContainerView.hidden) {
        productSurveyDetailViewController.isPercentageReport = YES;
        ratingS = [NSMutableString stringWithFormat:@"%d",(int)self.percentageSlider.value];
        [productWebService surveyDetailWithKey:@"password" withType:@"rate" withQuizeId:quizeIdString withPoints:ratingS withMobile:[COMMON_SETTINGS userMobile] andCompletionBlock:^(id responseData) {
            if (responseData && [responseData isKindOfClass:[NSDictionary class]]) {
                DLog(@"responce data is : %@",responseData);
                id responseMessage = [responseData objectForKey:@"response_message"];
                if (responseMessage && [responseMessage isKindOfClass:[NSString class]] && [responseMessage isEqualToString:@"Success"]) {
                    productSurveyDetailViewController.ratingString = [NSString stringWithFormat:@"%d",(int)self.percentageSlider.value];
                    [self.navigationController pushViewController:productSurveyDetailViewController animated:YES];
                }else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Survey Alert" message:responseMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alert show];
                }
            }
        }];
    }else if (!self.buttonContainerView.hidden) {
        productSurveyDetailViewController.isPercentageReport = NO;
        if (self.yesButton.selected) {
            [ratingS appendString:@"1"];
//            productSurveyDetailViewController.ratingString = @"YES";
        }else if (self.noButton.selected) {
            [ratingS appendString:@"0"];
//            productSurveyDetailViewController.ratingString = @"NO";
        }
        [productWebService surveyDetailWithKey:@"password" withType:@"rate" withQuizeId:quizeIdString withPoints:ratingS withMobile:[COMMON_SETTINGS userMobile] andCompletionBlock:^(id responseData) {
            if (responseData && [responseData isKindOfClass:[NSDictionary class]]) {
                DLog(@"responce data is : %@",responseData);
                id responseMessage = [responseData objectForKey:@"response_message"];
                if (responseMessage && [responseMessage isKindOfClass:[NSString class]] && [responseMessage isEqualToString:@"Success"]) {
//                    id data = [responseData objectForKey:@"data"];
                    if (ratingS) {
                        NSString *dataString;
                        if ([ratingS isEqualToString:@"1"]) {
                            dataString = @"YES";
                        }else if ([ratingS isEqualToString:@"0"]){
                            dataString = @"NO";
                        }
                        if (![dataString isEqualToString:@""]) {
                            productSurveyDetailViewController.ratingString = dataString;
                            [self.navigationController pushViewController:productSurveyDetailViewController animated:YES];
                        }
                    }
                }else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Survey Alert" message:responseMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alert show];
                }
            }
        }];
    }else if (!self.textResponseContainerView.hidden) {
        if (self.responseTextView.text.length == 0) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please enter your feedback.." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            return;
        }
        productSurveyDetailViewController.isPercentageReport = YES;
        productSurveyDetailViewController.ratingString = [NSString stringWithFormat:@"%@",self.responseTextView.text];
        self.responseTextView.text = @"";
//        [self.navigationController pushViewController:productSurveyDetailViewController animated:YES];
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Thanks for submitting your feedback.." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }

}

#pragma mark RatingView delegate

- (void)rateChanged:(RatingView *)ratingView
{
    DLog(@"rate value = %f", ratingView.value);
    
}

#pragma mark - keydoard methods

-(void)keyboardDidAppear:(NSNotification *)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    if (keyboardAppeared) {
        keyboardAppeared = NO;
        [UIView animateWithDuration:0.3f animations:^{
            
            CGRect frame = self.containerView.frame;
            frame.origin.y = APP_DELEGATE.window.frame.size.height-(keyboardFrameBeginRect.size.height+self.containerView.frame.size.height)-60.0f;
            
            [self.containerView setFrame:frame];
        } completion: ^ (BOOL finished)
         {
         }];
    }else {
        if (keyboardFrameBeginRect.size.height < keyboardframe.size.height) {
            float diffheight= (keyboardframe.size.height - keyboardFrameBeginRect.size.height)-60.0f;
            [UIView animateWithDuration:0.3f animations:^{
                
                CGRect frame = self.containerView.frame;
                frame.origin.y += diffheight;
                [self.containerView setFrame:frame];
            } completion: ^ (BOOL finished)
             {
             }];
        }else if (keyboardFrameBeginRect.size.height > keyboardframe.size.height) {
            float diffheight= (keyboardFrameBeginRect.size.height - keyboardframe.size.height)-60.0f;
            [UIView animateWithDuration:0.3f animations:^{
                
                CGRect frame = self.containerView.frame;
                frame.origin.y -= diffheight;
                
                [self.containerView setFrame:frame];
            } completion: ^ (BOOL finished)
             {
             }];
        }
    }
    keyboardframe = keyboardFrameBeginRect;
    //    [self addButtonToKeyboard];
}

-(void)keyboardDidDisappear:(NSNotification *)notification
{
    //    NSDictionary* keyboardInfo = [notification userInfo];
    //    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    //    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    keyboardAppeared = YES;
    [UIView animateWithDuration:0.3f animations:^{
        
        CGRect frame = self.containerView.frame;
        frame.origin.y = (APP_DELEGATE.window.frame.size.height/2)-(self.containerView.frame.size.height/2);
        [self.containerView setFrame:frame];
    } completion: ^ (BOOL finished)
     {
     }];
}

@end
