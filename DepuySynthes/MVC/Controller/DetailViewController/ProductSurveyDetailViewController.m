//
//  ProductSurveyDetailViewController.m
//  DepuySynthes
//
//  Created by Akshay Gohel on 09/01/15.
//  Copyright (c) 2015 The_world_inc. All rights reserved.
//

#define COMPUTE_GRAPH_VALUES(givenValue) ((givenValue/finalValue)*100.0f)

#import "ProductSurveyDetailViewController.h"
#import "Constants.h"
#import "XYPieChart.h"
#import "ProductWebService.h"

@interface ProductSurveyDetailViewController ()<XYPieChartDataSource,XYPieChartDelegate> {
    NSMutableArray *sliceColors,*slices,*sliceText;
    float firstValue,secondValue,finalValue;
}

@property (weak, nonatomic) IBOutlet UIScrollView *containerScrollView;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@end

@implementation ProductSurveyDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    sliceColors = [NSMutableArray arrayWithObjects:
                   [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:235.0f/255.0 alpha:1],
                   [UIColor colorWithRed:235.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1],
                   nil];
    sliceText = [NSMutableArray arrayWithObjects:@"YES",@"NO",nil];
    [self loadSurveyFeedback];
    if (self.isPercentageReport) {
        [self initializeTheView];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initializeTheView {
    float y = 25.0f;
    
    for (UIView *v in self.containerView.subviews) {
        [v removeFromSuperview];
    }
    
    id catTitle = [self.surveyDict objectForKey:@"feed_title"];
    if (catTitle && [catTitle isKindOfClass:[NSString class]]) {
        NSDictionary *titleAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(13.0f)]};
        CGSize size = [catTitle boundingRectWithSize:CGSizeMake(self.containerView.frame.size.width-32.0f, 2000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:titleAttr context:nil].size;
        
        UILabel *categoryNewsHeadingLabel = [[UILabel alloc] init];
        categoryNewsHeadingLabel.numberOfLines = 0;
        categoryNewsHeadingLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(13.0f)];
        categoryNewsHeadingLabel.textColor = [UIColor blackColor];
        [self.containerView addSubview:categoryNewsHeadingLabel];
        
        categoryNewsHeadingLabel.frame = CGRectMake(16.0f, y, self.containerView.frame.size.width-32.0f, size.height+5.0f);
        categoryNewsHeadingLabel.text = catTitle;
        
        y += size.height+20.0f;
        
    }
    
    NSString *yourReportString = @"";
    if (self.isPercentageReport) {
        yourReportString = [NSString stringWithFormat:@"Your ratings : %@%%",self.ratingString];
    }else {
        yourReportString = [NSString stringWithFormat:@"Your opinion is : %@",self.ratingString];
    }
    
    NSDictionary *titleAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(13.0f)]};
    CGSize size = [yourReportString boundingRectWithSize:CGSizeMake(self.containerView.frame.size.width-32.0f, 2000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:titleAttr context:nil].size;
    
    UILabel *reportHeadingLabel = [[UILabel alloc] init];
    reportHeadingLabel.numberOfLines = 0;
    reportHeadingLabel.textAlignment = NSTextAlignmentCenter;
    reportHeadingLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(13.0f)];
    reportHeadingLabel.textColor = [UIColor blackColor];
    [self.containerView addSubview:reportHeadingLabel];
    
    reportHeadingLabel.frame = CGRectMake(16.0f, y, self.containerView.frame.size.width-32.0f, size.height+5.0f);
    reportHeadingLabel.text = yourReportString;
    
    y += size.height+20.0f;
    
    XYPieChart *pieChart = [[XYPieChart alloc]initWithFrame:CGRectMake(20.0, y, self.containerView.frame.size.width-40.0f, 252.0f)
                                                     Center:CGPointMake((self.containerView.frame.size.width-40.0)/2, 252.0f/2)
                                                     Radius:110.0f];
    [pieChart setDelegate:self];
    [pieChart setDataSource:self];
    [pieChart setStartPieAngle:M_PI_2];
    [pieChart setAnimationSpeed:1.0];
    [pieChart setLabelFont:[UIFont systemFontOfSize:12]];
    [pieChart setLabelRadius:pieChart.labelRadius+15.0f];
    [pieChart setShowPercentage:YES];
    [pieChart setPieBackgroundColor:[UIColor colorWithWhite:0.95 alpha:0]];
    [pieChart setUserInteractionEnabled:YES];
    [pieChart setLabelShadowColor:[UIColor blackColor]];
    pieChart.showLabel = YES;
    [self.containerView addSubview:pieChart];
    
    if (!self.isPercentageReport) {
        [pieChart reloadData];
        
        y += pieChart.frame.size.height+20.0f;
        
        float x = APP_DELEGATE.window.frame.size.width/2.0f-70.0f;
        
        UILabel *yesBackgroundLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, 10.0f, 10.0f)];
        yesBackgroundLabel.layer.backgroundColor = [[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:235.0f/255.0 alpha:1] CGColor];
        yesBackgroundLabel.layer.cornerRadius = 5.0f;
        [self.containerView addSubview:yesBackgroundLabel];
        x += 20.0f;
        
        UILabel *yesLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y-5.0f, 40.0f, 20.0f)];
        yesLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(10.0f)];
        yesLabel.text = @"Yes";
        [self.containerView addSubview:yesLabel];
        x += 40.0f;
        
        UILabel *noBackgroundLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, 10.0f, 10.0f)];
        noBackgroundLabel.layer.backgroundColor = [[UIColor colorWithRed:235.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1] CGColor];
        noBackgroundLabel.layer.cornerRadius = 5.0f;
        [self.containerView addSubview:noBackgroundLabel];
        x += 20.0f;
        
        UILabel *noLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y-5.0f, 40.0f, 20.0f)];
        noLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(10.0f)];
        noLabel.text = @"No";
        [self.containerView addSubview:noLabel];
        x += 40.0f;
        
        y += 40.0f;
    }
    
    CGRect frame = self.containerView.frame;
    frame.size.height = y;
    self.containerView.frame = frame;
    
    self.containerScrollView.contentSize = CGSizeMake(APP_DELEGATE.window.frame.size.width, y+frame.origin.y+53.0f);
}

#pragma mark - XYPieChart Data Source

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    return [slices count];
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    return [[slices objectAtIndex:index] floatValue];
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    return [sliceColors objectAtIndex:index];
}

- (NSString *)pieChart:(XYPieChart *)pieChart textForSliceAtIndex:(NSUInteger)index {
    return [sliceText objectAtIndex:index];
}

-(void) loadSurveyFeedback {
    NSString *quizeIdString;
    id feedId = [self.surveyDict objectForKey:@"feed_id"];
    if (feedId && [feedId isKindOfClass:[NSString class]]) {
        quizeIdString = [NSMutableString stringWithFormat:@"%@",feedId];
    }
    
    ProductWebService *productWebService = [[ProductWebService alloc]init];
    [productWebService surveyDetailWithKey:@"password" withType:@"report" withQuizeId:quizeIdString withPoints:nil withMobile:[COMMON_SETTINGS userMobile] andCompletionBlock:^(id responseData) {
        if (responseData && [responseData isKindOfClass:[NSDictionary class]]) {
            DLog(@"responce data is : %@",responseData);
            id responseMessage = [responseData objectForKey:@"response_message"];
            if (responseMessage && [responseMessage isKindOfClass:[NSString class]] && [responseMessage isEqualToString:@"Success"]) {
                id data = [responseData objectForKey:@"data"];
                if (data && [data isKindOfClass:[NSDictionary class]]) {
                    id yes = [data objectForKey:@"yes"];
                    if (yes && [yes isKindOfClass:[NSString class]]) {
                        firstValue = [yes floatValue];
                    }
                    id no = [data objectForKey:@"no"];
                    if (no && [no isKindOfClass:[NSString class]]) {
                        secondValue = [no floatValue];
                    }
                    finalValue = firstValue+secondValue;
                }
                [self loadPieCharValues];
            }else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Survey Alert" message:responseMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }
    }];
}

-(void) loadPieCharValues {
    NSNumber *value = [[NSNumber alloc]initWithFloat:COMPUTE_GRAPH_VALUES(firstValue)];
    NSNumber *value1 = [[NSNumber alloc]initWithFloat:COMPUTE_GRAPH_VALUES(secondValue)];
    slices = [[NSMutableArray alloc]initWithObjects:value,value1, nil];
    
    [self initializeTheView];
}

@end
