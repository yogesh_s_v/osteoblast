//
//  DetailViewController.h
//  DepuySynthes
//
//  Created by cdp on 12/21/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property NSString *detailViewTitleString,*venueString,*timingString,*dateString;

@property (weak, nonatomic) IBOutlet UILabel *detailHeadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *conferenceDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timingLabel;
@property (weak, nonatomic) IBOutlet UILabel *venueLabel;
@property (weak, nonatomic) IBOutlet UIWebView *detailViewDescriptionWebView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;



@end
