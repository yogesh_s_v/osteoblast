//
//  ProductDetailViewController.m
//  DepuySynthes
//
//  Created by Akshay Gohel on 30/12/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "FeedsWebService.h"
#import "AsyncImageView.h"
#import "CommonSettings.h"
#import "Constants.h"
#import "ContactUsViewController.h"
#import "NSString+HTML.h"

#define MAX_FONT_SIZE_OFFSET 5.0f

//#define COMPUTE_FONT_SIZE(initialFontSize) ((([CommonSettings sharedInstance].fontPercentage*MAX_FONT_SIZE_OFFSET)/100.0f)+initialFontSize)

@interface ProductDetailViewController ()<UIWebViewDelegate>{
    NSDateFormatter *fromDateFormatter, *toDateFormatter;
    UIWebView *contentWebView;
    float y;
    NSString *articleLink;
}

@property(nonatomic, weak) IBOutlet UIScrollView *containerScrollView;
@property (nonatomic, weak) IBOutlet UIButton *shareButton, *registerButton, *readFullArticleButton, *interestedButton;
@property(nonatomic, strong) UIPopoverController *popup;

@end

@implementation ProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    fromDateFormatter = [[NSDateFormatter alloc] init];
    [fromDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    toDateFormatter = [[NSDateFormatter alloc] init];
    [toDateFormatter setDateFormat:@"MMMM dd"];
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, APP_DELEGATE.window.frame.size.width-40.0f, 44.0f)];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-5.0f, 2.0f, 155.0f, 42.0f)];
    titleLabel.text = @"OsteoBlast";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont systemFontOfSize:15.0f];
    [titleView addSubview:titleLabel];
    self.navigationItem.titleView = titleView;
    
    [self.shareButton setHidden:YES];
    [self.registerButton setHidden:YES];
    [self.readFullArticleButton setHidden:YES];
    [self.interestedButton setHidden:YES];
    
    self.shareButton.layer.cornerRadius = 4.0f;
    self.shareButton.layer.borderColor = [UIColor colorWithRed:192.0f/255.0f green:50.0f/255.0f blue:25.0f/255.0f alpha:1.0f].CGColor;
    self.shareButton.layer.borderWidth = 1.0f;
    self.interestedButton.layer.cornerRadius = 4.0f;
    self.interestedButton.layer.borderColor = [UIColor colorWithRed:192.0f/255.0f green:50.0f/255.0f blue:25.0f/255.0f alpha:1.0f].CGColor;
    self.interestedButton.layer.borderWidth = 1.0f;
    self.registerButton.layer.cornerRadius = 4.0f;
    self.registerButton.layer.borderColor = [UIColor colorWithRed:0.0f/255.0f green:54.0f/255.0f blue:293.0f/255.0f alpha:1.0f].CGColor;
    self.registerButton.layer.borderWidth = 1.0f;
    self.readFullArticleButton.layer.cornerRadius = 4.0f;
    self.readFullArticleButton.layer.borderColor = [UIColor colorWithRed:0.0f/255.0f green:54.0f/255.0f blue:293.0f/255.0f alpha:1.0f].CGColor;
    self.readFullArticleButton.layer.borderWidth = 1.0f;
    
//    FeedsWebService *feedsWebService = [[FeedsWebService alloc] init];
//    [feedsWebService getFeedsWithKey:@"password" columns:nil byFeedID:[self.productDetails objectForKey:@"feed_id"] byCategoryID:nil limit:0 andCompletionBlock:^(id responseData) {
//        DLog(@"ResponseData: %@", responseData);
//    }];
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    y = 16.0f;
    
    NSString *linkString = [self.productDetails objectForKey:@"feed_article_link"];
    if (linkString && [linkString isKindOfClass:[NSString class]] && ![linkString isEqualToString:@""]) {
        articleLink = linkString;
    }
    
    id categoryID = [self.productDetails objectForKey:@"feed_cat_id"];
    if (categoryID && ([categoryID isKindOfClass:[NSString class]] || [categoryID isKindOfClass:[NSString class]])) {
    {
        if ([categoryID intValue] == 1) {
            [self.readFullArticleButton setHidden:NO];
            [self.shareButton setHidden:NO];
        }else if ([categoryID intValue] == 2) {
            [self.registerButton setHidden:NO];
            [self.shareButton setHidden:NO];
        }else if ([categoryID intValue] == 3) {
            [self.shareButton setHidden:NO];
        }else if ([categoryID intValue] == 4) {
            [self.interestedButton setHidden:NO];
        }
        
        id feedImageURL = [self.productDetails objectForKey:@"feed_image"];
        if (feedImageURL && [feedImageURL isKindOfClass:[NSString class]] && ![feedImageURL isEqualToString:@""]) {
            UIImageView *categoryImageView = [[UIImageView alloc] init];
//            categoryImageView.contentMode = UIViewContentModeScaleAspectFit;
            [self.containerScrollView addSubview:categoryImageView];
            [((AsyncImageView*)categoryImageView) setImageURL:[NSURL URLWithString:feedImageURL]];
            if ([categoryID integerValue] == 2) {
                categoryImageView.frame = CGRectMake((self.view.frame.size.width/2)-144.0f, y, 288.0f, 50.0f);
                y += 54.0f;
            }else {
                categoryImageView.frame = CGRectMake((self.view.frame.size.width/2)-144.0f, y, 288.0f, 234.0f);
                y += 244.0f;
            }
            categoryImageView.autoresizingMask = UIViewAutoresizingNone;
        }
    }
    
        id title = [self.productDetails objectForKey:@"feed_title"];
        if (title && [title isKindOfClass:[NSString class]]) {
            NSDictionary *titleAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(18.0f)]};
            CGSize size = [title boundingRectWithSize:CGSizeMake(self.view.frame.size.width-32.0f, 400.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:titleAttr context:nil].size;
            
            UILabel *categoryNewsHeadingLabel = [[UILabel alloc] init];
            categoryNewsHeadingLabel.numberOfLines = 0;
            categoryNewsHeadingLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(18.0f)];
            categoryNewsHeadingLabel.textColor = [UIColor blackColor];
            [self.containerScrollView addSubview:categoryNewsHeadingLabel];
            
            categoryNewsHeadingLabel.frame = CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, size.height+5.0f);
            NSString *titleString = @"";
            if ([categoryID integerValue] == 3) {
                id caseAutherAge = [self.productDetails objectForKey:@"feed_case_age"];
                if (caseAutherAge && [caseAutherAge isKindOfClass:[NSString class]]) {
                    titleString = [NSString stringWithFormat:@"%@ : %@",title,caseAutherAge];
                }
            }
            if ([categoryID integerValue] == 3) {
                categoryNewsHeadingLabel.text = titleString;
            }else {
                categoryNewsHeadingLabel.text = title;
            }
            y += size.height+10.0f;
        }
        
        if ([categoryID intValue] == 2) {
            UILabel *backgroundViewLabel = [[UILabel alloc] init];
            backgroundViewLabel.backgroundColor = [UIColor colorWithRed:22.0/255.0 green:165.0/255.0 blue:63.0/255.0 alpha:1.0];
            [self.containerScrollView addSubview:backgroundViewLabel];
            
            {
                NSDictionary *feedScheduleBoldAttr = @{ NSFontAttributeName : [UIFont boldSystemFontOfSize:COMPUTE_FONT_SIZE(15.0f)], NSForegroundColorAttributeName : [UIColor whiteColor]};
                NSDictionary *feedScheduleAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(14.0f)], NSForegroundColorAttributeName : [UIColor whiteColor]};
                
                NSMutableAttributedString *feedScheduleString = [[NSMutableAttributedString alloc] init];
                id feedConfDate = [self.productDetails objectForKey:@"feed_conf_date"];
                if (feedConfDate && [feedConfDate isKindOfClass:[NSString class]]) {
                    [feedScheduleString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Conference Date : " attributes:feedScheduleBoldAttr]];
                    [feedScheduleString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", feedConfDate] attributes:feedScheduleAttr]];
                }
                id feedConfTime = [self.productDetails objectForKey:@"feed_conf_time"];
                if (feedConfTime && [feedConfTime isKindOfClass:[NSString class]]) {
                    [feedScheduleString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Timings : " attributes:feedScheduleBoldAttr]];
                    [feedScheduleString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", feedConfTime] attributes:feedScheduleAttr]];
                }
                id feedConfVenue = [self.productDetails objectForKey:@"feed_conf_venue"];
                if (feedConfVenue && [feedConfVenue isKindOfClass:[NSString class]]) {
                    [feedScheduleString appendAttributedString:[[NSAttributedString alloc] initWithString:@"Venue : " attributes:feedScheduleBoldAttr]];
                    [feedScheduleString appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n", feedConfVenue] attributes:feedScheduleAttr]];
                }
                
                CGSize size = [feedScheduleString boundingRectWithSize:CGSizeMake(self.view.frame.size.width-64.0f, 20000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) context:nil].size;
                
                UILabel *horizontalRuleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, 0.5f)];
                horizontalRuleLabel.backgroundColor = [UIColor lightGrayColor];
                y += 5.0f;
                [self.containerScrollView addSubview:horizontalRuleLabel];
                
                UILabel *feedScheduleLabel = [[UILabel alloc] init];
                feedScheduleLabel.numberOfLines = 0;
                feedScheduleLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(13.0f)];
                feedScheduleLabel.textColor = [UIColor whiteColor];
                [self.containerScrollView addSubview:feedScheduleLabel];
                
                feedScheduleLabel.frame = CGRectMake(32.0f, y+16.0f, self.view.frame.size.width-64.0f, size.height+5.0f);
                feedScheduleLabel.attributedText = feedScheduleString;
                
                backgroundViewLabel.frame = CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, size.height+37.0f);
                
                y += size.height+42.0f;
            }
        } else {
            id feedAuthors = [self.productDetails objectForKey:@"feed_authors"];
            if (feedAuthors && [feedAuthors isKindOfClass:[NSString class]]) {
                NSDictionary *feedAuthorsBoldAttr = @{ NSFontAttributeName : [UIFont boldSystemFontOfSize:COMPUTE_FONT_SIZE(15.0f)], NSForegroundColorAttributeName : [UIColor blackColor]};
                NSDictionary *feedAuthorsAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(14.0f)], NSForegroundColorAttributeName : [UIColor blackColor]};
                
                NSMutableAttributedString *feedAuthorsString = [[NSMutableAttributedString alloc] initWithString:@"Authors: " attributes:feedAuthorsBoldAttr];
                [feedAuthorsString appendAttributedString:[[NSAttributedString alloc] initWithString:feedAuthors attributes:feedAuthorsAttr]];
                CGSize size = [feedAuthorsString boundingRectWithSize:CGSizeMake(self.view.frame.size.width-32.0f, 20000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) context:nil].size;
                
                UILabel *horizontalRuleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, 0.5f)];
                horizontalRuleLabel.backgroundColor = [UIColor lightGrayColor];
                y += 5.0f;
                [self.containerScrollView addSubview:horizontalRuleLabel];
                
                UILabel *feedAuthorsLabel = [[UILabel alloc] init];
                feedAuthorsLabel.numberOfLines = 0;
                feedAuthorsLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(13.0f)];
                feedAuthorsLabel.textColor = [UIColor blackColor];
                [self.containerScrollView addSubview:feedAuthorsLabel];
                
                feedAuthorsLabel.frame = CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, size.height+5.0f);
                feedAuthorsLabel.attributedText = feedAuthorsString;
                y += size.height+10.0f;
            }
        }
    }
    
    
    id shortdesc = [self.productDetails objectForKey:@"feed_data"];
    
    if (shortdesc && [shortdesc isKindOfClass:[NSString class]]) {
//        shortdesc = [shortdesc stringByConvertingHTMLToPlainText];
//        NSMutableAttributedString *descString = [[NSMutableAttributedString alloc] initWithData:[shortdesc dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType} documentAttributes:nil error:nil];
        
        NSDictionary *shortDescBoldAttr = @{ NSFontAttributeName : [UIFont boldSystemFontOfSize:COMPUTE_FONT_SIZE(15.0f)], NSForegroundColorAttributeName : [UIColor blackColor]};
//        NSDictionary *shortDescAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(15.0f)], NSForegroundColorAttributeName : [UIColor blackColor]};
        
        NSMutableAttributedString *shortDescString = [[NSMutableAttributedString alloc] initWithString:@"" attributes:shortDescBoldAttr];
        if (categoryID && ([categoryID isKindOfClass:[NSString class]] || [categoryID isKindOfClass:[NSString class]])) {
            if ([categoryID intValue] != 4 && [categoryID intValue] != 3 && [categoryID intValue] == 1) {
                shortDescString = [[NSMutableAttributedString alloc] initWithString:@"Abstract:\n" attributes:shortDescBoldAttr];
            }
            if ([categoryID intValue] == 3) {
                shortDescString = [[NSMutableAttributedString alloc] initWithString:@"Information:\n" attributes:shortDescBoldAttr];
            }
            if ([categoryID intValue] == 2) {
                shortDescString = [[NSMutableAttributedString alloc] initWithString:@"Additional Information:\n" attributes:shortDescBoldAttr];
            }
        }
        
//        [shortDescString appendAttributedString:[[NSAttributedString alloc] initWithString:descString.string attributes:shortDescAttr]];
        CGSize size = [shortDescString boundingRectWithSize:CGSizeMake(self.view.frame.size.width-32.0f, 20000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) context:nil].size;
        
        UILabel *horizontalRuleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, 0.5f)];
        horizontalRuleLabel.backgroundColor = [UIColor lightGrayColor];
        y += 5.0f;
        [self.containerScrollView addSubview:horizontalRuleLabel];
        
        UILabel *categoryNewsHeadingLabel = [[UILabel alloc] init];
        categoryNewsHeadingLabel.numberOfLines = 0;
        categoryNewsHeadingLabel.font = [UIFont boldSystemFontOfSize:COMPUTE_FONT_SIZE(14.0f)];
        categoryNewsHeadingLabel.textColor = [UIColor blackColor];
        [self.containerScrollView addSubview:categoryNewsHeadingLabel];
        
        categoryNewsHeadingLabel.frame = CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, size.height+5.0f);
        categoryNewsHeadingLabel.attributedText = shortDescString;
        y += size.height+5.0f;
        
        if ([categoryID intValue] == 1) {
            shortDescBoldAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(15.0f)], NSForegroundColorAttributeName : [UIColor blackColor]};
            shortdesc = [shortdesc stringByConvertingHTMLToPlainText];
            shortDescString = [[NSMutableAttributedString alloc] initWithString:shortdesc attributes:shortDescBoldAttr];
            CGSize size = [shortDescString boundingRectWithSize:CGSizeMake(self.view.frame.size.width-32.0f, 20000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) context:nil].size;
            
            UILabel *categoryDetailLabel = [[UILabel alloc] init];
            categoryDetailLabel.numberOfLines = 0;
            categoryDetailLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(15.0f)];
            categoryDetailLabel.textColor = [UIColor blackColor];
            [self.containerScrollView addSubview:categoryDetailLabel];
            
            categoryDetailLabel.frame = CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, size.height+5.0f);
            categoryDetailLabel.attributedText = shortDescString;
            y += size.height+10.0f;
            
            self.containerScrollView.contentSize = CGSizeMake(self.containerScrollView.contentSize.width, y);
        }else {
            contentWebView = [[UIWebView alloc]initWithFrame:CGRectMake(12.0f, y, self.view.frame.size.width-32.0f, 20.0f)];
            contentWebView.delegate = self;
            [contentWebView loadHTMLString:shortdesc baseURL:nil];
            //        contentWebView.frame = CGRectMake(12.0f,y,self.view.frame.size.width-32.0f,size1.height);
            [contentWebView setUserInteractionEnabled:YES];
            contentWebView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        }
//        [shortDescString appendAttributedString:[[NSAttributedString alloc] initWithString:descString.string attributes:shortDescAttr]];
//        CGSize size1 = [shortDescString boundingRectWithSize:CGSizeMake(self.view.frame.size.width-32.0f, 20000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) context:nil].size;
        
//        [self.containerScrollView addSubview:contentWebView];
        
//        UILabel *categoryNewsSubHeadingLabel = [[UILabel alloc] init];
//        categoryNewsSubHeadingLabel.numberOfLines = 0;
//        categoryNewsSubHeadingLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(13.0f)];
//        categoryNewsSubHeadingLabel.textColor = [UIColor blackColor];
//        [self.containerScrollView addSubview:categoryNewsSubHeadingLabel];
//        
//        categoryNewsSubHeadingLabel.frame = CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, size.height+5.0f);
//        categoryNewsSubHeadingLabel.attributedText = shortDescString;
//        y += contentWebView.frame.size.height+10.0f;
    }
    
//    if (categoryID && ([categoryID isKindOfClass:[NSString class]] || [categoryID isKindOfClass:[NSString class]])) {
//        if ([categoryID intValue] == 3) {
//            NSDictionary *descBoldAttr = @{ NSFontAttributeName : [UIFont boldSystemFontOfSize:COMPUTE_FONT_SIZE(15.0f)], NSForegroundColorAttributeName : [UIColor blackColor]};
//
//            id feedImageURL = [self.productDetails objectForKey:@"feed_case_preop_url"];
//            if (feedImageURL && [feedImageURL isKindOfClass:[NSString class]] && ![feedImageURL isEqualToString:@""]) {
//                feedImageURL = [(NSString*)feedImageURL stringByReplacingOccurrencesOfString:@";na" withString:@""];
//                CGSize size = [@"Xrays(Pre):" boundingRectWithSize:CGSizeMake(self.view.frame.size.width-32.0f, 20000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:descBoldAttr context:nil].size;
//                
//                UILabel *categoryNewsSubHeadingLabel = [[UILabel alloc] init];
//                categoryNewsSubHeadingLabel.numberOfLines = 0;
//                categoryNewsSubHeadingLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(15.0f)];
//                categoryNewsSubHeadingLabel.textColor = [UIColor blackColor];
//                [self.containerScrollView addSubview:categoryNewsSubHeadingLabel];
//                categoryNewsSubHeadingLabel.frame = CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, size.height+5.0f);
//                y += size.height+10.0f;
//                categoryNewsSubHeadingLabel.text = @"Xrays(Pre):";
//                
//                UILabel *horizontalRuleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, 0.5f)];
//                horizontalRuleLabel.backgroundColor = [UIColor lightGrayColor];
//                y += 5.0f;
//                [self.containerScrollView addSubview:horizontalRuleLabel];
//                
//                UIImageView *categoryImageView = [[UIImageView alloc] init];
//                //            categoryImageView.contentMode = UIViewContentModeScaleAspectFit;
//                [self.containerScrollView addSubview:categoryImageView];
//                [((AsyncImageView*)categoryImageView) setImageURL:[NSURL URLWithString:feedImageURL]];
//                
//                categoryImageView.frame = CGRectMake((self.view.frame.size.width/2)-144.0f, y, 288.0f, 234.0f);
//                categoryImageView.autoresizingMask = UIViewAutoresizingNone;
//                y += 244.0f;
//            }
//            
//            feedImageURL = [self.productDetails objectForKey:@"feed_case_postop_url"];
//            if (feedImageURL && [feedImageURL isKindOfClass:[NSString class]] && ![feedImageURL isEqualToString:@""]) {
////                feedImageURL = [(NSString*)feedImageURL stringByReplacingOccurrencesOfString:@";na" withString:@""];
//                CGSize size = [@"Xrays(Post):" boundingRectWithSize:CGSizeMake(self.view.frame.size.width-32.0f, 20000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:descBoldAttr context:nil].size;
//                
//                UILabel *categoryNewsSubHeadingLabel = [[UILabel alloc] init];
//                categoryNewsSubHeadingLabel.numberOfLines = 0;
//                categoryNewsSubHeadingLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(15.0f)];
//                categoryNewsSubHeadingLabel.textColor = [UIColor blackColor];
//                [self.containerScrollView addSubview:categoryNewsSubHeadingLabel];
//                categoryNewsSubHeadingLabel.frame = CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, size.height+5.0f);
//                y += size.height+10.0f;
//                categoryNewsSubHeadingLabel.text = @"Xrays(Post):";
//                
//                UILabel *horizontalRuleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, 0.5f)];
//                horizontalRuleLabel.backgroundColor = [UIColor lightGrayColor];
//                y += 5.0f;
//                [self.containerScrollView addSubview:horizontalRuleLabel];
//                
//                for (int i = 0; i < 2; i++) {
//                    NSString *resultImageURL;
//                    NSRange equalRange = [feedImageURL rangeOfString:@";" options:NSBackwardsSearch];
//                    if (equalRange.location != NSNotFound) {
//                        if (i == 0) {
//                            resultImageURL = [feedImageURL substringToIndex:equalRange.location];
//                        }else if (i == 1) {
//                            resultImageURL = [feedImageURL substringFromIndex:equalRange.location + equalRange.length];
//                        }
//                        DLog(@"The result = %@", resultImageURL);
//                        if (resultImageURL && ![resultImageURL isEqualToString:@""] && ![resultImageURL isEqualToString:@"na"]) {
//                            UIImageView *categoryImageView = [[UIImageView alloc] init];
//                            //            categoryImageView.contentMode = UIViewContentModeScaleAspectFit;
//                            [self.containerScrollView addSubview:categoryImageView];
//                            [((AsyncImageView*)categoryImageView) setImageURL:[NSURL URLWithString:resultImageURL]];
//                            
//                            categoryImageView.frame = CGRectMake((self.view.frame.size.width/2)-144.0f, y, 288.0f, 234.0f);
//                            categoryImageView.autoresizingMask = UIViewAutoresizingNone;
//                            y += 244.0f;
//                        } else {
//                            DLog(@"There is no = in the string");
//                        }
//                    }
//                }
//            }
//        }
//    }
    
//    id creationDate = [self.productDetails objectForKey:@"feed_created_date"];
//    if (creationDate && [creationDate isKindOfClass:[NSString class]]) {
//        UILabel *categoryNewsDateLabel = [[UILabel alloc] init];
//        categoryNewsDateLabel.numberOfLines = 0;
//        categoryNewsDateLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(13.0f)];
//        categoryNewsDateLabel.textColor = [UIColor blackColor];
//        [self.containerScrollView addSubview:categoryNewsDateLabel];
//        
//        categoryNewsDateLabel.frame = CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, 20);
//        
//        NSDate *date = [fromDateFormatter dateFromString:creationDate];
//        categoryNewsDateLabel.text = [toDateFormatter stringFromDate:date];
//        y += 30.0f;
//    }
    
//    self.containerScrollView.contentSize = CGSizeMake(self.containerScrollView.contentSize.width, y);
}

#pragma Web View Delegate Methods

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    DLog(@"request.URL.absoluteString:%@", request.URL);
    if (![request.URL.absoluteString isEqualToString:@"about:blank"]) {
        [[UIApplication sharedApplication] openURL:request.URL];
        return NO;
    }
    return YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    contentWebView.allowsInlineMediaPlayback = YES;
    contentWebView.dataDetectorTypes = UIDataDetectorTypeAll;
    
    contentWebView.scrollView.scrollEnabled = NO;
    float height = [[contentWebView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight"] floatValue]+10.0f;

    CGRect frame = contentWebView.frame;
    frame.size.height = height;
    contentWebView.frame = frame;
    
//    = CGRectMake(12.0f, y, self.view.frame.size.width-32.0f, height);
    [self.containerScrollView addSubview:contentWebView];
    
    y += contentWebView.frame.size.height+10.0f;
    
    id categoryID = [self.productDetails objectForKey:@"feed_cat_id"];
    if (categoryID && ([categoryID isKindOfClass:[NSString class]] || [categoryID isKindOfClass:[NSString class]])) {
        if ([categoryID intValue] == 3) {
            NSDictionary *descBoldAttr = @{ NSFontAttributeName : [UIFont boldSystemFontOfSize:COMPUTE_FONT_SIZE(15.0f)], NSForegroundColorAttributeName : [UIColor blackColor]};
            
            id feedImageURL = [self.productDetails objectForKey:@"feed_case_preop_url"];
            if (feedImageURL && [feedImageURL isKindOfClass:[NSString class]] && ![feedImageURL isEqualToString:@""]) {
                feedImageURL = [(NSString*)feedImageURL stringByReplacingOccurrencesOfString:@";na" withString:@""];
                CGSize size = [@"Xrays(Pre):" boundingRectWithSize:CGSizeMake(self.view.frame.size.width-32.0f, 20000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:descBoldAttr context:nil].size;
                
                UILabel *categoryNewsSubHeadingLabel = [[UILabel alloc] init];
                categoryNewsSubHeadingLabel.numberOfLines = 0;
                categoryNewsSubHeadingLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(15.0f)];
                categoryNewsSubHeadingLabel.textColor = [UIColor blackColor];
                [self.containerScrollView addSubview:categoryNewsSubHeadingLabel];
                categoryNewsSubHeadingLabel.frame = CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, size.height+5.0f);
                y += size.height+10.0f;
                categoryNewsSubHeadingLabel.text = @"Xrays(Pre):";
                
                UILabel *horizontalRuleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, 0.5f)];
                horizontalRuleLabel.backgroundColor = [UIColor lightGrayColor];
                y += 5.0f;
                [self.containerScrollView addSubview:horizontalRuleLabel];
                
                UIImageView *categoryImageView = [[UIImageView alloc] init];
                //            categoryImageView.contentMode = UIViewContentModeScaleAspectFit;
                [self.containerScrollView addSubview:categoryImageView];
                [((AsyncImageView*)categoryImageView) setImageURL:[NSURL URLWithString:feedImageURL]];
                
                categoryImageView.frame = CGRectMake((self.view.frame.size.width/2)-144.0f, y, 288.0f, 234.0f);
                categoryImageView.autoresizingMask = UIViewAutoresizingNone;
                y += 244.0f;
            }
            
            feedImageURL = [self.productDetails objectForKey:@"feed_case_postop_url"];
            if (feedImageURL && [feedImageURL isKindOfClass:[NSString class]] && ![feedImageURL isEqualToString:@""]) {
                //                feedImageURL = [(NSString*)feedImageURL stringByReplacingOccurrencesOfString:@";na" withString:@""];
                CGSize size = [@"Xrays(Post):" boundingRectWithSize:CGSizeMake(self.view.frame.size.width-32.0f, 20000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:descBoldAttr context:nil].size;
                
                UILabel *categoryNewsSubHeadingLabel = [[UILabel alloc] init];
                categoryNewsSubHeadingLabel.numberOfLines = 0;
                categoryNewsSubHeadingLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(15.0f)];
                categoryNewsSubHeadingLabel.textColor = [UIColor blackColor];
                [self.containerScrollView addSubview:categoryNewsSubHeadingLabel];
                categoryNewsSubHeadingLabel.frame = CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, size.height+5.0f);
                y += size.height+10.0f;
                categoryNewsSubHeadingLabel.text = @"Xrays(Post):";
                
                UILabel *horizontalRuleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16.0f, y, self.view.frame.size.width-32.0f, 0.5f)];
                horizontalRuleLabel.backgroundColor = [UIColor lightGrayColor];
                y += 5.0f;
                [self.containerScrollView addSubview:horizontalRuleLabel];
                
                for (int i = 0; i < 100; i++) {
                    NSString *resultImageURL;
                    BOOL moreImages;
                    NSRange equalRange = [feedImageURL rangeOfString:@";" options:NSLiteralSearch];
                    if (equalRange.location != NSNotFound) {
                        
                        resultImageURL = [feedImageURL substringToIndex:equalRange.location];
                        feedImageURL = [feedImageURL substringFromIndex:equalRange.location + equalRange.length];
                        DLog(@"The result = %@", resultImageURL);
                        moreImages = YES;
                    }else {
                        moreImages = NO;
                    }
                    if ((resultImageURL && ![resultImageURL isEqualToString:@""] && ![resultImageURL isEqualToString:@"na"]) || (feedImageURL && ![feedImageURL isEqualToString:@""] && ![feedImageURL isEqualToString:@"na"])) {
                        UIImageView *categoryImageView = [[UIImageView alloc] init];
                        //            categoryImageView.contentMode = UIViewContentModeScaleAspectFit;
                        [self.containerScrollView addSubview:categoryImageView];
                        if (moreImages) {
                            [((AsyncImageView*)categoryImageView) setImageURL:[NSURL URLWithString:resultImageURL]];
                        }else {
                            [((AsyncImageView*)categoryImageView) setImageURL:[NSURL URLWithString:feedImageURL]];
                        }
                        
                        categoryImageView.frame = CGRectMake((self.view.frame.size.width/2)-144.0f, y, 288.0f, 234.0f);
                        categoryImageView.autoresizingMask = UIViewAutoresizingNone;
                        
                        y += 244.0f;
                        if (!moreImages) {
                            break;
                        }
                    } else {
                        DLog(@"There is no = in the string");
                    }
                }
                
//                for (int i = 0; i < 2; i++) {
//                    NSString *resultImageURL;
//                    NSRange equalRange = [feedImageURL rangeOfString:@";" options:NSBackwardsSearch];
//                    if (equalRange.location != NSNotFound) {
//                        if (i == 0) {
//                            resultImageURL = [feedImageURL substringToIndex:equalRange.location];
//                        }else if (i == 1) {
//                            resultImageURL = [feedImageURL substringFromIndex:equalRange.location + equalRange.length];
//                        }
//                        DLog(@"The result = %@", resultImageURL);
//                        if (resultImageURL && ![resultImageURL isEqualToString:@""] && ![resultImageURL isEqualToString:@"na"]) {
//                            UIImageView *categoryImageView = [[UIImageView alloc] init];
//                            //            categoryImageView.contentMode = UIViewContentModeScaleAspectFit;
//                            [self.containerScrollView addSubview:categoryImageView];
//                            [((AsyncImageView*)categoryImageView) setImageURL:[NSURL URLWithString:resultImageURL]];
//                            
//                            categoryImageView.frame = CGRectMake((self.view.frame.size.width/2)-144.0f, y, 288.0f, 234.0f);
//                            categoryImageView.autoresizingMask = UIViewAutoresizingNone;
//                            y += 244.0f;
//                        } else {
//                            DLog(@"There is no = in the string");
//                        }
//                    }
//                }
            }
        }
    }
    self.containerScrollView.contentSize = CGSizeMake(self.containerScrollView.contentSize.width, y+30.0f);
}

#pragma mark - button tapped events  

-(IBAction)shareButtonTapped:(UIButton*)sender {
    NSString *myWebsite = [[self.productDetails objectForKey:@"feed_data"] stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
//    NSString *myWebsite = @"Download OsteOblast from App Store";
    if (myWebsite) {
        UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:[NSArray arrayWithObject:myWebsite] applicationActivities:nil];
        activityController.excludedActivityTypes = @[UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll];
        if ([[[UIDevice currentDevice] model].lowercaseString rangeOfString:@"ipad"].location != NSNotFound) {
            self.popup = [[UIPopoverController alloc] initWithContentViewController:activityController];
            [self.popup presentPopoverFromRect:sender.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        } else {
            [APP_DELEGATE.window.rootViewController presentViewController:activityController animated:YES completion:nil];
        }
    }
}

-(IBAction)registerButtonTapped:(id)sender {
    ContactUsViewController *contactUsViewController = [[ContactUsViewController alloc]initWithNibName:@"ContactUsViewController" bundle:nil];
//    contactUsViewController.titleString = [self.productDetails objectForKey:@"feed_title"];
    contactUsViewController.titleString = @"";
    [self.navigationController pushViewController:contactUsViewController animated:YES];
}

-(IBAction)inerestedClickHereButtonTapped:(id)sender {
    ContactUsViewController *contactUsViewController = [[ContactUsViewController alloc]initWithNibName:@"ContactUsViewController" bundle:nil];
//    contactUsViewController.titleString = [self.productDetails objectForKey:@"feed_title"];
    contactUsViewController.titleString = @"";
    [self.navigationController pushViewController:contactUsViewController animated:YES];
}

-(IBAction)readFullArticleButtonTapped:(id)sender {
    if (articleLink) {
        NSURL *url = [NSURL URLWithString:articleLink];
        [[UIApplication sharedApplication] openURL:url];
    }
}

@end
