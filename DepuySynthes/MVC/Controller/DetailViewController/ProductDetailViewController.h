//
//  ProductDetailViewController.h
//  DepuySynthes
//
//  Created by Akshay Gohel on 30/12/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductDetailViewController : UIViewController

@property(nonatomic, strong) NSDictionary *productDetails;

@end
