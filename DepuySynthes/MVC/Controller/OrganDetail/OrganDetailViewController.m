//
//  OrganDetailViewController.m
//  DepuySynthes
//
//  Created by cdp on 12/25/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import "OrganDetailViewController.h"
#import "OrganDetailTableViewCell.h"
#import "OrganInfoViewController.h"
#import "PFCSigmaCategoryViewController.h"
#import "ProductWebService.h"
#import "Constants.h"

@interface OrganDetailViewController ()<UITableViewDelegate,UITableViewDataSource>{

}
@property (weak, nonatomic) IBOutlet UITableView *organDetailTableView;

@end

@implementation OrganDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initializeDataAndView];
    self.title = @"Product Catalog";
}

-(void)initializeDataAndView
{
//    organCategoryArray = [[NSMutableArray alloc]initWithObjects:@"Attune",@"PFC Sigma",@"Sigma RP",@"PS 150",@"Sigma Partial",@"True Match", nil];
    
    [self.organDetailTableView registerNib:[UINib nibWithNibName:@"OrganDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"OrganDetailTableViewCell"];
    
    [self.organDetailTableView registerNib:[UINib nibWithNibName:@"OrganDetailTableViewCell" bundle:nil] forHeaderFooterViewReuseIdentifier:@"OrganDetailTableViewCell"];
    [self.organDetailTableView setTableFooterView:[UIView new]];
//    ProductWebService *productWebService = [[ProductWebService alloc]init];
//    [productWebService getProductCategoryWithKey:@"password" byProductID:nil byPartID:nil andCompletionBlock:^(id responsedata) {
//        DLog(@"ResponseData : %@",responsedata);
//        responsedata = [responsedata objectForKey:@"data"];
//        if (responsedata && [responsedata isKindOfClass:[NSArray class]]) {
//            self.organCategoryArray = [[NSMutableArray alloc]initWithArray:responsedata];
//            [self.organDetailTableView reloadData];
//        }
//    }];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [APP_DELEGATE.bottomFooter setHidden:NO];
}

#pragma mark - Table View data source methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.organCategoryArray.count != 0) {
        return [self.organCategoryArray count];
    }else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"OrganDetailTableViewCell";
    
    OrganDetailTableViewCell *cell = (OrganDetailTableViewCell*) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    id category = [self.organCategoryArray objectAtIndex:indexPath.row];
    if ([category isKindOfClass:[NSDictionary class]]) {
        cell.organCategoryLabel.text = [category objectForKey:@"product_cat_name"];
//        NSString *organCategoryString = [category objectForKey:@"product_cat_name"];
//        
//        NSDictionary *organCategoryAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:18.0f]};
//        
//        CGSize size = [organCategoryString boundingRectWithSize:CGSizeMake(self.view.frame.size.width-96.0f, 90.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:organCategoryAttr context:nil].size;
//        CGRect frame = cell.organCategoryLabel.frame;
//        frame.size.height = size.height+5.0f;
//        cell.organCategoryLabel.frame = frame;
        
        [cell.organDetailImageView setImage:[UIImage imageNamed:[[category objectForKey:@"product_icon"] stringByReplacingOccurrencesOfString:@"@drawable/" withString:@""]]];
//        cell.organDetailImageView.layer.cornerRadius = cell.organDetailImageView.frame.size.width/2;
//        
//        cell.organDetailImageView.layer.borderColor = [[UIColor colorWithRed:(rand()%255)/255.0f green:(rand()%255)/255.0f blue:(rand()%255)/255.0f alpha:1.0f] CGColor];
//        cell.organDetailImageView.layer.borderWidth = 1.0f;
        cell.imageTextLable.textColor = [UIColor whiteColor];
        cell.imageTextLable.text = @"";//[NSString stringWithFormat:@"%@",[[category objectForKey:@"product_cat_name"] substringToIndex:1]];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    float height = 60.0f;
    id category = [self.organCategoryArray objectAtIndex:indexPath.row];
    if ([category isKindOfClass:[NSDictionary class]]) {
        NSString *organCategoryString = [category objectForKey:@"product_cat_name"];
        
        NSDictionary *organCategoryAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:18.0f]};
        
        CGSize size = [organCategoryString boundingRectWithSize:CGSizeMake(self.view.frame.size.width-96.0f, 90.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:organCategoryAttr context:nil].size;
        if ((size.height+16.0f)>height) {
            height = size.height+16.0f;
        }
    }
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id selectedOrgan = [self.organCategoryArray objectAtIndex:indexPath.row];
    if ([[selectedOrgan objectForKey:@"has_sub_products"] isEqualToString:@"0"]) {
        ProductWebService *productWebService = [[ProductWebService alloc]init];
        [productWebService getProductInfoWithKey:@"password" byProductID:@"" byCategoryID:[selectedOrgan objectForKey:@"product_cat_id"] andCompletionBlock:^(id responseData) {
            if (responseData && [responseData isKindOfClass:[NSDictionary class]]) {
                OrganInfoViewController *organInfoViewController = [[OrganInfoViewController alloc]init];
                id data = [responseData objectForKey:@"data"];
                if (data && [data isKindOfClass:[NSArray class]]) {
                    organInfoViewController.organInfoArray = data;
                    
                    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, APP_DELEGATE.window.frame.size.width-40.0f, 44.0f)];
                    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-5.0f, 2.0f, 147.0f, 42.0f)];
                    titleLabel.text = [selectedOrgan objectForKey:@"product_cat_name"];
                    titleLabel.textColor = [UIColor whiteColor];
                    titleLabel.font = [UIFont systemFontOfSize:15.0f];
                    [titleView addSubview:titleLabel];
                    organInfoViewController.navigationItem.titleView = titleView;
                    
                    [self.navigationController pushViewController:organInfoViewController animated:YES];
                }
            }
        }];
    }else if ([[selectedOrgan objectForKey:@"has_sub_products"] isEqualToString:@"1"]) {
        ProductWebService *productWebService = [[ProductWebService alloc]init];
        [productWebService getSubProductWithKey:@"password" forCategory:[selectedOrgan objectForKey:@"product_cat_id"] andCompletionBlock:^(id responseData) {
            if (responseData && [responseData isKindOfClass:[NSDictionary class]]) {
                PFCSigmaCategoryViewController *pFCSigmaCategoryViewController = [[PFCSigmaCategoryViewController alloc]init];
                id data = [responseData objectForKey:@"data"];
                if (data && [data isKindOfClass:[NSArray class]]) {
                    pFCSigmaCategoryViewController.pFCSigmaCategoryArray = data;
                    
                    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, APP_DELEGATE.window.frame.size.width-40.0f, 44.0f)];
                    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-5.0f, 2.0f, 155.0f, 42.0f)];
                    titleLabel.text = [selectedOrgan objectForKey:@"product_cat_name"];
                    titleLabel.textColor = [UIColor whiteColor];
                    titleLabel.font = [UIFont systemFontOfSize:15.0f];
                    [titleView addSubview:titleLabel];
                    pFCSigmaCategoryViewController.navigationItem.titleView = titleView;
                    
                    [self.navigationController pushViewController:pFCSigmaCategoryViewController animated:YES];
                }
            }
        }];
    }
//    if ([[[organCategoryArray objectAtIndex:indexPath.row] objectForKey:@"product_cat_name"] isEqualToString:@"Attune"]) {
//        //        detailViewController.detailViewTitleString = [allCategoryHeadNameArray objectAtIndex:indexPath.row];
//        //        detailViewController.venueString = [allCategoryDateLocationArray objectAtIndex:indexPath.row];
//        //        detailViewController.dateString = [allCategorySubArray objectAtIndex:indexPath.row];
//    }
//    else if([[[organCategoryArray objectAtIndex:indexPath.row] objectForKey:@"product_cat_name"] isEqualToString:@"PFC Sigma"])
//    {
//        [self.navigationController pushViewController:pFCSigmaCategoryViewController animated:YES];
//    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
