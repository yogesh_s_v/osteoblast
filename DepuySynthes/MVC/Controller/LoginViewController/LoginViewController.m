//
//  LoginViewController.m
//  DepuySynthes
//
//  Created by cdp on 12/19/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"
#import "RegisterViewController.h"
#import "Constants.h"

#import "FLAnimatedImageView.h"
#import "FLAnimatedImage.h"

@interface LoginViewController ()
{
    __weak IBOutlet UIButton *loginButton;
    __weak IBOutlet UIButton *registrationButton;
    BOOL loadedOnce;
}

@property (nonatomic, weak) IBOutlet UIImageView *topLogoIV;
@property (nonatomic, weak) IBOutlet UIView *animationContainerView;
@property (nonatomic, strong) FLAnimatedImageView *imageView;

- (IBAction)onLoginButtonClicked:(id)sender;
- (IBAction)onRegistrationButtonClicked:(id)sender;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    loadedOnce = NO;
    // Do any additional setup after loading the view from its nib.
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, APP_DELEGATE.window.frame.size.width-40.0f, 44.0f)];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-5.0f, 2.0f, 155.0f, 42.0f)];
    titleLabel.text = @"Welcome";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont systemFontOfSize:15.0f];
    [titleView addSubview:titleLabel];
    self.navigationItem.titleView = titleView;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    loginButton.layer.cornerRadius = 4.0f;
    registrationButton.layer.cornerRadius = 4.0f;
    
    self.topLogoIV.alpha = 0.0f;
    loginButton.alpha = 0.0f;
    registrationButton.alpha = 0.0f;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (!loadedOnce) {
        loadedOnce = YES;
        if (!self.imageView) {
            self.imageView = [[FLAnimatedImageView alloc] init];
            self.imageView.contentMode = UIViewContentModeScaleAspectFit;
            self.imageView.clipsToBounds = YES;
        }
        [self.animationContainerView addSubview:self.imageView];
        CGRect frame = self.animationContainerView.bounds;
        frame.origin.x += 3.0f;
        frame.origin.y += 3.0f;
        frame.size.width -= 6.0f;
        frame.size.height -= 6.0f;
        self.imageView.frame = frame;
        
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"OsteoBlastAnimated" withExtension:@"gif"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        FLAnimatedImage *animatedImage = [FLAnimatedImage animatedImageWithGIFData:data];
        self.imageView.animatedImage = animatedImage;
        self.imageView.layer.borderColor = [[UIColor colorWithRed:253.0f/255.0f green:251.0f/255.0f blue:48.0f/255.0f alpha:1.0f] CGColor];
        self.imageView.layer.borderWidth = 1.0f;
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = APP_DELEGATE.window.bounds;
        gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:253.0f/255.0f green:252.0f/255.0f blue:199.0f/255.0f alpha:1.0f] CGColor], (id)[[UIColor colorWithRed:255.0f/255.0f green:254.0f/255.0f blue:165.0f/255.0f alpha:1.0f] CGColor], nil];
        [self.view.layer insertSublayer:gradient atIndex:0];
        NSLog(@"%@",COMMON_SETTINGS.userInfoDict);
        if([[COMMON_SETTINGS.userInfoDict objectForKey:@"IsLogin"]isEqualToString:@"YES"])
        {
            [UIView animateWithDuration:15.3f animations:^{
                self.topLogoIV.alpha = 1.0f;

            }completion:^(BOOL finished){
                [UIView animateWithDuration:1.3f animations:^{
                    self.topLogoIV.alpha = 1.0f;

                [APP_DELEGATE setHomeScreen:YES];
                }];
            }];
        }
        else
        {
            [self performSelector:@selector(showOtherViews) withObject:nil afterDelay:12.7f];
        }
    }
}

-(void) showOtherViews {
    [UIView animateWithDuration:1.3f animations:^{
        self.topLogoIV.alpha = 1.0f;
        loginButton.alpha = 1.0f;
        registrationButton.alpha = 1.0f;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onLoginButtonClicked:(id)sender {
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *userReigstered = [def objectForKey:@"userRegistered"];
    if (userReigstered && [userReigstered isKindOfClass:[NSString class]] && [userReigstered isEqualToString:@"Yes"]) {
        [APP_DELEGATE setHomeScreen:YES];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please register your account." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [alert show];
    }
    [def synchronize];
}

- (IBAction)onRegistrationButtonClicked:(id)sender {
    RegisterViewController *registerViewController = [[RegisterViewController alloc]initWithNibName:@"RegisterViewController" bundle:nil];
    registerViewController.loginTapped = NO;
    [self.navigationController pushViewController:registerViewController animated:YES];
}
@end
