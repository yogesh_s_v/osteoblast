//
//  RegisterViewController.m
//  DepuySynthes
//
//  Created by Akshay G on 24/03/15.
//  Copyright (c) 2015 The_world_inc. All rights reserved.
//

#import "RegisterViewController.h"
#import "Constants.h"
#import "ProductWebService.h"

@interface RegisterViewController () <UITextFieldDelegate> {
    UIButton *doneButton;
    BOOL keyboardAppeared;
    CGRect keyboardframe;
}

@property (nonatomic, weak) IBOutlet UIScrollView *containerScrollView;
@property (nonatomic, weak) IBOutlet UITextField *nameTextField, *hospitalTextField, *cityTextField, *emailTextField, *mobileTextField;
@property (nonatomic, weak) IBOutlet UIButton *createAccButton;

@end

@implementation UIView (FindViewThatIsFirstResponder)

- (UIView *)findViewThatIsFirstResponder
{
    if (self.isFirstResponder) {
        return self;
    }
    
    for (UIView *subView in self.subviews) {
        UIView *firstResponder = [subView findViewThatIsFirstResponder];
        if (firstResponder != nil) {
            return firstResponder;
        }
    }
    return nil;
}

@end


@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.createAccButton.layer.cornerRadius = 4.0f;
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, APP_DELEGATE.window.frame.size.width-40.0f, 44.0f)];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-5.0f, 2.0f, 155.0f, 42.0f)];
    titleLabel.text = @"OsteoBlast";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont systemFontOfSize:15.0f];
    [titleView addSubview:titleLabel];
    self.navigationItem.titleView = titleView;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 3.2) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidAppear:)
                                                     name:UIKeyboardDidShowNotification
                                                   object:nil];
    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidAppear:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidDisappear:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    keyboardAppeared = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    self.containerScrollView.contentSize = CGSizeMake(self.containerScrollView.frame.size.width, self.createAccButton.frame.origin.y+self.createAccButton.frame.size.height+22.0f);
}

#pragma text field methods

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.mobileTextField == textField) {
        NSString *stringTitle = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if (stringTitle.length>10) {
            return NO;
        }
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([self.nameTextField isFirstResponder]) {
        [self.hospitalTextField becomeFirstResponder];
    }else if ([self.hospitalTextField isFirstResponder]) {
        [self.cityTextField becomeFirstResponder];
    }else if ([self.cityTextField isFirstResponder]) {
        [self.emailTextField becomeFirstResponder];
    }else if ([self.emailTextField isFirstResponder]) {
        [self.mobileTextField becomeFirstResponder];
    }else if ([self.mobileTextField isFirstResponder]) {
        [textField resignFirstResponder];
    }else {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark - Button Tapped Events

-(IBAction)createAccButtonTapped:(id)sender {
    [self.view endEditing:YES];
    NSString *emailString = [self.emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSUUID *uniqueId = [[UIDevice currentDevice] identifierForVendor];
    NSString *uuidString = uniqueId.UUIDString;
    
    if (self.nameTextField.text == nil || [self.nameTextField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your name." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if (self.cityTextField.text == nil || [self.cityTextField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your city." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if (self.emailTextField.text == nil || [self.emailTextField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your email Id." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if(![self validateEmailString:emailString]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter a valid email Id." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if (self.mobileTextField.text == nil || [self.mobileTextField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your mobile." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    } else if (self.mobileTextField.text.length != 10) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter correct mobile number." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if (uuidString == nil || [uuidString isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Something went wrong." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    ProductWebService *productWebService = [[ProductWebService alloc]init];
    if (!self.loginTapped) {
        
        [productWebService registerWithKey:@"password" withMobile:self.mobileTextField.text withUUID:uuidString withHospital:self.hospitalTextField.text withEmail:emailString withName:self.nameTextField.text withCity:self.cityTextField.text andCompletionBlock:^(id responseData) {
            if (responseData && [responseData isKindOfClass:[NSDictionary class]]) {
                id responseCode = [((NSDictionary*)responseData) objectForKey:@"response_code"];
                DLog(@"responseData : %@", responseData);
                if (responseCode && [responseCode isKindOfClass:[NSNumber class]] && [responseCode intValue] == 200) {
                    id data = [responseData objectForKey:@"data"];
                    if ([data isKindOfClass:[NSDictionary class]]) {
                        id string = [data objectForKey:@"error"];
                        if (string && [string isKindOfClass:[NSString class]] && ![string isEqualToString:@""]) {
                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Registration Failed" message:string delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                        }
                    }else if (data && [data isKindOfClass:[NSNumber class]] && [data integerValue] == 1) {
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[responseData objectForKey:@"response_message"] message:@"Successfully registered" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];
                        
                        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
                        [def setObject:@"Yes" forKey:@"userRegistered"];
                        [def synchronize];
                        
                        [COMMON_SETTINGS loadInfo];
                        if (COMMON_SETTINGS.userInfoDict == nil) {
                            COMMON_SETTINGS.userInfoDict = [[NSMutableDictionary alloc] init];
                        }
                        [COMMON_SETTINGS.userInfoDict setObject:@"YES" forKey:@"IsLogin"];
                        [COMMON_SETTINGS.userInfoDict setObject:self.nameTextField.text forKey:@"userName"];
                        [COMMON_SETTINGS.userInfoDict setObject:emailString forKey:@"userEmail"];
                        [COMMON_SETTINGS.userInfoDict setObject:self.hospitalTextField.text forKey:@"userHospital"];
                        [COMMON_SETTINGS.userInfoDict setObject:self.cityTextField.text forKey:@"userCity"];
                        [COMMON_SETTINGS.userInfoDict setObject:self.mobileTextField.text forKey:@"userMobile"];
                        [COMMON_SETTINGS saveInfo];
                        
                        [APP_DELEGATE setHomeScreen:YES];
                    }
                } else {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Registration Failed" message:@"Registration Incomplete" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                }
            }
        }];
    } else {
        ProductWebService *productWebService = [[ProductWebService alloc]init];
        [productWebService loginWithKey:@"password" withMobile:self.mobileTextField.text withUUID:uuidString andCompletionBlock:^(id responseData) {
            if (responseData && [responseData isKindOfClass:[NSDictionary class]]) {
                id responseCode = [((NSDictionary*)responseData) objectForKey:@"response_code"];
                DLog(@"responseData : %@", responseData);
                if (responseCode && [responseCode isKindOfClass:[NSNumber class]] && [responseCode intValue] == 200) {
                    UIAlertView *alert;
                    id data = [responseData objectForKey:@"data"];
                    if (data && [data isKindOfClass:[NSDictionary class]]) {
                        id error = [data objectForKey:@"error"];
                        if (error && [error isKindOfClass:[NSString class]]) {
                            alert = [[UIAlertView alloc]initWithTitle:[responseData objectForKey:@"response_message"] message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                            [alert show];
                        }
                    }else {
                        alert = [[UIAlertView alloc]initWithTitle:[responseData objectForKey:@"response_message"] message:@"Successfully registered" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];
                    }
                }else {
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[responseData objectForKey:@"response_message"] message:@"Registration Incomplete" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                }
            }
        }];
    }
    
}

- (BOOL)validateEmailString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}


#pragma mark - keydoard methods

-(void)keyboardDidAppear:(NSNotification *)notification
{
    [self addButtonToKeyboard];
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    if (keyboardAppeared) {
        keyboardAppeared = NO;
        [UIView animateWithDuration:0.3f animations:^{
            
            CGRect frame = APP_DELEGATE.bottomFooter.frame;
            frame.origin.y = APP_DELEGATE.window.frame.size.height-40.0f-keyboardFrameBeginRect.size.height;
            
            CGRect frame1 = self.containerScrollView.frame;
            frame1.size.height = self.view.frame.size.height-keyboardFrameBeginRect.size.height-64.0f-46.0f;
            
            [APP_DELEGATE.bottomFooter setFrame:frame];
            [self.containerScrollView setFrame:frame1];
        } completion:nil];
    }else {
        if (keyboardFrameBeginRect.size.height < keyboardframe.size.height) {
            float diffheight= (keyboardframe.size.height - keyboardFrameBeginRect.size.height);
            [UIView animateWithDuration:0.3f animations:^{
                
                CGRect frame = APP_DELEGATE.bottomFooter.frame;
                CGRect frame1 = self.containerScrollView.frame;
                
                frame.origin.y += diffheight;
                frame1.size.height += diffheight;
                
                [APP_DELEGATE.bottomFooter setFrame:frame];
                [self.containerScrollView setFrame:frame1];
            } completion:nil];
        }else if (keyboardFrameBeginRect.size.height > keyboardframe.size.height) {
            float diffheight= (keyboardFrameBeginRect.size.height - keyboardframe.size.height);
            [UIView animateWithDuration:0.3f animations:^{
                
                CGRect frame = APP_DELEGATE.bottomFooter.frame;
                CGRect frame1 = self.containerScrollView.frame;
                
                frame.origin.y -= diffheight;
                frame1.size.height -= diffheight;
                
                [APP_DELEGATE.bottomFooter setFrame:frame];
                [self.containerScrollView setFrame:frame1];
            } completion:nil];
        }
    }
    keyboardframe = keyboardFrameBeginRect;
    //    [self addButtonToKeyboard];
}

-(void)keyboardDidDisappear:(NSNotification *)notification
{
    //    NSDictionary* keyboardInfo = [notification userInfo];
    //    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    //    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    keyboardAppeared = YES;
    [UIView animateWithDuration:0.3f animations:^{
        
        CGRect frame = APP_DELEGATE.bottomFooter.frame;
        frame.origin.y = APP_DELEGATE.window.frame.size.height-40.0f;
        
        CGRect frame1 = self.containerScrollView.frame;
        frame1.size.height = self.view.frame.size.height-64.0f-46.0f;
        
        [self.containerScrollView setFrame:frame1];
        [APP_DELEGATE.bottomFooter setFrame:frame];
    } completion:nil];
}

#pragma Mark Keyboard Delegate methods
- (void)addButtonToKeyboard {
    UIView *view = [self.view findViewThatIsFirstResponder];
    if ([view isKindOfClass:[UITextField class]] || [view isKindOfClass:[UITextView class]]) {
        if (((UITextView*)view).keyboardType == UIKeyboardTypeNumberPad)
        {
            @try {
                // create custom button
                if (doneButton != nil && doneButton.superview != nil) {
                    [doneButton removeFromSuperview];
                    [self removeButtonFromKeyboard];
                }
                doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [doneButton setTitle:@"Done" forState:UIControlStateNormal];
                [doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                doneButton.frame = CGRectMake(0, 163, 106, 53);
                doneButton.adjustsImageWhenHighlighted = NO;
                [doneButton addTarget:self action:@selector(doneButton:) forControlEvents:UIControlEventTouchUpInside];
                // locate keyboard view
                UIWindow* tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
                UIView* keyboard;
                for(int i=0; i<[tempWindow.subviews count]; i++) {
                    keyboard = [tempWindow.subviews objectAtIndex:i];
                    // keyboard found, add the button
                    if ([[keyboard description] hasPrefix:@"<UIPeripheralHost"] == YES)
                    {
                        [keyboard addSubview:doneButton];
                    }
                    //This code will work on iOS 8.0
                    else if([[keyboard description] hasPrefix:@"<UIInputSetContainerView"] == YES)
                    {
                        for(int i = 0 ; i < [keyboard.subviews count] ; i++)
                        {
                            UIView* hostkeyboard = [keyboard.subviews objectAtIndex:i];
                            if([[hostkeyboard description] hasPrefix:@"<UIInputSetHost"] == YES)
                            {
                                [hostkeyboard addSubview:doneButton];
                            }
                        }
                    }
                }
            }
            @catch (NSException *exception) {}
            @finally {}
        } else {
            if (doneButton != nil && doneButton.superview != nil) {
                [doneButton removeFromSuperview];
                [self removeButtonFromKeyboard];
            }
        }
    }
}

- (void)doneButton:(id)sender {
    [self textFieldShouldReturn:self.mobileTextField];
}


- (void)removeButtonFromKeyboard
{
    NSArray *arTemp = [[UIApplication sharedApplication] windows];
    if ([arTemp count] <= 1) return;
    UIWindow* tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    UIView* keyboard;
    for(int i=0; i<[tempWindow.subviews count]; i++)
    {
        keyboard = [tempWindow.subviews objectAtIndex:i];
        // keyboard found, add the button
        if ([[keyboard description] hasPrefix:@"<UIPeripheralHost"] == YES)
        {
            for (id temp in keyboard.subviews)
            {
                if ([temp isKindOfClass:[UIButton class]])
                {
                    UIButton *btnDone = (UIButton*) temp;
                    [btnDone removeFromSuperview];
                    break;
                }
            }
        }
        //This code will work on iOS 8.0
        else if([[keyboard description] hasPrefix:@"<UIInputSetContainerView"] == YES)
        {
            for(int i = 0 ; i < [keyboard.subviews count] ; i++)
            {
                UIView* hostkeyboard = [keyboard.subviews objectAtIndex:i];
                if([[hostkeyboard description] hasPrefix:@"<UIInputSetHost"] == YES)
                {
                    for (id temp in hostkeyboard.subviews)
                    {
                        if ([temp isKindOfClass:[UIButton class]])
                        {
                            UIButton *btnDone = (UIButton*) temp;
                            [btnDone removeFromSuperview];
                            break;
                        }
                    }
                }
            }
        }
    }
}


@end
