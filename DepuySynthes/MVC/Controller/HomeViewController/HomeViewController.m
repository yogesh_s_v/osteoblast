//
//  HomeViewController.m
//  DepuySynthes
//
//  Created by cdp on 12/20/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeTableViewCell.h"
#import "ProductDetailViewController.h"
#import "CategoryViewController.h"
#import "FeedsWebService.h"
#import "CommonSettings.h"
#import "AsyncImageView.h"
#import "Constants.h"
#import "ProductSurveyViewController.h"

//#define MAX_FONT_SIZE_OFFSET 5.0f
//
//#define COMPUTE_FONT_SIZE(initialFontSize) ((([CommonSettings sharedInstance].fontPercentage*MAX_FONT_SIZE_OFFSET)/100.0f)+initialFontSize)

@interface HomeViewController ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>{
    NSMutableArray *parentCategorySubArray, *allCategorySubArray;//,*allCategoryHeadNameArray,*allCategoryDateLocationArray;
    NSDateFormatter *fromDateFormatter, *toDateFormatter;
}

@property (weak, nonatomic) IBOutlet UISearchBar *articleSerachBar;
@property (weak, nonatomic) IBOutlet UITableView *categoryTableView;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    fromDateFormatter = [[NSDateFormatter alloc] init];
    [fromDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    toDateFormatter = [[NSDateFormatter alloc] init];
    [toDateFormatter setDateFormat:@"MMM dd"];
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, APP_DELEGATE.window.frame.size.width-40.0f, 44.0f)];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-5.0f, 2.0f, 155.0f, 42.0f)];
    titleLabel.text = @"OsteoBlast";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont systemFontOfSize:15.0f];
    [titleView addSubview:titleLabel];
    self.navigationItem.titleView = titleView;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.articleSerachBar.showsCancelButton = YES;
    self.articleSerachBar.placeholder = @"search";
    
    [self.categoryTableView registerNib:[UINib nibWithNibName:@"HomeTableViewCell" bundle:nil] forCellReuseIdentifier:@"HomeTableViewCell"];
    [self.categoryTableView registerNib:[UINib nibWithNibName:@"HomeTableViewCell" bundle:nil] forHeaderFooterViewReuseIdentifier:@"HomeTableViewCell"];
    [self.categoryTableView setTableFooterView:[UIView new]];
    
    // Keyboard events
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        NSDictionary *userInfoCurrent = oneEvent.userInfo;
        NSString *uid=[NSString stringWithFormat:@"%@",[userInfoCurrent valueForKey:@"uid"]];
        if ([uid isEqualToString:APP_DELEGATE.uidToDelete])
        {
            //Cancelling local notification
            [app cancelLocalNotification:oneEvent];
            break;
        }
    }
    [self initializeDataAndView];
    [APP_DELEGATE.bottomFooter setHidden:NO];
}

-(void)initializeDataAndView
{
    FeedsWebService *feedsWebService = [[FeedsWebService alloc] init];
    [feedsWebService getFeedsWithKey:@"password" columns:nil byFeedID:nil byCategoryID:nil limit:0 andCompletionBlock:^(id responseData) {
        DLog(@"Resp: %@", responseData);
        if (responseData && [responseData isKindOfClass:[NSDictionary class]]) {
            responseData = [((NSDictionary*)responseData) objectForKey:@"data"];
            if (responseData && [responseData isKindOfClass:[NSArray class]]) {
                allCategorySubArray = [[NSMutableArray alloc] initWithArray:responseData];
                
                NSMutableArray *arr = [[NSMutableArray alloc]initWithArray:responseData];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arr];
                [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"feedsData"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
                NSData *data1 = [pref objectForKey:@"feedsData"];
                NSArray *catArray = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
                for (id cat in catArray) {
                    id feedDate = [cat objectForKey:@"feed_created_date"];
                    if (feedDate && [feedDate isKindOfClass:[NSString class]]) {
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                        dateFormatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
                        NSDate *catDate = [dateFormatter dateFromString:((NSString*)feedDate)];
//                        NSDate *catDate = [dateFormatter dateFromString:@"2015-02-04 10:07:06"];
                        dateFormatter.dateFormat = @"YYYY-MM-dd";
                        NSString *catDateString = [dateFormatter stringFromDate:catDate];
                        NSString *currentDateString = [dateFormatter stringFromDate:[NSDate date]];
                        if ([catDateString isEqualToString:currentDateString]) {
                            UILocalNotification *localNotification = [[UILocalNotification alloc]init];
//                            currentDateString = @"2015-02-04";
//                            dateFormatter.dateFormat = @"YYYY-MM-dd HH:mm:ss";
//                            localNotification.fireDate = [dateFormatter dateFromString:currentDateString];
                            localNotification.fireDate = [NSDate date];
                            localNotification.alertBody = @"1 new feeds to read";
                            localNotification.repeatInterval = NSCalendarUnitHour;
                            localNotification.timeZone = [NSTimeZone defaultTimeZone];
                            APP_DELEGATE.uidToDelete = @"12345";
                            NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:APP_DELEGATE.uidToDelete,@"uid", nil];
                            localNotification.userInfo = dict;
                            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                            break;
                        }
                    }
                }
                
                if (APP_DELEGATE.selectedMenu != DPMenuNone) {
                    allCategorySubArray = [[NSMutableArray alloc]init];
                    for (id category in (NSArray*)responseData) {
                        id cat_id = [category objectForKey:@"feed_cat_id"];
                        if (cat_id && [cat_id isKindOfClass:[NSString class]]) {
                            DLog(@"selected menu is : %d",(int)APP_DELEGATE.selectedMenu);
                            switch (APP_DELEGATE.selectedMenu) {
                                case DPMenuJournal:
                                {
                                    if ([cat_id isEqualToString:@"1"]) {
                                        [allCategorySubArray addObject:category];
                                    }
                                    break;
                                }
                                case DPMenuConferencesAndWorkshops:
                                {
                                    if ([cat_id isEqualToString:@"2"]) {
                                        [allCategorySubArray addObject:category];
                                    }
                                    break;
                                }
                                case DPMenuCaseStudies:
                                {
                                    if ([cat_id isEqualToString:@"3"]) {
                                        [allCategorySubArray addObject:category];
                                    }
                                    break;
                                }
                                case DPMenuProfEducation:
                                {
                                    if ([cat_id isEqualToString:@"4"]) {
                                        [allCategorySubArray addObject:category];
                                    }
                                    break;
                                }
                                case DPMenuTakeASurvey:
                                {
                                    if ([cat_id isEqualToString:@"5"]) {
                                        [allCategorySubArray addObject:category];
                                    }
                                    break;
                                }
                                default:
                                {
                                    [allCategorySubArray addObject:category];
                                    break;
                                }
                            }
                        }
                    }
                }
                parentCategorySubArray = [[NSMutableArray alloc] initWithArray:allCategorySubArray];
                [self searchContentWithText:self.articleSerachBar.text];
//                [self.categoryTableView reloadData];
            }
        }
    }];
}

//-(void)onCategoryButtonClicked
//{
//    CategoryViewController *categoryViewController = [[CategoryViewController alloc] init];
//    [self.navigationController pushViewController:categoryViewController animated:YES];
//}

-(void)viewDidLayoutSubviews
{
    // iOS 7
    if ([self.categoryTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.categoryTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    /*
    // iOS 8
    if ([self.categoryTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.categoryTableView setLayoutMargins:UIEdgeInsetsZero];
    }
     */
}

#pragma mark - TableView Data source methods
//
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [allCategorySubArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"HomeTableViewCell";
    
    HomeTableViewCell *cell = (HomeTableViewCell*) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.categoryTitleLabel.text = nil;
    cell.categoryNewsHeadingLabel.text = nil;
    cell.categoryNewsSubHeadingLabel.text = nil;
    cell.categoryNewsDateLabel.text = nil;
    id dict = [allCategorySubArray objectAtIndex:indexPath.row];
    if (dict && [dict isKindOfClass:[NSDictionary class]]) {
        cell.categoryNewsHeadingLabel.text = [dict objectForKey:@"feed_title"];
        if ([[dict objectForKey:@"feed_shortdesc"] isKindOfClass:[NSString class]]) {
            cell.categoryNewsSubHeadingLabel.text = [dict objectForKey:@"feed_shortdesc"];
        }
        cell.categoryNewsDateLabel.text = [dict objectForKey:@"feed_created_date"];
        
        float y = 16.0f;
        
        NSString *categoryTitle = @"";
        id categoryID = [dict objectForKey:@"feed_cat_id"];
        if (categoryID && ([categoryID isKindOfClass:[NSString class]] || [categoryID isKindOfClass:[NSString class]])) {
            if ([categoryID intValue] == 1) {
                categoryTitle = @"Journal Articles";
                cell.categoryIconImageView.image = [UIImage imageNamed:@"JournalArticles"];
                cell.categoryTitleLabel.textColor = [UIColor colorWithRed:251.0/255.0 green:0.0/255.0 blue:8.0/255.0 alpha:1.0];
            } else if ([categoryID intValue] == 2) {
                categoryTitle = @"Events";
                cell.categoryIconImageView.image = [UIImage imageNamed:@"Events"];
                cell.categoryTitleLabel.textColor = [UIColor colorWithRed:22.0/255.0 green:165.0/255.0 blue:63.0/255.0 alpha:1.0];
            }else if ([categoryID intValue] == 3) {
                categoryTitle = @"Case Studies";
                cell.categoryIconImageView.image = [UIImage imageNamed:@"CaseStudies"];
                cell.categoryTitleLabel.textColor = [UIColor colorWithRed:254.0/255.0 green:180.0/255.0 blue:9.0/255.0 alpha:1.0];
            } else if ([categoryID intValue] == 4) {
                categoryTitle = @"Professional Education";
                cell.categoryIconImageView.image = [UIImage imageNamed:@"ProfEd"];
                cell.categoryTitleLabel.textColor = [UIColor colorWithRed:11.0/255.0 green:90.0/255.0 blue:179.0/255.0 alpha:1.0];
            }else if ([categoryID intValue] == 5) {
                categoryTitle = @"Survey";
                cell.categoryIconImageView.image = [UIImage imageNamed:@"Feedback"];
                cell.categoryTitleLabel.textColor = [UIColor colorWithRed:46.0/255.0 green:46.0/255.0 blue:46.0/255.0 alpha:1.0];
            }else {//if ([categoryID intValue] == 3) {
                categoryTitle = @"Unknown";
                categoryID = nil;
            }
            NSDictionary *titleAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(16.0f)]};
            CGSize size = [categoryTitle boundingRectWithSize:CGSizeMake(self.view.frame.size.width-86.0f, 35.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:titleAttr context:nil].size;
            
            CGRect frame = cell.categoryTitleLabel.frame;
            frame.origin.y = y;
            y += size.height+10.0f;
            frame.size.height = size.height+5.0f;
            cell.categoryTitleLabel.frame = frame;
            cell.categoryTitleLabel.text = categoryTitle;
            //cell.categoryTitleLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(16.0f)];
            cell.categoryTitleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
        }
        
        id title = [dict objectForKey:@"feed_title"];
        if ([categoryID intValue] == 1) {
            title = [NSString stringWithFormat:@"%@. %@", title, [dict objectForKey:@"feed_shortdesc"]];
        }
        if (title && [title isKindOfClass:[NSString class]]) {
            NSDictionary *titleAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(16.0f)]};
            CGSize size = [title boundingRectWithSize:CGSizeMake(self.view.frame.size.width-86.0f, 200.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:titleAttr context:nil].size;
            
            
            
            CGRect frame = cell.categoryNewsHeadingLabel.frame;
            frame.origin.y = y;
            y += size.height+10.0f;
            frame.size.height = size.height+5.0f;
            cell.categoryNewsHeadingLabel.frame = frame;
            cell.categoryNewsHeadingLabel.text = title;
            cell.categoryNewsHeadingLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(16.0f)];
        }
        
        id shortdesc = [dict objectForKey:@"feed_shortdesc"];
        if (categoryID && ([categoryID isKindOfClass:[NSString class]] || [categoryID isKindOfClass:[NSString class]])) {
            if ([categoryID intValue] == 1) {
                shortdesc = [NSString stringWithFormat:@"Authors: %@", [dict objectForKey:@"feed_authors"]];
            }else if ([categoryID intValue] == 2) {
                shortdesc = [dict objectForKey:@"feed_conf_venue"];
            }
        }
        
        if (shortdesc && [shortdesc isKindOfClass:[NSString class]]) {
            NSDictionary *shortDescAttr;
            if ([shortdesc intValue] == 1) {
                shortDescAttr = @{ NSFontAttributeName : [UIFont italicSystemFontOfSize:COMPUTE_FONT_SIZE(12.0f)]};
            }else {
                shortDescAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(13.0f)]};
            }
            
            CGSize size = [shortdesc boundingRectWithSize:CGSizeMake(self.view.frame.size.width-86.0f, 1000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:shortDescAttr context:nil].size;
            
            CGRect frame = cell.categoryNewsSubHeadingLabel.frame;
            frame.origin.y = y;
            y += size.height+10.0f;
            frame.size.height = size.height+5.0f;
            cell.categoryNewsSubHeadingLabel.frame = frame;
            if (categoryID && ([categoryID isKindOfClass:[NSString class]] || [categoryID isKindOfClass:[NSString class]])) {
                cell.categoryNewsSubHeadingLabel.text = shortdesc;
            }
            if ([categoryID intValue] == 1) {
                cell.categoryNewsSubHeadingLabel.font = [UIFont italicSystemFontOfSize:COMPUTE_FONT_SIZE(12.0f)];
            }else {
                cell.categoryNewsSubHeadingLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(13.0f)];
            }
        }
        
        id creationDate = [dict objectForKey:@"feed_created_date"];
        if (creationDate && [creationDate isKindOfClass:[NSString class]]) {
            NSDate *date = [fromDateFormatter dateFromString:creationDate];
//            CGRect frame = cell.categoryNewsDateLabel.frame;
//            frame.origin.y = y;
//            y += 30.0f;
//            cell.categoryNewsDateLabel.frame = frame;
            cell.categoryNewsDateLabel.text = [toDateFormatter stringFromDate:date];
            cell.categoryNewsDateLabel.font = [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(13.0f)];
            cell.categoryNewsDateLabel.textColor = [UIColor lightGrayColor];
        }
        
        {
            cell.categoryImageView.hidden = YES;
//            id feedImageURL = [dict objectForKey:@"feed_image"];
//            if (feedImageURL && [feedImageURL isKindOfClass:[NSString class]] && ![feedImageURL isEqualToString:@""]) {
//                [((AsyncImageView*)cell.categoryImageView) setImageURL:[NSURL URLWithString:feedImageURL]];
//                cell.categoryImageView.hidden = NO;
//                
//                CGRect frame = cell.categoryImageView.frame;
//                frame.origin.y = y;
//                cell.categoryImageView.frame = frame;
//            }
        }
        cell.separatorInset = UIEdgeInsetsZero;
        return cell;
    }
    cell.separatorInset = UIEdgeInsetsZero;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    float height = 16.0f;//85.0f;
    id dict = [allCategorySubArray objectAtIndex:indexPath.row];
    if (dict && [dict isKindOfClass:[NSDictionary class]]) {
        NSString *categoryTitle = @"";
        id categoryID = [dict objectForKey:@"feed_cat_id"];
        if (categoryID && ([categoryID isKindOfClass:[NSString class]] || [categoryID isKindOfClass:[NSString class]])) {
            if ([categoryID intValue] == 1) {
                categoryTitle = @"Journal";
            } else if ([categoryID intValue] == 2) {
                categoryTitle = @"Conference & Workshops";
            } else {//if ([categoryID intValue] == 3) {
                categoryTitle = @"Unknown";
            }
            NSDictionary *titleAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(16.0f)]};
            CGSize size = [categoryTitle boundingRectWithSize:CGSizeMake(self.view.frame.size.width-86.0f, 35.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:titleAttr context:nil].size;
            height += size.height+10.0f;
        }
        id title = [dict objectForKey:@"feed_title"];
        if ([categoryID intValue] == 1) {
            title = [NSString stringWithFormat:@"%@. %@", title, [dict objectForKey:@"feed_shortdesc"]];
        }
        if (title && [title isKindOfClass:[NSString class]]) {
            NSDictionary *titleAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(16.0f)]};
            CGSize size = [title boundingRectWithSize:CGSizeMake(self.view.frame.size.width-86.0f, 200.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:titleAttr context:nil].size;
            height += size.height+10.0f;
        }
        
        id shortdesc = [dict objectForKey:@"feed_shortdesc"];
        if (categoryID && ([categoryID isKindOfClass:[NSString class]] || [categoryID isKindOfClass:[NSString class]])) {
            if ([categoryID intValue] == 1) {
                shortdesc = [NSString stringWithFormat:@"Authors: %@", [dict objectForKey:@"feed_authors"]];
            }else if ([categoryID intValue] == 2) {
                shortdesc = [dict objectForKey:@"feed_conf_venue"];
            }
        }
        if (shortdesc && [shortdesc isKindOfClass:[NSString class]]) {
            NSDictionary *shortDescAttr;
            if ([categoryID intValue] == 1) {
                shortDescAttr = @{ NSFontAttributeName : [UIFont italicSystemFontOfSize:COMPUTE_FONT_SIZE(12.0f)]};
            }else {
                shortDescAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:COMPUTE_FONT_SIZE(13.0f)]};
            }
            CGSize size = [shortdesc boundingRectWithSize:CGSizeMake(self.view.frame.size.width-86.0f, 1000.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:shortDescAttr context:nil].size;
            height += size.height+15.0f;
        }
        
        height += 10.0f;
        
//        id feedImageURL = [dict objectForKey:@"feed_image"];
//        if (feedImageURL && [feedImageURL isKindOfClass:[NSString class]] && ![feedImageURL isEqualToString:@""]) {
//            height += 240.0f;
//        }
    }
    
    if (height<70.0f) {
        height = 70.0f;
    }
    
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    id category = [allCategorySubArray objectAtIndex:indexPath.row];
    if (category && [category isKindOfClass:[NSDictionary class]]) {
        id catId = [category objectForKey:@"feed_cat_id"];
        if (catId && [catId isKindOfClass:[NSString class]]) {
            if ([catId integerValue] == 5) {
                ProductSurveyViewController *productSurveyViewController = [[ProductSurveyViewController alloc]initWithNibName:@"ProductSurveyViewController" bundle:nil];
                productSurveyViewController.surveyDetails = [allCategorySubArray objectAtIndex:indexPath.row];
                [self.navigationController pushViewController:productSurveyViewController animated:YES];
            }else {
                ProductDetailViewController *productDetailViewController = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
                productDetailViewController.productDetails = [allCategorySubArray objectAtIndex:indexPath.row];
                [self.navigationController pushViewController:productDetailViewController animated:YES];
            }
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // iOS 7
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    /*
    // iOS 8
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
     */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Search Bar Delegate Methods 

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self searchContentWithText:searchBar.text];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [self searchContentWithText:searchText];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    if (![searchBar.text isEqualToString:@""]) {
        [self initializeDataAndView];
    }
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    if (![searchBar.text isEqualToString:@""]) {
        [self initializeDataAndView];
    }
}

-(void)searchContentWithText:(NSString*)searchText {
    if ([searchText isEqualToString:@""]) {
        allCategorySubArray = parentCategorySubArray;
    } else {
        NSMutableArray *tempCategoryArray = [[NSMutableArray alloc]init];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(self.feed_title CONTAINS[c] %@) OR (self.feed_shortdesc CONTAINS[c] %@) OR (self.feed_data CONTAINS[c] %@)", searchText, searchText, searchText];
        [tempCategoryArray addObjectsFromArray:[parentCategorySubArray filteredArrayUsingPredicate:predicate]];
        allCategorySubArray = tempCategoryArray;
    }
    [self.categoryTableView reloadData];
}

#pragma mark - Keyboard events

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGRect frame = APP_DELEGATE.bottomFooter.frame;
    frame.origin.y = APP_DELEGATE.window.frame.size.height-40.0f-kbSize.height;
    
    CGRect frame1 = self.categoryTableView.frame;
    frame1.size.height = self.view.frame.size.height-kbSize.height-64.0f-84.0f;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.categoryTableView.frame = frame1;
        APP_DELEGATE.bottomFooter.frame = frame;
    }];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    CGRect frame = APP_DELEGATE.bottomFooter.frame;
    frame.origin.y = APP_DELEGATE.window.frame.size.height-40.0f;
    
    CGRect frame1 = self.categoryTableView.frame;
    frame1.size.height = self.view.frame.size.height-64.0f-84.0f;
    
    [UIView animateWithDuration:0.2f animations:^{
        APP_DELEGATE.bottomFooter.frame = frame;
        self.categoryTableView.frame = frame1;
    }];
}

@end
