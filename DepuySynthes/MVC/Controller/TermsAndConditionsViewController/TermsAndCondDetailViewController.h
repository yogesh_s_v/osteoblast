//
//  TermsAndCondDetailViewController.h
//  DepuySynthes
//
//  Created by Akshay Gohel on 20/01/15.
//  Copyright (c) 2015 The_world_inc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    DPTextPrivacyPolicy    = 0,
    DPTextLegalNotice      = 1,
    DPTextAboutUs          = 2,
}DPText;

@interface TermsAndCondDetailViewController : UIViewController

@property (nonatomic) DPText selectedText;

@end
