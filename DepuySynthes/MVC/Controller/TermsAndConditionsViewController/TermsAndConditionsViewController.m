//
//  TermsAndConditionsViewController.m
//  DepuySynthes
//
//  Created by Akshay Gohel on 20/01/15.
//  Copyright (c) 2015 The_world_inc. All rights reserved.
//

#import "TermsAndConditionsViewController.h"
#import "TermsAndCondDetailViewController.h"
#import "ContactUsViewController.h"
#import "Constants.h"

@interface TermsAndConditionsViewController ()<UITableViewDataSource,UITableViewDelegate> {
    NSMutableArray *containerArray;
}

@property (weak, nonatomic) IBOutlet UITableView *containerTableView;

@end

@implementation TermsAndConditionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.containerTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.containerTableView setTableFooterView:[UIView new]];
    
    containerArray = [[NSMutableArray alloc]initWithObjects:@"Privacy Policy",@"Legal Notice",@"About Us",@"Contact Us", nil];
    self.automaticallyAdjustsScrollViewInsets = NO;
}

-(void)viewDidAppear:(BOOL)animated {
    [APP_DELEGATE.bottomFooter setHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return containerArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.font = [UIFont systemFontOfSize:10.0f];
    cell.textLabel.text = [containerArray objectAtIndex:indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != 3) {
        TermsAndCondDetailViewController *termsAndCondDetailViewController = [[TermsAndCondDetailViewController alloc]initWithNibName:@"TermsAndCondDetailViewController" bundle:nil];
        switch ((int)indexPath.row) {
            case 0:
                termsAndCondDetailViewController.selectedText = DPTextPrivacyPolicy;
                break;
            case 1:
                termsAndCondDetailViewController.selectedText = DPTextLegalNotice;
                break;
            case 2:
                termsAndCondDetailViewController.selectedText = DPTextAboutUs;
                break;
            default:
                break;
        }
        [self.navigationController pushViewController:termsAndCondDetailViewController animated:YES];
    }else {
        ContactUsViewController *contactUsViewController = [[ContactUsViewController alloc]initWithNibName:@"ContactUsViewController" bundle:Nil];
        [self.navigationController pushViewController:contactUsViewController animated:YES];
    }
    [self.containerTableView deselectRowAtIndexPath:indexPath animated:YES];
}

//-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.row != 3) {
//        TermsAndCondDetailViewController *termsAndCondDetailViewController = [[TermsAndCondDetailViewController alloc]initWithNibName:@"TermsAndCondDetailViewController" bundle:nil];
//        [self.navigationController pushViewController:termsAndCondDetailViewController animated:YES];
//    }
//}

@end
