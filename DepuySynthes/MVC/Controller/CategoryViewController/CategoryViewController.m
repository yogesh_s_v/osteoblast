//
//  CategoryViewController.m
//  DepuySynthes
//
//  Created by cdp on 12/25/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import "CategoryViewController.h"
#import "CategoryTableViewCell.h"
#import "OrganDetailViewController.h"
#import "ProductWebService.h"
#import "Constants.h"

@interface CategoryViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UIImageView *skeletonImageView, *infoImageView;
    __weak IBOutlet UIView *buttonContainer;
    __weak IBOutlet UIButton *shoulderButton, *kneeButton, *hipButton;
    NSMutableArray *partsArray;
    NSString *partIdString,*partNameString;
    BOOL infoImageViewAbove;
}

@end

@implementation CategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initializeDataAndView];
    [infoImageView setAlpha:0.0f];
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, APP_DELEGATE.window.frame.size.width-40.0f, 44.0f)];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-5.0f, 2.0f, 155.0f, 42.0f)];
    titleLabel.text = @"Product Catalog";
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont systemFontOfSize:15.0f];
    [titleView addSubview:titleLabel];
    self.navigationItem.titleView = titleView;
    
    infoImageViewAbove = NO;
}

-(void)viewDidAppear:(BOOL)animated {
    [APP_DELEGATE.bottomFooter setHidden:YES];
}

-(void)initializeDataAndView
{
    ProductWebService *productWebService = [[ProductWebService alloc] init];
    [productWebService getProductPartWithKey:@"password" byProductID:nil andCompletionBlock:^(id responseData) {
        DLog(@"ResponseData: %@", responseData);
        responseData = [responseData objectForKey:@"data"];
        if (responseData && [responseData isKindOfClass:[NSArray class]]) {
            partsArray = [[NSMutableArray alloc] initWithArray:responseData];
        }
    }];
}

-(IBAction)shoulderButtonTapped:(id)sender {
    for (int i = 0; i <partsArray.count; i++) {
        id part = [partsArray objectAtIndex:i];
        if (part && [part isKindOfClass:[NSDictionary class]]) {
            id partName = [part objectForKey:@"Parts_Name"];
            if (partName && [partName isKindOfClass:[NSString class]] && [partName isEqualToString:@"Shoulder"]) {
                id Parts_ID = [part objectForKey:@"Parts_ID"];
                if (Parts_ID && [Parts_ID isKindOfClass:[NSString class]]) {
                    partIdString = Parts_ID;
                }
                partNameString = partName;
                [self buttonTapped];
            }
        }
    }
}
//{
//    for (int i = 0; i <partsArray.count; i++) {
//        id part = [partsArray objectAtIndex:i];
//        if (part && [part isKindOfClass:[NSDictionary class]]) {
//            id partName = [part objectForKey:@"Parts_Name"];
//            if (partName && [partName isKindOfClass:[NSString class]] && [partName isEqualToString:@"Shoulder"]) {
//                UIImageView *skeletonIV = nil;
//                UIImageView *infoIV = nil;
//                
//                if (infoImageViewAbove) {
//                    infoIV = skeletonImageView;
//                    skeletonIV = infoImageView;
//                } else {
//                    infoIV = infoImageView;
//                    skeletonIV = skeletonImageView;
//                }
//                infoImageViewAbove = !infoImageViewAbove;
//                [infoIV setAlpha:0.0f];
//                [infoIV setImage:[UIImage imageNamed:@"skeleton_shoulder"]];
//                [self.view bringSubviewToFront:infoIV];
//                [self.view sendSubviewToBack:skeletonIV];
//                [self.view bringSubviewToFront:buttonContainer];
//                [UIView animateWithDuration:0.4f animations:^{
//                    [infoIV setAlpha:1.0f];
////                    [skeletonIV setAlpha:0.0f];
//                } completion:^(BOOL finished) {
//                    [infoIV setAlpha:1.0f];
////                    [skeletonIV setAlpha:0.0f];
//                }];
//                
//                id Parts_ID = [part objectForKey:@"Parts_ID"];
//                if (Parts_ID && [Parts_ID isKindOfClass:[NSString class]]) {
//                    partIdString = Parts_ID;
//                }
//                partNameString = partName;
//            }
//        }
//    }
//}

-(IBAction)kneeButtonTapped:(id)sender {
    for (int i = 0; i <partsArray.count; i++) {
        id part = [partsArray objectAtIndex:i];
        if (part && [part isKindOfClass:[NSDictionary class]]) {
            id partName = [part objectForKey:@"Parts_Name"];
            if (partName && [partName isKindOfClass:[NSString class]] && [partName isEqualToString:@"Knee"]) {
                id Parts_ID = [part objectForKey:@"Parts_ID"];
                if (Parts_ID && [Parts_ID isKindOfClass:[NSString class]]) {
                    partIdString = Parts_ID;
                }
                partNameString = partName;
                [self buttonTapped];
            }
        }
    }
    
}
//{
//    for (int i = 0; i <partsArray.count; i++) {
//        id part = [partsArray objectAtIndex:i];
//        if (part && [part isKindOfClass:[NSDictionary class]]) {
//            id partName = [part objectForKey:@"Parts_Name"];
//            if (partName && [partName isKindOfClass:[NSString class]] && [partName isEqualToString:@"Knee"]) {
//                UIImageView *skeletonIV = nil;
//                UIImageView *infoIV = nil;
//                
//                if (infoImageViewAbove) {
//                    infoIV = skeletonImageView;
//                    skeletonIV = infoImageView;
//                } else {
//                    infoIV = infoImageView;
//                    skeletonIV = skeletonImageView;
//                }
//                infoImageViewAbove = !infoImageViewAbove;
//                [infoIV setAlpha:0.0f];
//                [infoIV setImage:[UIImage imageNamed:@"skeleton_knee"]];
//                [self.view bringSubviewToFront:infoIV];
//                [self.view sendSubviewToBack:skeletonIV];
//                [self.view bringSubviewToFront:buttonContainer];
//                [UIView animateWithDuration:0.4f animations:^{
//                    [infoIV setAlpha:1.0f];
////                    [skeletonIV setAlpha:0.0f];
//                } completion:^(BOOL finished) {
//                    [infoIV setAlpha:1.0f];
////                    [skeletonIV setAlpha:0.0f];
//                }];
//                
//                id Parts_ID = [part objectForKey:@"Parts_ID"];
//                if (Parts_ID && [Parts_ID isKindOfClass:[NSString class]]) {
//                    partIdString = Parts_ID;
//                }
//                partNameString = partName;
//            }
//        }
//    }
//}

-(IBAction)hipButtonTapped:(id)sender {
    for (int i = 0; i <partsArray.count; i++) {
        id part = [partsArray objectAtIndex:i];
        if (part && [part isKindOfClass:[NSDictionary class]]) {
            id partName = [part objectForKey:@"Parts_Name"];
            if (partName && [partName isKindOfClass:[NSString class]] && [partName isEqualToString:@"Hip"]) {
                id Parts_ID = [part objectForKey:@"Parts_ID"];
                if (Parts_ID && [Parts_ID isKindOfClass:[NSString class]]) {
                    partIdString = Parts_ID;
                }
                partNameString = partName;
                [self buttonTapped];
            }
        }
    }
}
//{
//    for (int i = 0; i <partsArray.count; i++) {
//        id part = [partsArray objectAtIndex:i];
//        if (part && [part isKindOfClass:[NSDictionary class]]) {
//            id partName = [part objectForKey:@"Parts_Name"];
//            if (partName && [partName isKindOfClass:[NSString class]] && [partName isEqualToString:@"Hip"]) {
//                UIImageView *skeletonIV = nil;
//                UIImageView *infoIV = nil;
//                
//                if (infoImageViewAbove) {
//                    infoIV = skeletonImageView;
//                    skeletonIV = infoImageView;
//                } else {
//                    infoIV = infoImageView;
//                    skeletonIV = skeletonImageView;
//                }
//                infoImageViewAbove = !infoImageViewAbove;
//                [infoIV setAlpha:0.0f];
//                [infoIV setImage:[UIImage imageNamed:@"skeleton_hip"]];
//                [self.view bringSubviewToFront:infoIV];
//                [self.view sendSubviewToBack:skeletonIV];
//                [self.view bringSubviewToFront:buttonContainer];
//                [UIView animateWithDuration:0.4f animations:^{
//                    [infoIV setAlpha:1.0f];
////                    [skeletonIV setAlpha:0.0f];
//                } completion:^(BOOL finished) {
//                    [infoIV setAlpha:1.0f];
////                    [skeletonIV setAlpha:0.0f];
//                }];
//                
//                id Parts_ID = [part objectForKey:@"Parts_ID"];
//                if (Parts_ID && [Parts_ID isKindOfClass:[NSString class]]) {
//                    partIdString = Parts_ID;
//                }
//                partNameString = partName;
//            }
//        }
//    }
//}

-(IBAction)revisionButtonTapped:(id)sender {
    for (int i = 0; i <partsArray.count; i++) {
        id part = [partsArray objectAtIndex:i];
        if (part && [part isKindOfClass:[NSDictionary class]]) {
            id partName = [part objectForKey:@"Parts_Name"];
            if (partName && [partName isKindOfClass:[NSString class]] && [partName isEqualToString:@"Revision"]) {
                id Parts_ID = [part objectForKey:@"Parts_ID"];
                if (Parts_ID && [Parts_ID isKindOfClass:[NSString class]]) {
                    partIdString = Parts_ID;
                }
                partNameString = partName;
                [self buttonTapped];
            }
        }
    }
}

-(void) buttonTapped {
    if (partIdString && ![partIdString isEqualToString:@""]) {
        ProductWebService *productWebService = [[ProductWebService alloc]init];
        [skeletonImageView setUserInteractionEnabled:NO];
        [infoImageView setUserInteractionEnabled:NO];
        
        [productWebService getProductCategoryWithKey:@"password" byProductID:nil byPartID:partIdString andCompletionBlock:^(id responsedata) {
            DLog(@"ResponseData : %@",responsedata);
            responsedata = [responsedata objectForKey:@"data"];
            if (responsedata && [responsedata isKindOfClass:[NSArray class]]) {
                OrganDetailViewController *organDetailViewController = [[OrganDetailViewController alloc]init];
                organDetailViewController.organCategoryArray = responsedata;
                
                UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, APP_DELEGATE.window.frame.size.width-40.0f, 44.0f)];
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-5.0f, 2.0f, 155.0f, 42.0f)];
                titleLabel.text = partNameString;
                titleLabel.textColor = [UIColor whiteColor];
                titleLabel.font = [UIFont systemFontOfSize:15.0f];
                [titleView addSubview:titleLabel];
                organDetailViewController.navigationItem.titleView = titleView;
                
                [self.navigationController pushViewController:organDetailViewController animated:YES];
            }
            [skeletonImageView setUserInteractionEnabled:YES];
            [infoImageView setUserInteractionEnabled:YES];
        }];
    }
}

#pragma mark - touch methods

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if (touch.view == skeletonImageView || touch.view == infoImageView)  {
        if (partIdString && ![partIdString isEqualToString:@""]) {
            ProductWebService *productWebService = [[ProductWebService alloc]init];
            [skeletonImageView setUserInteractionEnabled:NO];
            [infoImageView setUserInteractionEnabled:NO];
            
            [productWebService getProductCategoryWithKey:@"password" byProductID:nil byPartID:partIdString andCompletionBlock:^(id responsedata) {
                DLog(@"ResponseData : %@",responsedata);
                responsedata = [responsedata objectForKey:@"data"];
                if (responsedata && [responsedata isKindOfClass:[NSArray class]]) {
                    OrganDetailViewController *organDetailViewController = [[OrganDetailViewController alloc]init];
                    organDetailViewController.organCategoryArray = responsedata;
                    
                    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, APP_DELEGATE.window.frame.size.width-40.0f, 44.0f)];
                    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-5.0f, 2.0f, 155.0f, 42.0f)];
                    titleLabel.text = partNameString;
                    titleLabel.textColor = [UIColor whiteColor];
                    titleLabel.font = [UIFont systemFontOfSize:15.0f];
                    [titleView addSubview:titleLabel];
                    organDetailViewController.navigationItem.titleView = titleView;
                    
                    [self.navigationController pushViewController:organDetailViewController animated:YES];
                }
                [skeletonImageView setUserInteractionEnabled:YES];
                [infoImageView setUserInteractionEnabled:YES];
            }];
        }
    }
}


#pragma mark - Table View data source methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CategoryTableViewCell";
    
    CategoryTableViewCell *cell = (CategoryTableViewCell*) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

//    id part = [partsArray objectAtIndex:indexPath.row];
//    if ([part isKindOfClass:[NSDictionary class]]) {
//        cell.categoryHeadingLabel.text = [part objectForKey:@"Parts_Name"];
//        NSString *categoryHeadingText = [part objectForKey:@"Parts_Name"];
//        
//        NSDictionary *categoryHeadingAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:19.0f]};
//        
//        CGSize size = [categoryHeadingText boundingRectWithSize:CGSizeMake(self.view.frame.size.width-96.0f, 90.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:categoryHeadingAttr context:nil].size;
//        CGRect frame = cell.categoryHeadingLabel.frame;
//        frame.size.height = size.height+5.0f;
//        cell.categoryHeadingLabel.frame = frame;
//        
//        cell.categoryDescriptionTextView.text = [part objectForKey:@"Parts_Desc"];
//        NSString *categoryDescText = [part objectForKey:@"Parts_Desc"];
//        NSDictionary *categoryDescAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:14.0f]};
//        
//        size = [categoryDescText boundingRectWithSize:CGSizeMake(self.view.frame.size.width-96.0f, 900.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:categoryDescAttr context:nil].size;
//        frame = cell.categoryDescriptionTextView.frame;
//        frame.origin.y = cell.categoryHeadingLabel.frame.origin.y + cell.categoryHeadingLabel.frame.size.height + 5.0f;
//        frame.size.height = size.height+5.0f;
//        cell.categoryDescriptionTextView.frame = frame;
//    }
//    
//    cell.categoryImageView.center = CGPointMake(cell.categoryImageView.center.x, (cell.categoryDescriptionTextView.frame.origin.y + cell.categoryDescriptionTextView.frame.size.height + 10.0f)/2.0f);
//    
//    if ([cell.categoryHeadingLabel.text isEqualToString:@"Knee"]) {
//        cell.categoryImageView.image = [UIImage imageNamed:@"knee"];
//    }
//    else if ([cell.categoryHeadingLabel.text isEqualToString:@"Hip"])
//    {
//        cell.categoryImageView.image = [UIImage imageNamed:@"hip"];
//    }
//    else if ([cell.categoryHeadingLabel.text isEqualToString:@"Shoulder"])
//    {
//        cell.categoryImageView.image = [UIImage imageNamed:@"shoulder"];
//    }
//    else if ([cell.categoryHeadingLabel.text isEqualToString:@"Revision"])
//    {
//        cell.categoryImageView.image = [UIImage imageNamed:@"revision"];
//    }
    return cell;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    float height = 17.0f;
//    id part = [partsArray objectAtIndex:indexPath.row];
//    if ([part isKindOfClass:[NSDictionary class]]) {
//        NSString *categoryHeadingText = [part objectForKey:@"Parts_Name"];
//        
//        NSDictionary *categoryHeadingAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:19.0f]};
//        
//        CGSize size = [categoryHeadingText boundingRectWithSize:CGSizeMake(self.view.frame.size.width-96.0f, 90.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:categoryHeadingAttr context:nil].size;
//        height += size.height+10.0f;
//        
//        NSString *categoryDescText = [part objectForKey:@"Parts_Desc"];
//        NSDictionary *categoryDescAttr = @{ NSFontAttributeName : [UIFont systemFontOfSize:14.0f]};
//        
//        size = [categoryDescText boundingRectWithSize:CGSizeMake(self.view.frame.size.width-100.0f, 900.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:categoryDescAttr context:nil].size;
//        height += size.height+17.0f;
//    }
//    
//    if (height < 120.0f) {
//        height = 120.0f;
//    }
//    return height;
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < 4) {
        id part = [partsArray objectAtIndex:indexPath.row];
        if (part && [part isKindOfClass:[NSDictionary class]]) {
            id Parts_ID = [part objectForKey:@"Parts_ID"];
            if (Parts_ID && [Parts_ID isKindOfClass:[NSString class]]) {
                ProductWebService *productWebService = [[ProductWebService alloc]init];
                [productWebService getProductCategoryWithKey:@"password" byProductID:nil byPartID:Parts_ID andCompletionBlock:^(id responsedata) {
                    DLog(@"ResponseData : %@",responsedata);
                    responsedata = [responsedata objectForKey:@"data"];
                    if (responsedata && [responsedata isKindOfClass:[NSArray class]]) {
                        OrganDetailViewController *organDetailViewController = [[OrganDetailViewController alloc]init];
                        organDetailViewController.organCategoryArray = responsedata;
                        
                        UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, APP_DELEGATE.window.frame.size.width-40.0f, 44.0f)];
                        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-5.0f, 2.0f, 155.0f, 42.0f)];
                        titleLabel.text = [part objectForKey:@"Parts_Name"];
                        titleLabel.textColor = [UIColor whiteColor];
                        titleLabel.font = [UIFont systemFontOfSize:15.0f];
                        [titleView addSubview:titleLabel];
                        organDetailViewController.navigationItem.titleView = titleView;
                        
                        [self.navigationController pushViewController:organDetailViewController animated:YES];
                    }
                }];
            }
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
