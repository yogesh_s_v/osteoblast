//
//  MenuViewController.m
//  DepuySynthes
//
//  Created by Akshay Gohel on 05/01/15.
//  Copyright (c) 2015 The_world_inc. All rights reserved.
//

#import "MenuViewController.h"
#import "Constants.h"

@interface MenuViewController () <UITableViewDataSource,UITableViewDelegate> {
    NSArray *mainMenuItems, *mainMenuIcons;
}
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;

@end

@implementation MenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    mainMenuItems = [[NSMutableArray alloc]initWithObjects:@"Journal Articles",@"Events",@"Case Studies",@"Prof Education",@"Survey",@"Products",@"Contact Us", nil];
    mainMenuIcons = [[NSMutableArray alloc]initWithObjects:@"JournalArticles48",@"Events48",@"CaseStudies48",@"ProfEd48",@"Feedback48",@"MenuProductImage",@"OsteoBlastLogo", nil];
    
    [self.menuTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    self.menuTableView.backgroundColor = [UIColor whiteColor];
    CGSize size = self.view.bounds.size;
    size.height = ((int)mainMenuItems.count)*31.0f;
    self.preferredContentSize = size;
    self.menuTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return mainMenuItems.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    [[cell viewWithTag:123321] removeFromSuperview];
    cell.textLabel.text = (NSString*)[mainMenuItems objectAtIndex:(int)indexPath.row];
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize:13.0f];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.highlightedTextColor = [UIColor blackColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    cell.selectedBackgroundView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 30.0f, 8.0f, 20.0f, 20.0f)];
    iconImageView.tag = 123321;
    iconImageView.image = [UIImage imageNamed:(NSString*)[mainMenuIcons objectAtIndex:(int)indexPath.row]];
    iconImageView.highlightedImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@", (NSString*)[mainMenuIcons objectAtIndex:(int)indexPath.row]]];
    [cell addSubview:iconImageView];
    cell.backgroundColor = [UIColor colorWithRed:0.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.4f];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 31.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        APP_DELEGATE.selectedMenu = DPMenuJournal;
    }else if (indexPath.row == 1) {
        APP_DELEGATE.selectedMenu = DPMenuConferencesAndWorkshops;
    }else if (indexPath.row == 2) {
        APP_DELEGATE.selectedMenu = DPMenuCaseStudies;
    }else if (indexPath.row == 3) {
        APP_DELEGATE.selectedMenu = DPMenuProfEducation;
    }else if (indexPath.row == 4) {
        APP_DELEGATE.selectedMenu = DPMenuTakeASurvey;
    }else if (indexPath.row == 5) {
        APP_DELEGATE.selectedMenu = DPMenuProducts;
    }else if (indexPath.row == 6) {
        APP_DELEGATE.selectedMenu = DPMenuContactUs;
    }
    [APP_DELEGATE menuButtonTapped];
    
}

@end
