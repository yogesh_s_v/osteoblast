//
//  PFCSigmaCategoryViewController.m
//  DepuySynthes
//
//  Created by cdp on 12/25/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import "PFCSigmaCategoryViewController.h"
#import "PFCSigmaTableViewCell.h"
#import "OrganInfoViewController.h"
#import "ProductWebService.h"
#import "Constants.h"

@interface PFCSigmaCategoryViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UITableView *pfcSigmaCategoryTableView;
}
@end

@implementation PFCSigmaCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initializeDataAndView];
}

-(void)initializeDataAndView
{
//    pFCSigmaCategoryArray = [[NSMutableArray alloc]initWithObjects:@"PS Femur",@"CoCr tray",@"XLK poly", nil];
    
    [pfcSigmaCategoryTableView registerNib:[UINib nibWithNibName:@"PFCSigmaTableViewCell" bundle:nil] forCellReuseIdentifier:@"PFCSigmaTableViewCell"];
    [pfcSigmaCategoryTableView registerNib:[UINib nibWithNibName:@"PFCSigmaTableViewCell" bundle:nil] forHeaderFooterViewReuseIdentifier:@"PFCSigmaTableViewCell"];
    [pfcSigmaCategoryTableView setTableFooterView:[UIView new]];
}

#pragma mark - Table View data source methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.pFCSigmaCategoryArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"PFCSigmaTableViewCell";
    
    PFCSigmaTableViewCell *cell = (PFCSigmaTableViewCell*) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.pfcSigmaLabel.text = [[self.pFCSigmaCategoryArray objectAtIndex:indexPath.row] objectForKey:@"product_name"];
    
    cell.pfcSigmaImageView.layer.cornerRadius = cell.pfcSigmaImageView.frame.size.width/2;
//    cell.pfcSigmaImageView.backgroundColor = [UIColor colorWithRed:(rand()%255)/255.0f green:(rand()%255)/255.0f blue:(rand()%255)/255.0f alpha:1.0f];
//    cell.imageTextLable.textColor = [UIColor whiteColor];
//    cell.imageTextLable.text = [NSString stringWithFormat:@"%@",[[[self.pFCSigmaCategoryArray objectAtIndex:indexPath.row] objectForKey:@"product_name"] substringToIndex:1]];
    cell.pfcSigmaImageView.image = [UIImage imageNamed:@"JournalArticles"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id category = [self.pFCSigmaCategoryArray objectAtIndex:indexPath.row];
    
    ProductWebService *productWebService = [[ProductWebService alloc]init];
    id product_id = [category objectForKey:@"product_id"];
    if (product_id && [product_id isKindOfClass:[NSString class]]) {
        [productWebService getProductInfoWithKey:@"password" byProductID:product_id byCategoryID:@"" andCompletionBlock:^(id responseData) {
            if (responseData && [responseData isKindOfClass:[NSDictionary class]]) {
                id data = [responseData objectForKey:@"data"];
                if (data && [data isKindOfClass:[NSArray class]]) {
                    OrganInfoViewController *organInfoViewController = [[OrganInfoViewController alloc]init];
                    organInfoViewController.organInfoArray = data;
                    
                    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, APP_DELEGATE.window.frame.size.width-40.0f, 44.0f)];
                    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-5.0f, 2.0f, 147.0f, 42.0f)];
                    titleLabel.text = [category objectForKey:@"product_name"];
                    titleLabel.textColor = [UIColor whiteColor];
                    titleLabel.font = [UIFont systemFontOfSize:15.0f];
                    [titleView addSubview:titleLabel];
                    organInfoViewController.navigationItem.titleView = titleView;
                    
                    [self.navigationController pushViewController:organInfoViewController animated:YES];
                }
            }
        }];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
