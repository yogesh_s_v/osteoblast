//
//  ContactUsViewController.h
//  DepuySynthes
//
//  Created by Akshay Gohel on 21/01/15.
//  Copyright (c) 2015 The_world_inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactUsViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) NSString *titleString;

@end
