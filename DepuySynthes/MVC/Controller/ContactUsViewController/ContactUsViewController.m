//
//  ContactUsViewController.m
//  DepuySynthes
//
//  Created by Akshay Gohel on 21/01/15.
//  Copyright (c) 2015 The_world_inc. All rights reserved.
//

#import "ContactUsViewController.h"
#import "ProductWebService.h"
#import "Constants.h"

@interface ContactUsViewController () <UITextFieldDelegate,UITextViewDelegate> {
    UIButton *doneButton;
    BOOL keyboardAppeared;
    CGRect keyboardframe;
}
@property (weak, nonatomic) IBOutlet UIScrollView *containerScrollView;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *contNoTextField;
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneKeyboardButton;
@property (strong, nonatomic) IBOutlet UIToolbar *keyboardToolBar;

@end

@implementation UIView (FindViewThatIsFirstResponder)

- (UIView *)findViewThatIsFirstResponder
{
    if (self.isFirstResponder) {
        return self;
    }
    
    for (UIView *subView in self.subviews) {
        UIView *firstResponder = [subView findViewThatIsFirstResponder];
        if (firstResponder != nil) {
            return firstResponder;
        }
    }
    return nil;
}

@end

@implementation ContactUsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.messageTextView.layer.cornerRadius = 4.0f;
    self.messageTextView.layer.borderWidth = 1.0f;
    self.messageTextView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.messageTextView.clipsToBounds = YES;
    self.submitButton.layer.cornerRadius = 4.0f;
    self.containerScrollView.scrollEnabled = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.containerScrollView.contentSize = CGSizeMake(self.containerScrollView.frame.size.width, self.containerScrollView.frame.size.height);
    
//    NSUUID *uniqueId = [[UIDevice currentDevice] identifierForVendor];
//    DLog(@"uniqueId is : %@",uniqueId.UUIDString);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 3.2) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidAppear:)
                                                     name:UIKeyboardDidShowNotification
                                                   object:nil];
    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardDidAppear:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidDisappear:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    keyboardAppeared = YES;
    [self.keyboardToolBar removeFromSuperview];
}

-(void)viewDidAppear:(BOOL)animated {
    [APP_DELEGATE.bottomFooter setHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    if (!self.titleString || [self.titleString isEqualToString:@""]) {
        [self.containerView setHidden:YES];
        CGRect frame = self.containerScrollView.frame;
        frame.origin.y = self.containerView.frame.origin.y+5.0f;
        self.containerScrollView.frame = frame;
    }else {
        self.titleLabel.text = self.titleString;
    }
}

#pragma text field methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([self.nameTextField isFirstResponder]) {
        [self.emailTextField becomeFirstResponder];
    }else if ([self.emailTextField isFirstResponder]) {
        [self.contNoTextField becomeFirstResponder];
    }else if ([self.contNoTextField isFirstResponder]) {
        [textField resignFirstResponder];
    }else {
        [textField resignFirstResponder];
    }
    return YES;
}


-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([textView.text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    textView.inputAccessoryView = self.keyboardToolBar;
    return YES;
}

#pragma mark - button tapped events 

-(IBAction)submitButtonTapped:(id)sender {
    NSString *emailString = [self.emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSUUID *uniqueId = [[UIDevice currentDevice] identifierForVendor];
    NSString *uuidString = uniqueId.UUIDString;
    
    if (self.nameTextField.text == nil || [self.nameTextField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your name." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if (self.emailTextField.text == nil || [self.emailTextField.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your email Id." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if(![self validateEmailString:emailString]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter a valid email Id." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if (self.contNoTextField.text == nil || [self.contNoTextField.text isEqualToString:@""]) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your phone number." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
//        return;
    } else {
        if (self.contNoTextField.text.length != 10) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter valid phone number." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            return;
        }
    }
    
    if (self.messageTextView.text.length == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your message." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if (uuidString == nil || [uuidString isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Something went wrong." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    ProductWebService *productWebService = [[ProductWebService alloc]init];
//        if (!self.titleString || [self.titleString isEqualToString:@""]) {
    
            [productWebService registeForArticleWithKey:@"password" withMobile:self.contNoTextField.text withUUID:uuidString withEmail:emailString withName:self.nameTextField.text withUserName:self.nameTextField.text andCompletionBlock:^(id responseData) {
                if (responseData && [responseData isKindOfClass:[NSDictionary class]]) {
                    id responseCode = [((NSDictionary*)responseData) objectForKey:@"response_code"];
                    DLog(@"responseData : %@", responseData);
                    if (responseCode && [responseCode isKindOfClass:[NSNumber class]] && [responseCode intValue] == 200) {
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[responseData objectForKey:@"response_message"] message:@"Successfully registered" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];
                    }else {
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[responseData objectForKey:@"response_message"] message:@"Registration Incomplete" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];
                    }
                }
    }];
//    } else {
//        ProductWebService *productWebService = [[ProductWebService alloc]init];
//        [productWebService loginWithKey:@"password" withMobile:self.contNoTextField.text withUUID:uuidString andCompletionBlock:^(id responseData) {
//            if (responseData && [responseData isKindOfClass:[NSDictionary class]]) {
//                id responseCode = [((NSDictionary*)responseData) objectForKey:@"response_code"];
//                DLog(@"responseData : %@", responseData);
//                if (responseCode && [responseCode isKindOfClass:[NSNumber class]] && [responseCode intValue] == 200) {
//                    UIAlertView *alert;
//                    id data = [responseData objectForKey:@"data"];
//                    if (data && [data isKindOfClass:[NSDictionary class]]) {
//                        id error = [data objectForKey:@"error"];
//                        if (error && [error isKindOfClass:[NSString class]]) {
//                            alert = [[UIAlertView alloc]initWithTitle:[responseData objectForKey:@"response_message"] message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//                            [alert show];
//                        }
//                    }else {
//                        alert = [[UIAlertView alloc]initWithTitle:[responseData objectForKey:@"response_message"] message:@"Successfully registered" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//                        [alert show];
//                    }
//                }else {
//                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[responseData objectForKey:@"response_message"] message:@"Registration Incomplete" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//                    [alert show];
//                }
//            }
//        }];
//    }
}

-(IBAction)doneKeyboardButton:(id)sender {
    [self.messageTextView resignFirstResponder];
}

- (BOOL)validateEmailString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

#pragma mark - keydoard methods

-(void)keyboardDidAppear:(NSNotification *)notification
{
    [self addButtonToKeyboard];
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue *keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
//    if (keyboardAppeared) {
//        keyboardAppeared = NO;
        [UIView animateWithDuration:0.3f animations:^{
            CGRect frame = APP_DELEGATE.bottomFooter.frame;
            frame.origin.y = APP_DELEGATE.window.frame.size.height-40.0f-keyboardFrameBeginRect.size.height;
            
            CGRect frame1 = self.containerScrollView.frame;
            frame1.size.height = self.view.frame.size.height-keyboardFrameBeginRect.size.height-64.0f-self.containerView.frame.size.height;
            
            [APP_DELEGATE.bottomFooter setFrame:frame];
            [self.containerScrollView setFrame:frame1];
        } completion: nil];
//    }else {
//        if (keyboardFrameBeginRect.size.height < keyboardframe.size.height) {
//            float diffheight= (keyboardframe.size.height - keyboardFrameBeginRect.size.height);
//            [UIView animateWithDuration:0.3f animations:^{
//                
//                CGRect frame = APP_DELEGATE.bottomFooter.frame;
//                CGRect frame1 = self.containerScrollView.frame;
//                
//                frame.origin.y += diffheight;
//                frame1.size.height += diffheight;
//                
//                [APP_DELEGATE.bottomFooter setFrame:frame];
//                [self.containerScrollView setFrame:frame1];
//            } completion: ^ (BOOL finished)
//             {
//             }];
//        }else if (keyboardFrameBeginRect.size.height > keyboardframe.size.height) {
//            float diffheight= (keyboardFrameBeginRect.size.height - keyboardframe.size.height);
//            [UIView animateWithDuration:0.3f animations:^{
//                
//                CGRect frame = APP_DELEGATE.bottomFooter.frame;
//                CGRect frame1 = self.containerScrollView.frame;
//                
//                frame.origin.y -= diffheight;
//                frame1.size.height -= diffheight;
//                
//                [APP_DELEGATE.bottomFooter setFrame:frame];
//                [self.containerScrollView setFrame:frame1];
//            } completion: ^ (BOOL finished)
//             {
//             }];
//        }
//    }
//    keyboardframe = keyboardFrameBeginRect;
    //    [self addButtonToKeyboard];
}

-(void)keyboardDidDisappear:(NSNotification *)notification
{
    //    NSDictionary* keyboardInfo = [notification userInfo];
    //    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    //    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
//    keyboardAppeared = YES;
    [UIView animateWithDuration:0.3f animations:^{
        
        CGRect frame = APP_DELEGATE.bottomFooter.frame;
        frame.origin.y = APP_DELEGATE.window.frame.size.height-40.0f;
        
        CGRect frame1 = self.containerScrollView.frame;
        frame1.size.height = self.view.frame.size.height-64.0f-self.containerView.frame.size.height;
        
        [self.containerScrollView setFrame:frame1];
        [APP_DELEGATE.bottomFooter setFrame:frame];
    } completion:nil];
}

#pragma Mark Keyboard Delegate methods
- (void)addButtonToKeyboard {
    UIView *view = [self.view findViewThatIsFirstResponder];
    if ([view isKindOfClass:[UITextField class]] || [view isKindOfClass:[UITextView class]]) {
        if (((UITextView*)view).keyboardType == UIKeyboardTypeNumberPad)
        {
            @try {
                // create custom button
                if (doneButton != nil && doneButton.superview != nil) {
                    [doneButton removeFromSuperview];
                    [self removeButtonFromKeyboard];
                }
                doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [doneButton setTitle:@"Next" forState:UIControlStateNormal];
                [doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                doneButton.frame = CGRectMake(0, 163, 106, 53);
                doneButton.adjustsImageWhenHighlighted = NO;
                [doneButton addTarget:self action:@selector(doneButton:) forControlEvents:UIControlEventTouchUpInside];
                // locate keyboard view
                UIWindow* tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
                UIView* keyboard;
                for(int i=0; i<[tempWindow.subviews count]; i++) {
                    keyboard = [tempWindow.subviews objectAtIndex:i];
                    // keyboard found, add the button
                    if ([[keyboard description] hasPrefix:@"<UIPeripheralHost"] == YES)
                    {
                        [keyboard addSubview:doneButton];
                    }
                    //This code will work on iOS 8.0
                    else if([[keyboard description] hasPrefix:@"<UIInputSetContainerView"] == YES)
                    {
                        for(int i = 0 ; i < [keyboard.subviews count] ; i++)
                        {
                            UIView* hostkeyboard = [keyboard.subviews objectAtIndex:i];
                            if([[hostkeyboard description] hasPrefix:@"<UIInputSetHost"] == YES)
                            {
                                [hostkeyboard addSubview:doneButton];
                            }
                        }
                    }
                }
            }
            @catch (NSException *exception) {}
            @finally {}
        } else {
            if (doneButton != nil && doneButton.superview != nil) {
                [doneButton removeFromSuperview];
                [self removeButtonFromKeyboard];
            }
        }
    }
}

- (void)doneButton:(id)sender {
    [self textFieldShouldReturn:self.contNoTextField];
}


- (void)removeButtonFromKeyboard
{
    NSArray *arTemp = [[UIApplication sharedApplication] windows];
    if ([arTemp count] <= 1) return;
    UIWindow* tempWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:1];
    UIView* keyboard;
    for(int i=0; i<[tempWindow.subviews count]; i++)
    {
        keyboard = [tempWindow.subviews objectAtIndex:i];
        // keyboard found, add the button
        if ([[keyboard description] hasPrefix:@"<UIPeripheralHost"] == YES)
        {
            for (id temp in keyboard.subviews)
            {
                if ([temp isKindOfClass:[UIButton class]])
                {
                    UIButton *btnDone = (UIButton*) temp;
                    [btnDone removeFromSuperview];
                    break;
                }
            }
        }
        //This code will work on iOS 8.0
        else if([[keyboard description] hasPrefix:@"<UIInputSetContainerView"] == YES)
        {
            for(int i = 0 ; i < [keyboard.subviews count] ; i++)
            {
                UIView* hostkeyboard = [keyboard.subviews objectAtIndex:i];
                if([[hostkeyboard description] hasPrefix:@"<UIInputSetHost"] == YES)
                {
                    for (id temp in hostkeyboard.subviews)
                    {
                        if ([temp isKindOfClass:[UIButton class]])
                        {
                            UIButton *btnDone = (UIButton*) temp;
                            [btnDone removeFromSuperview];
                            break;
                        }
                    }
                }
            }
        }
        else{}
    }
}


@end
