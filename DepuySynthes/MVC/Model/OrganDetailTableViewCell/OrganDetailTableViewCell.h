//
//  OrganDetailTableViewCell.h
//  DepuySynthes
//
//  Created by cdp on 12/25/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrganDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *organDetailImageView;
@property (weak, nonatomic) IBOutlet UILabel *organCategoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *imageTextLable;

@end
