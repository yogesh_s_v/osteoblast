//
//  LaunchViewController.m
//  DepuySynthes
//
//  Created by Akshay G on 24/03/15.
//  Copyright (c) 2015 The_world_inc. All rights reserved.
//

#import "LaunchViewController.h"
#import "Constants.h"
#import "FLAnimatedImageView.h"
#import "FLAnimatedImage.h"

@interface LaunchViewController ()

@property (nonatomic, strong) FLAnimatedImageView *imageView;

@end

@implementation LaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!self.imageView) {
        self.imageView = [[FLAnimatedImageView alloc] init];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.imageView.clipsToBounds = YES;
    }
    [self.view addSubview:self.imageView];
    self.imageView.frame = self.view.bounds;
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"OsteoBlastAnimated" withExtension:@"gif"];
    NSData *data = [NSData dataWithContentsOfURL:url];
    FLAnimatedImage *animatedImage = [FLAnimatedImage animatedImageWithGIFData:data];
    self.imageView.animatedImage = animatedImage;
    
//    self.imageView.userInteractionEnabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)viewDidAppear:(BOOL)animated {
//    [self performSelector:@selector(setLaunchScreen) withObject:nil afterDelay:1.5f];
//}

-(void) setLaunchScreen {
    [APP_DELEGATE setLaunchScreen:YES];
}

@end
