//
//  HomeTableViewCell.m
//  DepuySynthes
//
//  Created by cdp on 12/21/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import "HomeTableViewCell.h"

@implementation HomeTableViewCell
@synthesize categoryNewsHeadingLabel,categoryNewsSubHeadingLabel,categoryNewsDateLabel;

- (void)awakeFromNib {
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
        
    // Configure the view for the selected state
}

@end
