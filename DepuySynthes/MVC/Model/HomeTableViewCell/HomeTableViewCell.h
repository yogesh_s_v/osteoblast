//
//  HomeTableViewCell.h
//  DepuySynthes
//
//  Created by cdp on 12/21/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *categoryIconImageView;
@property (weak, nonatomic) IBOutlet UILabel *categoryTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryNewsHeadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryNewsSubHeadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryNewsDateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *categoryImageView;

@end
