//
//  SecondLaunchImageViewController.m
//  DepuySynthes
//
//  Created by Akshay Gohel on 22/01/15.
//  Copyright (c) 2015 The_world_inc. All rights reserved.
//

#import "SecondLaunchImageViewController.h"

@interface SecondLaunchImageViewController ()

@end

@implementation SecondLaunchImageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
