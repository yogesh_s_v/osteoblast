//
//  Constants.h
//  Akshay Gohel
//
//  Created by Akshay Gohel on 21/07/14.
//  Copyright (c) 2014 Akshay Gohel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "CommonSettings.h"
//#import "UIFont+Font.h"

//#define LOG_IT

#ifdef LOG_IT
#	define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#	define DLog(...)
#endif

#define isPhone568 ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568)

#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#define COMMON_SETTINGS ((CommonSettings*)[CommonSettings sharedInstance])

#define MAX_FONT_SIZE_OFFSET 5.0f

#define COMPUTE_FONT_SIZE(initialFontSize) (((COMMON_SETTINGS.fontPercentage*MAX_FONT_SIZE_OFFSET)/100.0f)+initialFontSize)

#define BOX_BODER_COLOR [UIColor colorWithRed:140/255.0 green:140/255.0 blue:140/255.0 alpha:1]
#define BOX_TEXT_COLOR [UIColor colorWithRed:122/255.0 green:122/255.0 blue:122/255.0 alpha:1]
