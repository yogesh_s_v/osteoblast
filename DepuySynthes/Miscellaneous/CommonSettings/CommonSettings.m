//
//  CommonSettings.m
//  Akshay Gohel
//
//  Created by Akshay Gohel on 27/07/14.
//  Copyright (c) 2014 Akshay Gohel. All rights reserved.
//

#import "CommonSettings.h"
#import "Constants.h"

@interface CommonSettings ()

@property(nonatomic, strong) UIView *progressView;

@end

@implementation CommonSettings

static CommonSettings *sharedInstance;

+(CommonSettings*) sharedInstance {
    if (!sharedInstance) {
        sharedInstance = [[CommonSettings alloc] init];
        [sharedInstance resetContents];
    }
    return sharedInstance;
}

-(void) resetContents {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:[NSString stringWithFormat:@"ProjectDetails_%@", [self.userInfoDict objectForKey:@"UserID"]]];
//    self.userInfoDict = nil;
    self.userInfoDict = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"Creds"] copyItems:YES];
    self.sessionToken = nil;
    self.customerID = @"0";
    self.selectedRow = -1;
    self.settingsOurViewsChecked = NO;
    self.settingsPortfolioTrackerChecked = NO;
    self.settingsReccomendationsChecked = NO;
    self.settingsTodaysMarketChecked = NO;
    self.userServicesDict = [[NSMutableDictionary alloc]init];
}

-(void) saveInfo {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.userInfoDict forKey:@"ApnaUserKaDetails"];
    [defaults synchronize];
}

-(void) loadInfo {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.userInfoDict = [defaults objectForKey:@"ApnaUserKaDetails"];
    [defaults synchronize];
}

-(NSString*)userMobile {
    id userMobile = [COMMON_SETTINGS.userInfoDict objectForKey:@"userMobile"];
    if (userMobile) {
        return userMobile;
    }
    return @"";
}

-(void) showActivityIndicator {
    if (self.progressView) {
        [self hideActivityIndicatorAnimated:NO];
    }
    dispatch_async(dispatch_get_main_queue(),^{
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        id windows = [[UIApplication sharedApplication] windows];
        for (UIWindow *w in windows) {
            if (w.isKeyWindow) {
                window = w;
                break;
            }
        }
        self.progressView = [[UIView alloc] initWithFrame:window.frame];
        //    self.progressView.alpha = 0.0f;
        
        UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 50.0f, 50.0f)];
        containerView.backgroundColor = [UIColor colorWithRed:192.0f/255.0f green:0.0f blue:0.0f alpha:0.8f];
        containerView.layer.cornerRadius = 5.0f;
        containerView.layer.masksToBounds = YES;
        containerView.center = CGPointMake(window.frame.size.width/2.0f, window.frame.size.height/2.0f);
        [self.progressView addSubview:containerView];
        containerView.tag = 123223;
        
        self.progressView.tag = 987132;
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
        activityIndicator.color = [UIColor whiteColor];
        activityIndicator.center = containerView.center;//CGPointMake(160.0f, isPhone568?294.0f:250.0f);
        activityIndicator.tag = 123789;
        [self.progressView addSubview:activityIndicator];
        
        [window addSubview:self.progressView];
        [activityIndicator startAnimating];
        self.progressView.userInteractionEnabled = YES;
        
        [self addBounceAnnimationToView:containerView];
    });
}

-(void) hideActivityIndicator {
    [self hideActivityIndicatorAnimated:YES];
}

-(void) hideActivityIndicatorAnimated:(BOOL) animated {
    dispatch_async(dispatch_get_main_queue(),^{
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        id windows = [[UIApplication sharedApplication] windows];
        for (UIWindow *w in windows) {
            if (w.isKeyWindow) {
                window = w;
                break;
            }
        }
        
        UIView *v = nil;
        
        BOOL shouldAddDelay = NO;
        BOOL progressViewFound = NO;
        for (UIView *tempV in window.subviews) {
            if (tempV.tag == 987132) {
                progressViewFound = YES;
                v = tempV;
            }
        }
        
        if (!progressViewFound) {
            v = self.progressView;
        }
        if (animated) {
            UIView *activityIndicator = [v viewWithTag:123223];
            if (activityIndicator) {
                shouldAddDelay = YES;
                [self performSelector:@selector(removeActivityIndicatorFromView:) withObject:v afterDelay:0.6f];
                
                CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
                
                bounceAnimation.values = @[@(1), @(0.9), @(1.1), @(0.05)];
                
                bounceAnimation.duration = 0.6;
                NSMutableArray *timingFunctions = [[NSMutableArray alloc] initWithCapacity:bounceAnimation.values.count];
                for (NSUInteger i = 0; i < bounceAnimation.values.count; i++) {
                    [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
                }
                [bounceAnimation setTimingFunctions:timingFunctions.copy];
                bounceAnimation.removedOnCompletion = NO;
                bounceAnimation.fillMode = kCAFillModeForwards;
                
                [activityIndicator.layer addAnimation:bounceAnimation forKey:@"bounce"];
            }
        }
        
        if (!shouldAddDelay) {
            progressViewFound = YES;
            UIView *activityIndicator = [v viewWithTag:123789];
            if (activityIndicator && [activityIndicator isKindOfClass:[UIActivityIndicatorView class]]) {
                [((UIActivityIndicatorView*)activityIndicator) stopAnimating];
                [((UIActivityIndicatorView*)activityIndicator) removeFromSuperview];
            }
            
            v.userInteractionEnabled = NO;
            [v removeFromSuperview];
            
            if (v != self.progressView) {
                self.progressView.userInteractionEnabled = NO;
                [self.progressView removeFromSuperview];
                self.progressView = nil;
            }
        }
    });
    
}

-(void) removeActivityIndicatorFromView:(UIView*) v {
    UIView *activityIndicator = [v viewWithTag:123789];
    if (activityIndicator && [activityIndicator isKindOfClass:[UIActivityIndicatorView class]]) {
        [((UIActivityIndicatorView*)activityIndicator) stopAnimating];
        [((UIActivityIndicatorView*)activityIndicator) removeFromSuperview];
    }
    
    v.userInteractionEnabled = NO;
    [v removeFromSuperview];
    
    if (v != self.progressView) {
        self.progressView.userInteractionEnabled = NO;
        [self.progressView removeFromSuperview];
        self.progressView = nil;
    }
}

- (void)addBounceAnnimationToView:(UIView *)view
{
    CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    bounceAnimation.values = @[@(0.05), @(1.1), @(0.9), @(1)];
    
    bounceAnimation.duration = 0.6;
    NSMutableArray *timingFunctions = [[NSMutableArray alloc] initWithCapacity:bounceAnimation.values.count];
    for (NSUInteger i = 0; i < bounceAnimation.values.count; i++) {
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    }
    [bounceAnimation setTimingFunctions:timingFunctions.copy];
    bounceAnimation.removedOnCompletion = NO;
    bounceAnimation.fillMode = kCAFillModeForwards;
    
    [view.layer addAnimation:bounceAnimation forKey:@"bounce"];
}

-(void) showAppologiesAlertWithTitle:(NSString*) title andMessage:(NSString*) message andDelegate:(id)delegate {
    if (!self.alertView.isVisible) {
        if (title == nil) {
            title = @"Apologies!";
        }
        if (message == nil) {
            message = @"There is some error. Please try again later.";
        }
        [self.alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:@"Dismiss" otherButtonTitles: nil] show];
    }
}

@end
