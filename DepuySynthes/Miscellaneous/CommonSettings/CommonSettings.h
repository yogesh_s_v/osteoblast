//
//  CommonSettings.h
//  Akshay Gohel
//
//  Created by Akshay Gohel on 27/07/14.
//  Copyright (c) 2014 Akshay Gohel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@class DimView;

@interface CommonSettings : NSObject

@property(nonatomic, strong) CLLocation * userLocation;
@property (strong, nonatomic) DimView *dimView;

@property(nonatomic, strong) id userInfoDict;

@property(nonatomic, strong) NSMutableDictionary *userServicesDict;

@property(nonatomic, strong) NSString *sessionToken;
@property(nonatomic, strong) NSString *customerID;

@property(nonatomic, strong) UIAlertView *alertView;

@property (nonatomic) int selectedRow;

@property (nonatomic) BOOL settingsTodaysMarketChecked;
@property (nonatomic) BOOL settingsOurViewsChecked;
@property (nonatomic) BOOL settingsReccomendationsChecked;
@property (nonatomic) BOOL settingsPortfolioTrackerChecked;

@property (nonatomic) BOOL showTodaysMarketNotification;
@property (nonatomic) BOOL showOurViewsNotification;
@property (nonatomic) BOOL showReccomendationsNotification;
@property (nonatomic) BOOL showPortfolioTrackerNotification;

@property (nonatomic) BOOL isManageEquityPortfolioSelected;
@property (nonatomic) BOOL isManageMutualfundPortfolioSelected;

@property (nonatomic) BOOL isModifyMutualfundPortfolioSelected;
@property (nonatomic) BOOL isModifyEquityPortfolioSelected;

@property (nonatomic) BOOL isBuyExistingPortfolioSelected;
@property (nonatomic) BOOL isBuyNewPortfolioSelected;

@property (nonatomic) BOOL isBuyEquityPortfolioSelected;
@property (nonatomic) BOOL isBuyMutualfundPortfolioSelected;

@property (nonatomic) BOOL isHomeViewVisible;

@property (nonatomic) float fontPercentage;
//@property (nonatomic) float catgryStringFont;
//@property (nonatomic) float titleStringFont;
//@property (nonatomic) float detailStringFont;

+(CommonSettings*) sharedInstance;
-(void) resetContents;
-(void) saveInfo;
-(void) loadInfo;

-(NSString*)userMobile;

-(void) showActivityIndicator;
-(void) hideActivityIndicator;
-(void) showAppologiesAlertWithTitle:(NSString*) title andMessage:(NSString*) message andDelegate:(id)delegate;

@end
