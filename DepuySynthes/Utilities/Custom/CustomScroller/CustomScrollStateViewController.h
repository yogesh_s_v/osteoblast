//
//  CustomScrollStateViewController.h
//  DepuySynthes
//
//  Created by Akshay Gohel on 2/1/2015.
//  Copyright (c) 2013 The_world_inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomScrollStateViewControllerDelegate <NSObject>

@required
-(void) customScrollStateViewControllerDidSelectItemAtIndex:(int)index;

@end

@interface CustomScrollStateViewController : UIViewController {
    IBOutlet UIScrollView *containerScrollView;
}

@property(nonatomic, weak) id<CustomScrollStateViewControllerDelegate> delegate;
@property(nonatomic) int selectedTitleIndex;

@end
