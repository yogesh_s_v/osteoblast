//
//  CustomScrollStateViewController.m
//  DepuySynthes
//
//  Created by Akshay Gohel on 2/1/2015.
//  Copyright (c) 2013 The_world_inc. All rights reserved.
//

#import "CustomScrollStateViewController.h"

@interface CustomScrollStateViewController ()

@end

@implementation CustomScrollStateViewController

@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) reloadData {
    for (UIView *v in containerScrollView.subviews) {
        if ([v isKindOfClass:[UIButton class]]) {
            [v removeFromSuperview];
        }
    }
    
    NSArray *listofItems = [[NSArray alloc] initWithObjects:NSLocalizedString(@"Upcoming", @"Upcoming"), NSLocalizedString(@"Pending", @"Pending"), NSLocalizedString(@"Confirmed", @"Confirmed"), NSLocalizedString(@"Archived", @"Archived"), nil];
    
    UIButton *firstButton = nil;
    self.selectedTitleIndex = 0;
    int i = 0;
    float xSize = 160.0f;
    for (id object in listofItems) {
        if ([object isKindOfClass:[NSString class]]) {
            UIButton *textButton = [[UIButton alloc] initWithFrame:CGRectZero];
            
            CGSize size = [object sizeWithFont:[UIFont fontWithName:@"HelveticaNeue-UltraLight" size:15.0f] constrainedToSize:CGSizeMake(self.view.frame.size.width, 20.0f)];
            
            if (i==0) {
                firstButton = textButton;
                xSize = xSize-(size.width+60.0f)/2;
            }
            [textButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [textButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
            [textButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
            textButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17.0f];
            [textButton setBackgroundColor:[UIColor clearColor]];
            [textButton setTitle:object forState:UIControlStateNormal];
            
            textButton.frame = CGRectMake(xSize, 0.0f, size.width+60.0f, self.view.frame.size.height);
            xSize += (size.width+60.0f);
            
            textButton.tag = i;
            
            [textButton addTarget:self action:@selector(selectButton:) forControlEvents:UIControlEventTouchUpInside];
            
            [containerScrollView addSubview:textButton];
            
            if (i == [listofItems count]-1) {
                xSize = xSize+160.0f-(size.width+60.0f)/2;
            }
        }
        i++;
    }
    containerScrollView.contentSize = CGSizeMake(xSize>self.view.frame.size.width?xSize:self.view.frame.size.width, self.view.frame.size.height);
//    if (containerScrollView.contentSize.width > containerScrollView.bounds.size.width) {
//    } else {
//        float differenceX = containerScrollView.contentSize.width-xSize;
//        for (UIView *v in containerScrollView.subviews) {
//            if ([v isKindOfClass:[UIButton class]]) {
//                CGRect frame = v.frame;
//                frame.origin.x += differenceX/2.0f - 7.5f;
//                v.frame = frame;
//            }
//        }
//    }
    [self selectButton:firstButton];
}

-(void) selectButton: (UIButton*) button {
    if (button == nil) {
        return;
    }
    for (UIView *v in containerScrollView.subviews) {
        if (v.tag == self.selectedTitleIndex && [v isKindOfClass:[UIButton class]]) {
            [((UIButton*)v) setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            ((UIButton*)v).titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17.0f];
            break;
        }
    }
    [button setTitleColor:[UIColor colorWithRed:121.0f/255.0f green:221.0f/255.0f blue:20.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    self.selectedTitleIndex = button.tag;
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:18.0f];
    [containerScrollView scrollRectToVisible:CGRectMake(button.center.x-160.0f, 0.0f, 320.0f, 46.0f) animated:YES];
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(customScrollStateViewControllerDidSelectItemAtIndex:)]) {
        [self.delegate customScrollStateViewControllerDidSelectItemAtIndex:button.tag];
    }
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if (containerScrollView.contentSize.width > containerScrollView.bounds.size.width) {
        float offset = scrollView.contentOffset.x;
        float width = scrollView.contentSize.width-self.view.frame.size.width;
        offset = offset/width;             // 144 = 464(Scrollbar contentSize)-320(ScreenSize)
    }
}

@end
