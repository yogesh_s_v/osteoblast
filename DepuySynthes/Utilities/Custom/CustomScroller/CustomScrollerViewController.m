//
//  CustomScrollerViewController.m
//  DepuySynthes
//
//  Created by Akshay Gohel on 2/1/2015.
//  Copyright (c) 2013 The_world_inc. All rights reserved.
//

#import "CustomScrollerViewController.h"
#import "CustomButton.h"

@interface CustomScrollerViewController ()

@property(nonatomic, weak) IBOutlet UIScrollView *containerScrollView;
@property(nonatomic, weak) IBOutlet UILabel *bottomSeparatorLabel;

@end

@implementation CustomScrollerViewController

@synthesize delegate;
@synthesize listofItems;
@synthesize containerScrollView;
@synthesize defaultColor;
@synthesize selectedColor;
@synthesize selectedTitleIndex;
@synthesize markerView;
@synthesize rightFadeImageView, leftFadeImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.leftFadeImageView.alpha = 0.0f;
    self.rightFadeImageView.alpha = 0.0f;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) reloadData {
    self.bottomSeparatorLabel.backgroundColor = self.markerColor?self.markerColor:(self.defaultColor!=nil?self.defaultColor:[UIColor blackColor]);
    for (UIView *v in self.containerScrollView.subviews) {
        if ([v isKindOfClass:[UIButton class]] || [v isKindOfClass:[UILabel class]]) {
            [v removeFromSuperview];
        }
    }
    CustomButton *firstButton = nil;
    self.selectedTitleIndex = 0;
    int i = 0;
    float xSize = 6.0f;
    for (id object in self.listofItems) {
        if ([object isKindOfClass:[NSString class]]) {
            NSString *titleString = object;
            CustomButton *textButton = [[CustomButton alloc] initWithFrame:CGRectZero];
            if (i==0) {
                firstButton = textButton;
            }
            [textButton setTitleColor:self.defaultColor!=nil?self.defaultColor:[UIColor blackColor] forState:UIControlStateNormal];
            [textButton setTitleColor:self.selectedColor!=nil?self.selectedColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
            [textButton setTitleColor:self.selectedColor!=nil?self.selectedColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
            textButton.titleLabel.font = [UIFont systemFontOfSize:11.5f];
            [textButton setBackgroundColor:[UIColor clearColor]];
            [textButton setTitle:titleString forState:UIControlStateNormal];
            
            NSDictionary* stringAttrs = @{ NSFontAttributeName:[UIFont systemFontOfSize:11.5f]};
            
            CGSize size = [titleString boundingRectWithSize:CGSizeMake(self.view.frame.size.width, 20.0f) options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:stringAttrs context:nil].size;
            
            textButton.frame = CGRectMake(xSize, 3.0f, size.width+10.0f, self.view.frame.size.height-6.0f);
            if (i != ((int)[self.listofItems count])-1) {
                xSize += size.width+20.0f;
            } else {
                xSize += size.width+14.0f;
            }
            
            textButton.tag = i;
            
            [textButton addTarget:self action:@selector(selectButton:) forControlEvents:UIControlEventTouchUpInside];
            
            [self.containerScrollView addSubview:textButton];
            if (self.showSeparator) {
                if (i != ((int)[self.listofItems count])-1) {
                    UILabel *separator = [[UILabel alloc] initWithFrame:CGRectMake(xSize-5.5f, 10.0f, 1.5f, self.view.frame.size.height-23.0f)];
                    separator.backgroundColor = self.markerColor?self.markerColor:(self.defaultColor!=nil?self.defaultColor:[UIColor blackColor]);
                    [self.containerScrollView addSubview:separator];
                }
            }
        }
        i++;
    }
    self.containerScrollView.contentSize = CGSizeMake(xSize>self.view.frame.size.width?xSize:self.view.frame.size.width, self.view.frame.size.height);
    if (self.containerScrollView.contentSize.width > self.containerScrollView.bounds.size.width) {
        self.rightFadeImageView.alpha = 1.0f;
    } else {
        float differenceX =self.containerScrollView.contentSize.width-xSize;
        for (UIView *v in self.containerScrollView.subviews) {
            if ([v isKindOfClass:[UIButton class]]) {
                CGRect frame = v.frame;
                frame.origin.x += differenceX/2.0f - 7.5f;
                v.frame = frame;
            }
        }
    }
    [self selectButton:firstButton];
}

-(void) selectButton: (CustomButton*) button {
    if (button == nil) {
        return;
    }
    for (UIView *v in self.containerScrollView.subviews) {
        if (v.tag == self.selectedTitleIndex && [v isKindOfClass:[UIButton class]]) {
            [((UIButton*)v) setTitleColor:defaultColor forState:UIControlStateNormal];
            ((UIButton*)v).titleLabel.font = [UIFont systemFontOfSize:11.5f];
            break;
        }
    }
    [button setTitleColor:selectedColor forState:UIControlStateNormal];
    self.selectedTitleIndex = button.tag;
    button.titleLabel.font = [UIFont systemFontOfSize:11.5f];
    [self.containerScrollView scrollRectToVisible:button.frame animated:YES];
    
    if (self.markerView == nil) {
        self.markerView = [[UIView alloc] init];//WithImage:[UIImage imageNamed:@"GreenMarker.png"]];
        self.markerView.backgroundColor = self.markerColor?self.markerColor:[UIColor colorWithRed:32.0f/255.0f green:117.0f/255.0f blue:188.0f/255.0f alpha:1.0f];
        [self.containerScrollView addSubview:self.markerView];
    }
    [UIView animateWithDuration:0.5f animations:^{
        CGRect frame = button.frame;
        frame.origin.x -= 6.0f;
        frame.origin.y += frame.size.height-3.0f;
        frame.size.height = 3.0f;
        frame.size.width += 12.0f;
        self.markerView.frame = frame;
    }];
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(customScrollerViewControllerDidSelectItem:atIndex:)]) {
        [self.delegate customScrollerViewControllerDidSelectItem:[self.listofItems objectAtIndex:button.tag] atIndex:button.tag];
    }
}

-(void) selectIndex: (int) index {
    if (index >= 0 && index < [self.listofItems count]) {
        for (UIView *v in self.containerScrollView.subviews) {
            if (v.tag == index && [v isKindOfClass:[CustomButton class]]) {
                CustomButton *button = (CustomButton*)v;
                [self selectButton:button];
                return;
            }
        }
    }
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.containerScrollView.contentSize.width > self.containerScrollView.bounds.size.width) {
        float offset = scrollView.contentOffset.x;
        float width = scrollView.contentSize.width-self.view.frame.size.width;
        offset = offset/width;             // 144 = 464(Scrollbar contentSize)-320(ScreenSize)
        self.leftFadeImageView.alpha = offset;
        self.rightFadeImageView.alpha = 1.0 - offset;
    }
}

@end
