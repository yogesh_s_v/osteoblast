//
//  CustomScrollerViewController.m
//  DepuySynthes
//
//  Created by Akshay Gohel on 2/1/2015.
//  Copyright (c) 2013 The_world_inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomScrollerViewControllerDelegate <NSObject>

@required
-(void) customScrollerViewControllerDidSelectItem:(id)item atIndex:(int)index;

@end

@interface CustomScrollerViewController : UIViewController <UIScrollViewDelegate>

@property(nonatomic, weak) id<CustomScrollerViewControllerDelegate> delegate;
@property(nonatomic, strong) NSArray *listofItems;
@property(nonatomic) UIColor *defaultColor;
@property(nonatomic) UIColor *selectedColor;
@property(nonatomic) UIColor *markerColor;
@property(nonatomic) int selectedTitleIndex;
@property(nonatomic) BOOL showSeparator;
@property(nonatomic, strong) UIView *markerView;
@property(nonatomic, weak) IBOutlet UIImageView *rightFadeImageView, *leftFadeImageView;

-(void) selectIndex: (int) index;
-(void) reloadData;

@end
