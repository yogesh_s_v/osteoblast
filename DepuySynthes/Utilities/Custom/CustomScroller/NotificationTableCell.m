//
//  NotificationTableCell.m
//  DepuySynthes
//
//  Created by Akshay Gohel on 2/1/2015.
//  Copyright (c) 2013 The_world_inc. All rights reserved.
//

#import "NotificationTableCell.h"

@implementation NotificationTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
