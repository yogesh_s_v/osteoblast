//
//  NotificationTableCell.h
//  DepuySynthes
//
//  Created by Akshay Gohel on 2/1/2015.
//  Copyright (c) 2013 The_world_inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *cellContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *firstLable;
@property (weak, nonatomic) IBOutlet UILabel *secondLable;
@property (weak, nonatomic) IBOutlet UILabel *centerLable;
@property (weak, nonatomic) IBOutlet UILabel *mainLable;



@end
