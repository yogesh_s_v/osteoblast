//
//  MyTravellPlansLogTableViewCell.h
//  DepuySynthes
//
//  Created by Akshay Gohel on 2/1/2015.
//  Copyright (c) 2013 The_world_inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTravellPlansLogTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *mytravelPlanLogViewContainer;
@property (weak, nonatomic) IBOutlet UILabel *fromLocationLable;
@property (weak, nonatomic) IBOutlet UILabel *toLocationLable;
@property (weak, nonatomic) IBOutlet UILabel *travelDateLable;
@property (weak, nonatomic) IBOutlet UILabel *purposeOfTravelLable;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@end
