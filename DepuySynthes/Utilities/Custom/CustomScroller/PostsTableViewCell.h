//
//  PostsTableCell.h
//  DepuySynthes
//
//  Created by Akshay Gohel on 2/1/2015.
//  Copyright (c) 2013 The_world_inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *cellContainerView;
@property (weak, nonatomic) IBOutlet UILabel *postNameLable;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *profileNameLable;
@property (weak, nonatomic) IBOutlet UILabel *postDateLable;
@property (weak, nonatomic) IBOutlet UIButton *messageButton;
@property (weak, nonatomic) IBOutlet UILabel *numberOfMessageLable;

@end
