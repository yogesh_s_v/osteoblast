//
//  AppDelegate.h
//  DepuySynthes
//
//  Created by cdp on 12/18/14.
//  Copyright (c) 2014 The_world_inc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    DPMenuNone                      = 0,
    DPMenuJournal                   = 1,
    DPMenuConferencesAndWorkshops   = 2,
    DPMenuCaseStudies               = 3,
    DPMenuProfEducation             = 4,
    DPMenuTakeASurvey               = 5,
    DPMenuProducts                  = 6,
    DPMenuContactUs                 = 7
} DPMenus;

@class WEPopoverController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) WEPopoverController *popoverController;

@property (strong, nonatomic) UIView *bottomFooter;

@property(nonatomic) DPMenus selectedMenu;

@property (strong, nonatomic) NSString *uidToDelete;

-(void) menuButtonTapped;

-(void) setLoginScreen:(BOOL)animated;

-(void) setHomeScreen:(BOOL)animated;

-(void) setLaunchScreen:(BOOL)animated;

@end

